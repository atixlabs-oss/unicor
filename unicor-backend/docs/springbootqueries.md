# Queries in Spring Boot

## Derived Queries
Spring Boot is framework convention-over-configuration focused geared towards creating applications with minimal effort nd provides the derived queries for that purpose.

We can get all the basic queries just by writing the function names correctly according to the derivated convention.

Only need create a interface and extends of _JpaRepository_ 

Example:  
We have a entity
```
@Entity
public class Book {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String description;
}
```

Only creating the interface
```
//JpaRepository<{Entity Class}, {Entity Id type}>
public interface BookRepository extends JpaRepository<Book, Long> {
 
}
```

now we have by default a large number of queries implemented like findById (), findAll () ...  
If we need a specific simple query, we just have to define a function with the named derived query convention  
```
public interface BookRepository extends JpaRepository<Book, Long> {
    //No need implemetation is a Derived Query
    List<Book> findByDescription(String description);
}
```

It's only useful for simples queries on one table, for complex queries we need to use other solutions

### Helpful links
[Derived Queries](https://www.baeldung.com/spring-data-derived-queries)  
[Derived Queries guide](https://thorben-janssen.com/ultimate-guide-derived-queries-with-spring-data-jpa/)  


## @Query, JPQL(hql) and native(sql)
It's a alternative for complex queries, but it is not the best choice. We should use JPA Criteria API for prevent [SQL Injection](https://www.baeldung.com/sql-injection).  
In some cases using JPQL or a native query is the only choice available, for example if we need to do a RIGHT JOIN (not compatible with Criteria)  

Example:
 ```
 public interface BookRepository extends JpaRepository<Book, Long> {
   
        @Query("update Book b set b.status = :status where b.description = :description")
        int updateBookSetStatusForDescription(@Param("status") Integer status,  @Param("description") String description);

 }
 ```
 
[@Query Documentation](https://www.baeldung.com/spring-data-jpa-query) 


## Criteria
we use [Criteria](https://www.baeldung.com/spring-data-criteria-queries) and [Specifications](https://reflectoring.io/spring-data-specifications/) pattern for build queries using 
 and [hibernate gen](https://hibernate.org/orm/tooling/) as Metamodel Generator to not harcode attribute names 
 
//TODO 
[REST API QUERY](https://www.baeldung.com/spring-rest-api-query-search-language-tutorial) <-*IM NOT SURE ABOUT THAT*  

## QueryDSL 
It is a good alternative to Criteria, better in some cases, but I'm not sure it's a mature enough project yet.  
Links  
[QueryDSL](http://www.querydsl.com/)  
[QueryDSL with JPA](https://www.baeldung.com/querydsl-with-jpa-tutorial)