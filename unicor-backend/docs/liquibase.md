# LIQUIBASE - Atix Java Spring Template

## Dependencies  
- [liquibase-core](https://mvnrepository.com/artifact/org.liquibase/liquibase-core)
- [liquibase-maven-plugin](https://docs.liquibase.com/tools-integrations/maven/home.html) 
- [liquibase-hibernate5](https://github.com/liquibase/liquibase-hibernate)  

### Helpful links
[Liquibase on spring boot](https://www.baeldung.com/liquibase-refactor-schema-of-java-app)  

## why liquibase?
//TODO  
[link 1](https://medium.com/@texorcist/6-reasons-you-should-try-liquibase-for-your-next-project-47c5bf86ca9)  
[link 2](https://medium.com/@texorcist/convince-your-team-to-try-liquibase-976741cd9613)  

## liquibase vs flyway
//TODO   
[link 1](https://medium.com/@ruxijitianu/database-version-control-liquibase-versus-flyway-9872d43ee5a4)  
[link 2 (liquibase link, maybe not impartial vision)](https://www.liquibase.org/liquibase-vs-flyway)
## Excecuting liquibase
There is two ways for run changelogs scripts
- spring boot embedded 
- run by MVN Liquibase command

### Liquibase conventions in Springboot
- changelogs master name: **db.changelog-master.yaml**
- changelogs path: **src/main/resources/db/changelog**

### Spring boot embedded 
It's need set some properties on  _**application.properties**_
``` 
#LIQUIBASE
logging.level.liquibase = INFO 
spring.liquibase.change-log= db/changelog/db.changelog-master.yaml
spring.liquibase.enabled=true
spring.liquibase.drop-first=false
``` 
**logging.level.liquibase**: _logging level_   
**spring.liquibase.change-log**: _master changelog file_  
**spring.liquibase.enabled**: _liquibase run on start app enabled/disabled  (MUST BE DISABLED ON PROD PROPERTIES)_  
**spring.liquibase.drop-first**: _clean database on start app, (NOT USE ON PROD PROPERTIES)_  

when can use this way?   
on local running and sometimes on QA server

### MVN Liquibase command
we can add all necesary configuration on command or generate a _liquibase.properties_  
example  
``` 
url=jdbc:postgresql://localhost:5432/ajtdb
username=ajtuser
password=ajtpass
driver=org.postgresql.Driver

referenceUrl=hibernate:spring:com.atixlabs?dialect=org.hibernate.dialect.PostgreSQLDialect&hibernate.physical_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy&hibernate.implicit_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy

changeLogFile=src/main/resources/db/changelog/db.changelog-master.yaml
diffChangeLogFile=src/main/resources/db/changelog/diff.db.postgresql.sql
outputChangeLogFile=src/main/resources/db/changelog/db.output.postgresql.sql
``` 
**url, username, password, driver**: _access DB info_   
**referenceUrl**:  _the source for [diff](https://docs.liquibase.com/tools-integrations/maven/commands/maven-diff.html) comparison. (App JPA Model in the example)_  
**changeLogFile**: _master changelog file_  
**diffChangeLogFile**: _output diff changelog file_   
**outputChangeLogFile**: [_output initial changelog file_](https://docs.liquibase.com/tools-integrations/maven/commands/maven-generatechangelog.html)  

#### Useful commands
- **Initial changelog** (usually not necesary)  

 ```
#schemas
./mvnw liquibase:generateChangeLog -Dliquibase.outputChangeLogFile={output file}

 ```
  ```
#data
 ./mvnw liquibase:generateChangeLog -Dliquibase.outputChangeLogFile={output file} -Dliquibase.diffTypes=data
 
  ```
 - **Generate diff file**  
_If you want make diff over model is important build project before run the command_   
``` 
./mvnw liquibase:diff -Dliquibase.diffChangeLogFile={output diff file}
``` 

**Hey read that!!**  
 	

> for the output files:  
The extension provided determines the format of the changelog, so if you specify the filename as changelog.xml you will get an XML formatted changelog. However, if you specify the filename as changelog.yaml or changelog.json or changelog.postgresql.sql you will get changelogs formatted in YAML or JSON or SQL, respectively.  

- **Update changelog on database**  
if you want to execute the changes on a server database, first redirect the db server port to local port  
```ssh root@{url server} -L{Port}:localhost:5432```  
then execute  
```
./mvnw liquibase:update -Dliquibase.changeLogFile=src/main/resources/db/changelog/db.changelog-master.yaml -Dliquibase.url=jdbc:postgresql://localhost:{Port}/{DB_NAME}
 -Dliquibase.username={DB_USERNAME} -Dliquibase.password={DB_PASS} -Dliquibase.contexts={staging, uat,prod, etc..}
```  
 
 ### Naming and conventions
#### changelogs files
we put the changelogs file on 
```
database/changelog/migrations/schema
#for tables and structure changes

database/changelog/migrations/data
#for data (inserts, updates, deletes...) 
```

when adding a new data or schema script , you must also add the link to master change log too

#### file changelogs names
the file name must respect the format   
```
{secuential number}-{creation date aaaammdd}-{type schema-datos}-{info}.sql (or yaml, xml, json)
```

example
```  
001-20201230-schema-security.sql
002-20210104-data-add new skus.sql
```
changelog master
```
...
 - include:
      file: db/changelog/migrations/schema/001-20201230-schema-security.sql
 - include:
      file: db/changelog/migrations/data/002-20210104-data-add new skus.sql
```

#### changeset names
if you add a changeset manually, set the name with this format
```
#sql format example
-- changeset {nombre del desarrollador}:{aaaammdd}-{nro correlativo en la fecha}

#example
-- changeset martin:20210601-01 
```
**Hey read that!!**  
We should use the tag _**comment**_  in each changeset to describe the purpose of it. 

### Labels ([new contexts](https://www.liquibase.org/blog/contexts-vs-labels))
Labels are tags that you can add to changesets to control which changeset will be executed in any migration run. Labels control whether a changeset is executed depending on runtime settings. Any string can be used for the label name, and it is case-insensitive.  

This feature can be util, for example, for insert diferents users according to the environment

**proposed names**  
labels="local, staging, uat, production, test"

**Helpful links**  
[Liquibase Labels](https://docs.liquibase.com/concepts/advanced/labels.html)

# Useful Links
[liquibase database migration medium](https://saikris12.medium.com/liquibase-database-migration-3de9be58ab16)  
[liquibase with springboot and maven](https://docs.liquibase.com/tools-integrations/springboot/using-springboot-with-maven.html)  
[hibernate plguin example](https://www.baeldung.com/liquibase-refactor-schema-of-java-app)  
[liquibase properties file](https://docs.liquibase.com/workflows/liquibase-community/creating-config-properties.html)  
[DATABASECHANGELOG table](https://docs.liquibase.com/concepts/databasechangelog-table.html?_ga=2.250196550.1513561886.1608058303-2015314669.1608058303)  
[changeSet](https://docs.liquibase.com/concepts/changeset.html?_ga=2.250196550.1513561886.1608058303-2015314669.1608058303)  
[Liquibase Best Practices](https://www.liquibase.org/get-started/best-practices)  
[liquibase tag](https://docs.liquibase.com/commands/community/tag.html)  
[Rollback](https://www.baeldung.com/liquibase-rollback)  
[Tag version for rollback example](https://www.eandbsoftware.org/liquibase-use-case-example/)

###Actuator
if are spring boot actuator enabled, you can view the liquibase metrics in
http://localhost:8080/actuator/liquibase