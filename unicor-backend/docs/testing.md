# Atix Java Spring Template - TESTING

## Getting Started

Here you find all realated backend tests.

### Info

currently we use [H2](https://www.h2database.com/html/main.html])  
but we must migrate to use [testcontainers](https://www.testcontainers.org/)


## Running the tests

./mvnw test


## Examples
Link code examples here