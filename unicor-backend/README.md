# UNICOR
Unicor is a platform that allows you to know and valorate the opinion of people regarding certain places grouped by super squares.
The solution allows to know the opinion of the people through the analysis by [npl](https://cloud.google.com/natural-language) of the public reviews/messages that they make about certain places in the different social media.  


This is the Unicor backend projects.  


## Getting Started
- [Run backend on develop](#run-backend-on-develop)
    - [Database Engine and pgadmin](#database-engine-And-pgadmin)  
        - [config pgadmin](#config-pgadmin)
    - [Run app](#run-app) 
    - [prerequisites](#prerequisites) 
  
- [Run frontend on develop](#run-frontend-on-develop)
  - [prerequisites front](#prerequisites-front)


## Run backend on develop
### Database Engine and pgadmin
- `cd unicor-backend/docker`  
- `sudo docker-compose -f develop.docker-compose.yml up`

### Config PgAdmin
- on a browser set localhost:8088
- login with unicor@atixlabs.com, unicor
- add new server 
    - server name: `db_unicor`
    - port: `5432`
    - database, user, pass: `unicor`  
    
### Run web app   
- only first time
    - on backend root run `mvn -N io.takari:maven:wrapper`  
- `./mvnw install` for update dependencies
- `./mvnw test` for run tests
- `./mvnw spring-boot:run` for run app  _(localhost:8080/appversion for check)_

### Scrapper
based on [googlemaps-scrapper](https://github.com/gaspa93/googlemaps-scraper) 
 
#### Chromedriver
 - Download ChromeDriver and unzip it
    - `wget -N https://chromedriver.storage.googleapis.com/index.html?path=2.45/chromedriver_linux64.zip -P ~/Downloads`
    - `unzip ~/Downloads/chromedriver_linux64.zip -d ~/Downloads`
 - Make it executable and move to /usr/local/share
    - `chmod +x ~/Downloads/chromedriver`
    - `sudo mv -f ~/Downloads/chromedriver /usr/local/share/chromedriver`
 - Create symbolic links
    - `sudo ln -s /usr/local/share/chromedriver /usr/local/bin/chromedriver`
    - `sudo ln -s /usr/local/share/chromedriver /usr/bin/chromedriver`

#### Chrome
The script need this specific version: [google-chrome-stable_71.0.3578.98-1](http://170.210.201.179/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_71.0.3578.98-1_amd64.deb)
 
#### Requirements 
  -  run `pip3 install -r src/main/resources/python/googlemaps-scraper/requirements.txt`
 
### Prerequisites
[docker engine installed](https://docs.docker.com/engine/install/)     
[docker compose installed](https://docs.docker.com/compose/install/)  
[maven](https://linuxize.com/post/how-to-install-apache-maven-on-ubuntu-18-04/)  
[openJDK 11](https://openjdk.java.net/projects/jdk/11/)  
[Postgres 11.1](https://www.postgresql.org/docs/11/index.html)  
[python 3.6 (or greater)](https://www.python.org/downloads/)  
[Chrome Driver 2.45](https://chromedriver.storage.googleapis.com/index.html?path=2.45/)  
[Google Chrome 71.0.3578.98-1](http://170.210.201.179/linux/chrome/deb/pool/main/g/google-chrome-stable/google-chrome-stable_71.0.3578.98-1_amd64.deb)
 

