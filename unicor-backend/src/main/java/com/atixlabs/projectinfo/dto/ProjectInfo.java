package com.atixlabs.projectinfo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectInfo {
  private String applicationName;
  private String buildVersion;
  private String buildTimestamp;
}
