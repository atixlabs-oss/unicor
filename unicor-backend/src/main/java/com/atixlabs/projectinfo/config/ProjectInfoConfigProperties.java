package com.atixlabs.projectinfo.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "project-info")
@Getter
@Setter
public class ProjectInfoConfigProperties {

  private String applicationName;
  private String buildVersion;
  private String buildTimestamp;
}
