package com.atixlabs.projectinfo.controllers;

import com.atixlabs.projectinfo.config.ProjectInfoConfigProperties;
import com.atixlabs.projectinfo.dto.ProjectInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/projectinfo")
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET})
public class ProjectInfoController {

  @Autowired private ProjectInfoConfigProperties projectInfoConfigProperties;

  @GetMapping("/buildversion")
  public ResponseEntity<?> getVersion() {
    return ResponseEntity.ok(projectInfoConfigProperties.getBuildVersion());
  }

  @GetMapping("/builtimestamp")
  public ResponseEntity<?> getTimestamp() {
    return ResponseEntity.ok(projectInfoConfigProperties.getBuildTimestamp());
  }

  @GetMapping("/applicationname")
  public ResponseEntity<?> getAplicationName() {
    return ResponseEntity.ok(projectInfoConfigProperties.getApplicationName());
  }

  @GetMapping
  public ResponseEntity<ProjectInfo> getProjectInfo() {
    return ResponseEntity.ok(
        ProjectInfo.builder()
            .applicationName(projectInfoConfigProperties.getApplicationName())
            .buildVersion(projectInfoConfigProperties.getBuildVersion())
            .buildTimestamp(projectInfoConfigProperties.getBuildTimestamp())
            .build());
  }
}
