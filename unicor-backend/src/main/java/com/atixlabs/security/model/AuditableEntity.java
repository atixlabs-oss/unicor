package com.atixlabs.security.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
@Setter
@Getter
public abstract class AuditableEntity implements Serializable {

  @JsonIgnore
  @Column(name = "CREATED", updatable = false)
  @CreationTimestamp
  private LocalDateTime created;

  @JsonIgnore
  @Column(name = "UPDATED")
  @UpdateTimestamp
  protected LocalDateTime updated;
}
