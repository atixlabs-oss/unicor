package com.atixlabs.security.model;

import com.atixlabs.security.enums.RoleCode;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Builder
@Table(name = "role")
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(unique = true, nullable = false, length = 20)
  @Enumerated(value = EnumType.STRING)
  private RoleCode code;

  private String description;

  @JsonIgnore
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
      name = "role_permission",
      joinColumns = {@JoinColumn(name = "ID_ROLE")},
      inverseJoinColumns = {@JoinColumn(name = "ID_PERMISSION")})
  private Set<Permission> permissions;

  @JsonIgnore
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
      name = "role_menu",
      joinColumns = {@JoinColumn(name = "ID_ROLE")},
      inverseJoinColumns = {@JoinColumn(name = "ID_MENU")})
  private Set<Menu> menus;

  @JsonIgnore
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(
      name = "role_action",
      joinColumns = {@JoinColumn(name = "ID_ROLE")},
      inverseJoinColumns = {@JoinColumn(name = "ID_ACTION")})
  private Set<Action> actions;

  public Role(
      Integer id,
      RoleCode code,
      String description,
      Set<Permission> permissions,
      Set<Menu> menus,
      Set<Action> actions) {
    this.id = id;
    this.code = code;
    this.description = description;
    this.permissions = permissions;
    this.menus = menus;
    this.actions = actions;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Role role = (Role) o;
    return Objects.equals(code, role.code);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, code, description, permissions, menus);
  }
}
