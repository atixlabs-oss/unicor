package com.atixlabs.security.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Getter
@Setter
@Builder
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User extends AuditableEntity {

  private static final PasswordEncoder pwEncoder = new BCryptPasswordEncoder();

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "email", nullable = false, unique = true)
  private String email;

  @Transient @JsonIgnore private boolean isPasswordUpdated = false;

  @JsonIgnore private String password;

  @JoinColumn(name = "id_role")
  @ManyToOne
  private Role role;

  private boolean active;

  public User() {
    this.active = true;
  }

  public User(Long id, String email, String password, Role role) {
    this.id = id;
    this.email = email;
    this.password = password;
    this.role = role;
    this.active = true;
  }

  @PrePersist
  public void prePersist() {
    password = pwEncoder.encode(password);
  }

  @PreUpdate
  public void preUpdate() {
    if (this.isPasswordUpdated) {
      password = pwEncoder.encode(password);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return active == user.active
        && Objects.equals(id, user.id)
        && Objects.equals(email, user.email)
        && role.equals(user.role);
  }

  public void setPassword(String password) {
    this.password = password;
    this.isPasswordUpdated = true;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, email, password, role, active);
  }
}
