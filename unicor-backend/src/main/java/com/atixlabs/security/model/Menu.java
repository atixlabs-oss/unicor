package com.atixlabs.security.model;

import com.atixlabs.security.model.enums.MenuType;
import com.google.common.collect.Sets;
import java.util.Collection;
import javax.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Entity
@Table(name = "menu")
public class Menu {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false)
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "id_parent")
  private Menu parent;

  @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true)
  private Collection<Menu> items = Sets.newHashSet();

  private String name;

  private String code;

  private String uri;

  private String requestType;

  private String icon;

  @Column(name = "item_order")
  private Integer order;

  @Column(nullable = false, length = 30)
  @Enumerated(value = EnumType.STRING)
  private MenuType menuType = MenuType.PRIMARY;

  public boolean isMainMenu() {
    return parent == null;
  }

  public Menu() {}

  public Menu(
      Integer id,
      Menu parent,
      Collection<Menu> items,
      String name,
      String code,
      String uri,
      String requestType,
      String icon,
      Integer order,
      MenuType menuType) {
    this.id = id;
    this.parent = parent;
    this.items = items;
    this.name = name;
    this.code = code;
    this.uri = uri;
    this.requestType = requestType;
    this.icon = icon;
    this.order = order;
    this.menuType = menuType;
  }
}
