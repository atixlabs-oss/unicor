package com.atixlabs.security.model;

import com.atixlabs.security.enums.ActionCode;
import javax.persistence.*;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "action")
public class Action {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(nullable = false)
  private Long id;

  @Column(unique = true, nullable = false, length = 20)
  @Enumerated(value = EnumType.STRING)
  private ActionCode code;

  private String description;
}
