package com.atixlabs.security.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Entity
@Table(name = "permission")
public class Permission extends AuditableEntity {

  @Id
  @Column(unique = true)
  private String code;

  private String description;

  public Permission() {}

  public Permission(String code, String description) {
    this.code = code;
    this.description = description;
  }
}
