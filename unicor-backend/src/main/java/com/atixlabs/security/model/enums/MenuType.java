package com.atixlabs.security.model.enums;

public enum MenuType {
  PRIMARY,
  SECONDARY
}
