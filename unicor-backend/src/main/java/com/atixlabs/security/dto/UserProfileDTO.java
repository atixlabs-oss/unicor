package com.atixlabs.security.dto;

import com.atixlabs.security.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileDTO {

  private Long id;

  private String email;

  public UserProfileDTO(User user) {
    this.id = user.getId();
    this.email = user.getEmail();
  }
}
