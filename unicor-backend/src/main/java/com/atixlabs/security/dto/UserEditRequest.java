package com.atixlabs.security.dto;

import com.atixlabs.security.model.annotations.ValidPassword;
import java.io.Serializable;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserEditRequest implements Serializable {

  private Long id;

  private String password;

  @ValidPassword private String confirmPassword;

  private String email;

  private String role;
}
