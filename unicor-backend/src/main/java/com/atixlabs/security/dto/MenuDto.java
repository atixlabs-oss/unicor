package com.atixlabs.security.dto;

import com.atixlabs.security.model.Menu;
import com.google.common.collect.Sets;
import java.util.Collection;
import lombok.Getter;

@Getter
public class MenuDto {

  private String key;

  private String description;

  private Collection<MenuItemDto> items;

  private String url;

  private String icon;

  private Integer order;

  public MenuDto(Menu menu) {
    this.key = menu.getCode();
    this.description = menu.getName();
    this.url = menu.getUri();
    this.icon = menu.getIcon();
    this.order = menu.getOrder();
    items = Sets.newHashSet();
  }
}
