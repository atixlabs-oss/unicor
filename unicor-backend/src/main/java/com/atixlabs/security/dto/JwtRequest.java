package com.atixlabs.security.dto;

import java.io.Serializable;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@Getter
@Setter
public class JwtRequest implements Serializable {

  private String email;
  private String password;

  public JwtRequest(String email, String password) {
    this.setEmail(email);
    this.setPassword(password);
  }
}
