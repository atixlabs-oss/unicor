package com.atixlabs.security.dto;

import com.atixlabs.security.model.Role;
import com.atixlabs.security.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserGridDTO {

  private Long id;

  private String email;

  private Role role;

  private boolean active;

  public UserGridDTO(User user) {
    this.id = user.getId();
    this.email = user.getEmail();
    this.role = user.getRole();
    this.active = user.isActive();
  }
}
