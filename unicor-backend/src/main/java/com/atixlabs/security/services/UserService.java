package com.atixlabs.security.services;

import static com.atixlabs.security.repositories.specifications.UserSpecifications.byFilter;
import static com.atixlabs.security.utils.ObjectMapper.mergeObjects;

import com.atixlabs.security.dto.FilterUserDto;
import com.atixlabs.security.dto.UserEditRequest;
import com.atixlabs.security.dto.UserGridDTO;
import com.atixlabs.security.dto.UserProfileDTO;
import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.exceptions.*;
import com.atixlabs.security.model.Role;
import com.atixlabs.security.model.User;
import com.atixlabs.security.repositories.RoleRepository;
import com.atixlabs.security.repositories.UserRepository;
import com.atixlabs.security.utils.PasswordConstraintValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import java.util.*;
import javax.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.RuleResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService {

  private static final String role_code_frontend = "role";

  private static final String type_code_frontend = "type";

  private static final String countryOrZone_code_frontend = "selectedKey";

  private UserRepository repository;

  private RoleRepository roleRepository;

  private PasswordEncoder passwordEncoder;

  private final PasswordConstraintValidator passwordConstraintValidator;

  @Autowired
  public UserService(
      UserRepository userRepository,
      RoleRepository roleRepository,
      PasswordEncoder passwordEncoder,
      PasswordConstraintValidator passwordConstraintValidator) {
    this.repository = userRepository;
    this.roleRepository = roleRepository;
    this.passwordEncoder = passwordEncoder;
    this.passwordConstraintValidator = passwordConstraintValidator;
  }

  public List<User> findAll() {
    return Lists.newArrayList(repository.findAll());
  }

  public User findById(Long id) {
    return repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
  }

  public User setActive(Long id, Boolean active) {
    log.info("Set active {} to user {}", active, id);
    return repository
        .findById(id)
        .map(
            user -> {
              user.setActive(active);
              return repository.save(user);
            })
        .orElseThrow(() -> new EntityNotFoundException("User does not exist."));
  }

  public User disable(Long id) {
    return this.setActive(id, false);
  }

  public User enable(Long id) {
    return this.setActive(id, true);
  }

  public User createOrEdit(UserEditRequest userResponse)
      throws ExistUserException, UsernameAlreadyExistsException, PasswordNotMatchException {
    User user = new User();
    if (userResponse.getId() != null) {
      user =
          repository
              .findById(userResponse.getId())
              .orElseThrow(() -> new UserNotFoundException(userResponse.getId()));
    }

    if (!isNullOrEmpty(userResponse.getPassword())
        || !isNullOrEmpty(userResponse.getConfirmPassword())) {
      if (!userResponse.getPassword().equals(userResponse.getConfirmPassword()))
        throw new PasswordNotMatchException();
      String password = userResponse.getPassword();
      RuleResult result = this.isValidPassword(password);
      if (result.isValid()) {
        user.setPassword(password);
      } else {
        throw new InvalidPasswordException();
      }
    }

    this.validateUsername(userResponse);

    Role role = roleRepository.findByCode(RoleCode.valueOf(userResponse.getRole()));
    user.setEmail(userResponse.getEmail());
    user.setRole(role);
    return repository.save(user);
  }

  private RuleResult isValidPassword(String password) {
    PasswordValidator validator = passwordConstraintValidator.getPasswordValidator();
    return validator.validate(new PasswordData(password));
  }

  public void validateUsername(UserEditRequest userResponse) throws UsernameAlreadyExistsException {
    Optional<User> opUser = repository.findByEmail(userResponse.getEmail());
    if (opUser.isPresent()) {
      if (userResponse.getId() != opUser.get().getId()) throw new UsernameAlreadyExistsException();
    }
  }

  public Optional<User> findByUsername(String username) {
    return repository.findByEmail(username);
  }

  public Optional<User> updateUser(Map<String, Object> updates, Long id) throws Exception {

    User existentUser;
    Optional<String> password = Optional.empty();
    Role role = null;

    if (id == null || id < 1) return Optional.empty();
    existentUser = repository.findById(id).orElseThrow(() -> new InexistentUserException());

    if (updates.containsKey(role_code_frontend)) {
      role =
          roleRepository.findByCode(RoleCode.valueOf(updates.get(role_code_frontend).toString()));
    }

    if (updates.containsKey("password")
        && !passwordEncoder.matches(
            updates.get("password").toString(), existentUser.getPassword())) {
      log.warn(
          String.format(
              "Error changed password. Password does not match for mail %s",
              existentUser.getEmail()));
      throw new PasswordNotMatchException();
    }
    if (updates.containsKey("newPassword") && updates.containsKey("confirmNewPassword")) {
      if (updates.get("newPassword").equals(updates.get("confirmNewPassword"))) {
        password = Optional.of(passwordEncoder.encode(updates.get("newPassword").toString()));
      }
    }

    Optional<User> tempUser = repository.findByEmail(updates.get("email").toString());
    if (updates.containsKey("email")
        && tempUser.isPresent()
        && updates.get("email").equals(existentUser.getEmail())
        && !tempUser.get().getId().equals(id)) {
      log.warn("Email already exists");
      return Optional.empty();
    }

    User updateUser;
    updates = parseUpdatesForUser(updates);
    updateUser = new ObjectMapper().convertValue(updates, User.class);
    if (role != null) updateUser.setRole(role);
    password.ifPresent(updateUser::setPassword);

    User third;
    try {
      third = mergeObjects(updateUser, existentUser);
      repository.save(third);
      return Optional.of(third);
    } catch (Exception ex) {
      log.error(Arrays.toString(ex.getStackTrace()));
      return Optional.empty();
    }
  }

  public void updateUsers(List<User> users) {
    repository.saveAll(users);
  }

  private Map<String, Object> parseUpdatesForUser(Map<String, Object> updates) {
    updates.remove(type_code_frontend);
    updates.remove(countryOrZone_code_frontend);
    updates.remove(role_code_frontend);
    updates.remove("newPassword");
    updates.remove("confirmNewPassword");
    return updates;
  }

  public Collection<User> findAll(FilterUserDto filter) {
    return repository.findAll(byFilter(filter));
  }

  public Page<UserGridDTO> findUsersFilteredAndPaginated(FilterUserDto filter, Pageable page) {
    return repository.findAll(byFilter(filter), page).map(UserGridDTO::new);
  }

  private boolean isNullOrEmpty(String word) {
    return (word == null || StringUtils.isEmpty(word));
  }

  public List<User> getAll() {
    return repository.findAll();
  }

  public void putDisableOnUsers(List<User> users) {
    users.forEach(user -> user.setActive(false));
  }

  public UserProfileDTO getProfile(Long id) {
    User user = this.findById(id);
    return new UserProfileDTO(user);
  }

  public List<User> findAllActiveAdmins() {
    return repository.findByActiveAndRoleCode(true, RoleCode.ROLE_ADMIN);
  }

  public List<User> findAllActiveUsers() {
    return repository.findByActive(true);
  }
}
