package com.atixlabs.security.services;

import com.atixlabs.security.configuration.CustomUser;
import com.atixlabs.security.model.User;
import com.atixlabs.security.repositories.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

  @Autowired private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    Optional<User> opUser = userRepository.findByEmail(username);

    if (opUser.isPresent()) {
      User user = opUser.get();
      List<GrantedAuthority> authorities = new ArrayList<>();
      CustomUser userDetails = CustomUser.create(user);

      return userDetails;
      // return new User("user", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6",
      //       new ArrayList<>());
    } else {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
  }
}
