package com.atixlabs.security.services;

import com.atixlabs.security.dto.AuthenticatedUserDto;
import com.atixlabs.security.dto.MenuDto;
import com.atixlabs.security.dto.MenuItemDto;
import com.atixlabs.security.dto.NavbarUserDto;
import com.atixlabs.security.exceptions.InactiveUserException;
import com.atixlabs.security.model.Menu;
import com.atixlabs.security.model.Permission;
import com.atixlabs.security.model.Role;
import com.atixlabs.security.model.User;
import com.atixlabs.security.model.enums.MenuType;
import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserPermissionsService {

  @Autowired private UserService userService;

  @Autowired private PasswordEncoder passwordEncoder;

  @Transactional
  public Optional<AuthenticatedUserDto> findUserAuthenticated(String username, String password)
      throws InactiveUserException {
    Optional<User> user = userService.findByUsername(username);
    if (user.isPresent() && passwordEncoder.matches(password, user.get().getPassword())) {
      if (!user.get().isActive()) {
        throw new InactiveUserException(user.get());
      }
      AuthenticatedUserDto authenticatedUser = new AuthenticatedUserDto();
      authenticatedUser.setId(user.get().getId());
      authenticatedUser.setUsername(username);
      authenticatedUser.setEmail(user.get().getEmail());
      authenticatedUser.setRole(user.get().getRole().getDescription());
      authenticatedUser.setPermissions(
          Sets.newHashSet(this.findPermissionsByRole(user.get().getRole())));
      authenticatedUser.setActions(Sets.newHashSet(this.findActionsByRole(user.get().getRole())));
      authenticatedUser.setPrimaryNavbar(
          NavbarUserDto.builder().menus(Sets.newLinkedHashSet()).build());
      authenticatedUser.setSecondaryNavbar(
          NavbarUserDto.builder().menus(Sets.newLinkedHashSet()).build());

      Collection<Menu> navbar = user.get().getRole().getMenus();
      for (Menu menu : navbar) {
        if (menu.isMainMenu()) {
          MenuDto menuDto = new MenuDto(menu);
          menuDto
              .getItems()
              .addAll(
                  navbar.stream()
                      .filter(m -> !m.isMainMenu() && m.getParent().equals(menu))
                      .sorted(Comparator.comparingInt(Menu::getOrder))
                      .map(MenuItemDto::new)
                      .collect(Collectors.toList()));
          if (menu.getMenuType().equals(MenuType.PRIMARY)) {
            authenticatedUser.getPrimaryNavbar().getMenus().add(menuDto);
          } else {
            authenticatedUser.getSecondaryNavbar().getMenus().add(menuDto);
          }
        }
      }
      return Optional.of(authenticatedUser);
    }

    return Optional.empty();
  }

  private Collection<String> findPermissionsByRole(Role role) {
    return role.getPermissions().stream().map(Permission::getCode).collect(Collectors.toList());
  }

  private Collection<String> findActionsByRole(Role role) {
    return role.getActions().stream()
        .map(action -> action.getCode().action())
        .collect(Collectors.toList());
  }

  public Collection<String> findPermissions(Long id) {
    User user = userService.findById(id);
    return findPermissionsByRole(user.getRole());
  }

  public Set<MenuDto> findNavbar(Long id) {
    User user = userService.findById(id);
    return findNavbarDto(user.getRole());
  }

  private Set<MenuDto> findNavbarDto(Role role) {
    Set<MenuDto> navbar = Sets.newHashSet();
    for (Menu menu : role.getMenus()) {
      if (menu.isMainMenu()) {
        MenuDto menuDto = new MenuDto(menu);
        menuDto
            .getItems()
            .addAll(menu.getItems().stream().map(MenuItemDto::new).collect(Collectors.toList()));
        navbar.add(menuDto);
      }
    }
    return navbar;
  }
}
