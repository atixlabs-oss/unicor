package com.atixlabs.security.services;

import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.model.Role;
import com.atixlabs.security.repositories.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RoleService {

  @Autowired RoleRepository roleRepository;

  public Role findByCode(RoleCode role) {
    return roleRepository.findByCode(role);
  }

  public Role save(Role role) {
    return roleRepository.save(role);
  }

  public Boolean existsByCode(RoleCode code) {
    return roleRepository.existsByCode(code);
  }
}
