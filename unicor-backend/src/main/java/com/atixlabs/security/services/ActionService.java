package com.atixlabs.security.services;

import com.atixlabs.security.enums.ActionCode;
import com.atixlabs.security.model.Action;
import com.atixlabs.security.repositories.ActionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActionService {

  @Autowired ActionRepository actionRepository;

  public Action findByCode(ActionCode action) {
    return actionRepository.findByCode(action);
  }

  public List<Action> findAll() {
    return actionRepository.findAll();
  }

  public Action createAction(Action action) {
    return actionRepository.save(action);
  }

  public Boolean existsByCode(ActionCode code) {
    return actionRepository.existsByCode(code);
  }
}
