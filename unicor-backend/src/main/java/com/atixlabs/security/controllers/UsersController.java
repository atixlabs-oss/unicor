package com.atixlabs.security.controllers;

import static org.springframework.http.ResponseEntity.status;

import com.atixlabs.security.configuration.CustomUser;
import com.atixlabs.security.dto.FilterUserDto;
import com.atixlabs.security.dto.MenuDto;
import com.atixlabs.security.dto.UserEditRequest;
import com.atixlabs.security.dto.UserGridDTO;
import com.atixlabs.security.exceptions.*;
import com.atixlabs.security.model.User;
import com.atixlabs.security.services.UserPermissionsService;
import com.atixlabs.security.services.UserService;
import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@Validated
@RequestMapping(UsersController.URL_MAPPING_USERS)
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
public class UsersController {

  public static final String URL_MAPPING_USERS = "/users";

  @Autowired private UserService userService;

  @Autowired private UserPermissionsService userPermissionsService;

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/filters")
  public Page<UserGridDTO> findAllWithFilters(
      @RequestParam("page") @Min(0) @Max(9999999) int page,
      @RequestParam("size") @Min(1) @Max(9999) int size,
      @RequestParam(required = false) Boolean enabled,
      @RequestParam(required = false) String search,
      @RequestParam(required = false) String role) {
    Pageable pageRequest = PageRequest.of(page, size, Sort.by("email").ascending());
    return userService.findUsersFilteredAndPaginated(
        FilterUserDto.builder()
            .search(search)
            .enabled(Optional.ofNullable(enabled))
            .role(Optional.ofNullable(role))
            .build(),
        pageRequest);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping
  public Collection<User> findAll() {
    return Lists.newArrayList(userService.getAll());
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping(params = {"page", "size"})
  public Page<UserGridDTO> findPaginated(
      @AuthenticationPrincipal CustomUser currentUser,
      @RequestParam("page") @Min(0) @Max(9999999) int page,
      @RequestParam("size") @Min(1) @Max(9999) int size,
      @RequestParam(required = false) String searchCriteria) {
    Optional<Long> ignoreId =
        (currentUser != null && currentUser.getId() != null)
            ? Optional.of(currentUser.getId())
            : Optional.empty();
    Pageable pageRequest = PageRequest.of(page, size, Sort.by("email").ascending());
    return userService.findUsersFilteredAndPaginated(
        FilterUserDto.builder().id(ignoreId).search(searchCriteria).build(), pageRequest);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping
  public ResponseEntity<?> createUser(@RequestBody @Valid UserEditRequest user) {
    try {
      return status(HttpStatus.CREATED).body(userService.createOrEdit(user));
    } catch (UsernameAlreadyExistsException
        | PasswordNotMatchException
        | InvalidPasswordException
        | ExistUserException ex) {
      log.error("Error when creating a user: {}", ex.getMessage(), ex);
      return status(HttpStatus.PRECONDITION_FAILED).body(ex.getMessage());
    }
  }

  @PreAuthorize("@permission.hasRole(authentication.principal, 'ROLE_ADMIN')")
  @GetMapping("/{id}")
  public ResponseEntity<?> findById(@PathVariable @Min(1) Long id) {
    try {
      return status(HttpStatus.OK).body(userService.findById(id));
    } catch (UserNotFoundException ex) {
      log.error("Error to get a user: {}", ex.getMessage(), ex);
      return status(HttpStatus.NOT_FOUND).body(null);
    }
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/disable/{id}")
  public void disable(@PathVariable @Min(1) Long id) {
    userService.disable(id);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/enable/{id}")
  public void enable(@PathVariable @Min(1) Long id) {
    userService.enable(id);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PutMapping("/{id}/active/{active}")
  public ResponseEntity<?> active(
      @PathVariable("id") @Min(1) Long id, @PathVariable("active") Boolean active) {
    try {
      userService.setActive(id, active);
      log.info("User id {} update active state to {}", id, active);
      return ResponseEntity.status(HttpStatus.OK).body(String.format("User %s updated", id));
    } catch (EntityNotFoundException e) {
      log.error("Cant update User id {} active state to {}, User not found", id, active, e);
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body(String.format("User %s not exist", id));
    }
  }

  @GetMapping("/permissions/{id}")
  public Collection<String> findPermissions(@PathVariable @Min(1) Long id) {
    return userPermissionsService.findPermissions(id);
  }

  @GetMapping("/navbar")
  public Collection<MenuDto> getNavbar(@AuthenticationPrincipal CustomUser currentUser) {
    return userPermissionsService.findNavbar(currentUser.getId());
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PutMapping("/{id}")
  public ResponseEntity<?> putUser(
      @PathVariable @Min(1) Long id, @RequestBody UserEditRequest updatedUser) {
    try {
      userService.findById(id);
    } catch (UsernameNotFoundException ex) {
      return status(HttpStatus.NOT_FOUND).body(null);
    }
    try {
      updatedUser.setId(id);
      return status(HttpStatus.OK).body(userService.createOrEdit(updatedUser));
    } catch (UsernameAlreadyExistsException
        | PasswordNotMatchException
        | InvalidPasswordException
        | ExistUserException ex) {
      log.error("Error when creating a user: {}", ex.getMessage(), ex);
      return status(HttpStatus.PRECONDITION_FAILED).body(ex.getMessage());
    }
  }

  // TODO: check password to see if it is valid, maybe map to a @validpassword attribute
  // in a new class.
  @PreAuthorize("@permission.canUpdate(#currentUser, #id,'ROLE_ADMIN' )")
  @PatchMapping("/{id}")
  public ResponseEntity<String> updateUser(
      @AuthenticationPrincipal CustomUser currentUser,
      @PathVariable @Min(1) Long id,
      @RequestBody Map<String, Object> updates)
      throws Exception {
    Optional<User> updatedUser;
    try {
      updatedUser = userService.updateUser(updates, id);
    } catch (InexistentUserException ex) {
      return status(HttpStatus.NOT_FOUND).body("User not found");
    } catch (PasswordNotMatchException ex) {
      return status(HttpStatus.PRECONDITION_FAILED).body("Passwords do not match.");
    }

    if (!updatedUser.isPresent()) return status(HttpStatus.BAD_REQUEST).body(null);

    return status(HttpStatus.OK).body("Updated succesfully");
  }

  @GetMapping("/profile")
  public ResponseEntity<?> getProfile(@AuthenticationPrincipal CustomUser currentUser) {
    try {
      return status(HttpStatus.OK).body(userService.getProfile(currentUser.getId()));
    } catch (UserNotFoundException ex) {
      log.error("Cant get profile of User id {} ", currentUser.getId(), ex);
      return status(HttpStatus.NOT_FOUND).body("User not found");
    }
  }
}
