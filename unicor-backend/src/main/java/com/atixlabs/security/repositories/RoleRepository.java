package com.atixlabs.security.repositories;

import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

  Role findByCode(RoleCode code);

  Boolean existsByCode(RoleCode code);
}
