package com.atixlabs.security.repositories;

import com.atixlabs.security.enums.ActionCode;
import com.atixlabs.security.model.Action;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActionRepository extends JpaRepository<Action, Long> {

  Action findByCode(ActionCode code);

  List<Action> findAll();

  Action save(Action action);

  Boolean existsByCode(ActionCode code);
}
