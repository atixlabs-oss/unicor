package com.atixlabs.security.repositories;

import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.model.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
    extends JpaRepository<User, Long>,
        JpaSpecificationExecutor<User>,
        PagingAndSortingRepository<User, Long> {

  @Override
  Page<User> findAll(Pageable pageable);

  List<User> findByActiveAndRoleCode(boolean active, RoleCode roleAdmin);

  List<User> findByActive(boolean active);

  Optional<User> findByEmail(String email);
}
