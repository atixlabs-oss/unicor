package com.atixlabs.security.repositories;

import com.atixlabs.security.model.Menu;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Integer> {

  Optional<Menu> findByCode(String code);
}
