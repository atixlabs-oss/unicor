package com.atixlabs.security.repositories.specifications;

import com.atixlabs.security.dto.FilterUserDto;
import com.atixlabs.security.model.User;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import java.util.List;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

public abstract class UserSpecifications {
  public static Specification<User> byFilter(FilterUserDto filter) {
    return (Specification<User>)
        (root, query, cb) -> {
          List<Predicate> predicates = Lists.newLinkedList();
          if (!Strings.isNullOrEmpty(filter.getSearch())) {
            String search = "%".concat(filter.getSearch()).concat("%").toUpperCase();
            predicates.add(cb.or(cb.like(cb.upper(root.get("email")), search)));
          }
          if (filter.getId() != null && filter.getId().isPresent()) {
            predicates.add(cb.notEqual(root.get("id"), filter.getId().get()));
          }
          if (filter.getEnabled() != null && filter.getEnabled().isPresent()) {
            predicates.add(cb.equal(root.get("active"), filter.getEnabled().get()));
          }
          if ((filter.getRole() != null) && !Strings.isNullOrEmpty(filter.getRole().orElse(null))) {
            String search = "%".concat(filter.getRole().get()).concat("%").toUpperCase();
            predicates.add(cb.like(cb.upper(root.get("role").get("description")), search));
          }
          return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
  }
}
