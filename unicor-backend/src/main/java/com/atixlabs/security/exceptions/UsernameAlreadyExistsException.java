package com.atixlabs.security.exceptions;

public class UsernameAlreadyExistsException extends RuntimeException {
  public UsernameAlreadyExistsException() {
    super("El email ingresado ya se encuentra en uso.");
  }
}
