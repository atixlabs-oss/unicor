package com.atixlabs.security.exceptions;

public class InvalidPasswordException extends RuntimeException {
  public InvalidPasswordException() {
    super("Password is invalid.");
  }
}
