package com.atixlabs.security.enums;

public enum ActionCode {
  CREATE("CREATE"),
  UPDATE("UPDATE"),
  DELETE("DELETE"),
  VIEW("VIEW");

  private String action;

  ActionCode(String action) {
    this.action = action;
  }

  public String action() {
    return this.action;
  }
}
