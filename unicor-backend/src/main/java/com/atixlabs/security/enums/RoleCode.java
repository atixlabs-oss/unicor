package com.atixlabs.security.enums;

import lombok.Getter;

@Getter
public enum RoleCode {
  ROLE_ADMIN(1, "ROLE_ADMIN"),
  ROLE_OPERATOR(2, "ROLE_OPERATOR");

  private Integer id;

  private String code;

  RoleCode(Integer id, String role) {
    this.id = id;
    this.code = role;
  }

  public String role() {
    return this.code;
  }
}
