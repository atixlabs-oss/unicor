package com.atixlabs.security.utils;

import com.atixlabs.security.model.annotations.ValidPassword;
import java.util.Arrays;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.passay.*;
import org.springframework.stereotype.Component;

@Component
public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

  @Override
  public void initialize(ValidPassword arg0) {}

  @Override
  public boolean isValid(String password, ConstraintValidatorContext context) {

    PasswordValidator validator = this.getPasswordValidator();

    RuleResult result = validator.validate(new PasswordData(password));
    if (result.isValid()) {
      return true;
    }
    List<String> messages = validator.getMessages(result);

    String messageTemplate = String.join(",", messages);
    context
        .buildConstraintViolationWithTemplate(messageTemplate)
        .addConstraintViolation()
        .disableDefaultConstraintViolation();
    return false;
  }

  public PasswordValidator getPasswordValidator() {
    return new PasswordValidator(
        Arrays.asList(
            // at least 6 characters
            new LengthRule(6, Integer.MAX_VALUE),

            // at least one digit character
            new CharacterRule(EnglishCharacterData.Digit, 1),
            new CharacterRule(EnglishCharacterData.Alphabetical, 1)));
  }
}
