package com.atixlabs.security.utils;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import java.math.BigDecimal;
import java.util.List;

public class SentimentStateUtils {

  public static SentimentState getState(
      BigDecimal average, List<SentimentStateConfiguration> configuredParameters) {
    // TODO: What if you do not find state?
    return configuredParameters.stream()
        .filter(
            configuration ->
                average.compareTo(configuration.getMinimumValue()) != -1
                    && average.compareTo(configuration.getMaximumValue()) != 1)
        .findFirst()
        .get()
        .getState();
  }
}
