package com.atixlabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtixApplication {

  public static void main(final String[] args) {
    SpringApplication.run(AtixApplication.class, args);
  }
}
