package com.atixlabs.modules.pdfexport.example;

import com.atixlabs.modules.pdfexport.services.PdfService;
import com.atixlabs.security.model.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Setter
@Getter
@Service
public class UserExportPdfServiceExample extends PdfService {

  private User user;

  @Autowired
  public UserExportPdfServiceExample(SpringTemplateEngine templateEngine) {
    super(templateEngine);
  }

  @Override
  protected Context getContext() {
    Context context = new Context();
    context.setVariable("user", this.getUser());
    return context;
  }

  @Override
  protected String getTemplateName() {
    return "user-template-example";
  }

  @Override
  protected String getFileName() {
    return String.format("User%d", getUser().getId());
  }
}
