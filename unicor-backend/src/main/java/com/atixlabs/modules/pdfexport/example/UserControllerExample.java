package com.atixlabs.modules.pdfexport.example;

import com.atixlabs.app.services.dto.ExportResultDTO;
import com.lowagie.text.DocumentException;
import java.io.IOException;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(UserControllerExample.URL_MAPPING)
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET})
public class UserControllerExample {

  public static final String URL_MAPPING = "/example/user";

  private UserServiceExample service;

  @Autowired
  public UserControllerExample(UserServiceExample service) {
    this.service = service;
  }

  @GetMapping("/{id}/export")
  public ResponseEntity<?> exportUserToPDF(@PathVariable @Min(1) Long id) {
    try {
      ExportResultDTO result = service.exportUserToPDF(id);
      return ResponseEntity.ok()
          .contentType(MediaType.APPLICATION_PDF)
          .header(
              HttpHeaders.CONTENT_DISPOSITION,
              String.format("attachment;filename=\"%s.pdf\"", result.getName()))
          .body(result.getResource());
    } catch (IOException | DocumentException ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
    }
  }
}
