package com.atixlabs.modules.pdfexport.services;

import com.atixlabs.app.services.dto.ExportResultDTO;
import com.lowagie.text.DocumentException;
import java.io.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.xhtmlrenderer.pdf.ITextRenderer;

public abstract class PdfService {

  private static final String PDF_RESOURCES = "/pdf-resources/";

  private SpringTemplateEngine templateEngine;

  @Autowired
  public PdfService(SpringTemplateEngine templateEngine) {
    this.templateEngine = templateEngine;
  }

  /**
   * Generate export of the pdf from the template and context.
   *
   * @return the entity ExportResultDTO with the resource and the file name.
   */
  public ExportResultDTO generatePdf() throws IOException, DocumentException {
    Context context = this.getContext();
    String html = this.loadAndFillTemplate(context);
    File file = this.renderPdf(html);
    ByteArrayResource resource = new ByteArrayResource(FileCopyUtils.copyToByteArray(file));
    return new ExportResultDTO(this.getFileName(), resource);
  }

  private File renderPdf(String html) throws IOException, DocumentException {
    File file = File.createTempFile("tempFile", ".pdf");
    OutputStream outputStream = new FileOutputStream(file);
    ITextRenderer renderer = new ITextRenderer(20f * 4f / 3f, 20);
    renderer.setDocumentFromString(
        html, new ClassPathResource(PDF_RESOURCES).getURL().toExternalForm());
    renderer.layout();
    renderer.createPDF(outputStream);
    outputStream.close();
    file.deleteOnExit();
    return file;
  }

  private String loadAndFillTemplate(Context context) {
    return templateEngine.process(this.getTemplateName(), context);
  }

  /**
   * Get the context that will provide the template.
   *
   * @return Context all the variables that will be used within the template.
   */
  protected abstract Context getContext();

  /**
   * Get the name of the template that will be used to export the pdf.
   *
   * @return the name of the template located in the directory indicated in the application
   *     properties.
   */
  protected abstract String getTemplateName();

  /**
   * Get the name of the file to be exported.
   *
   * @return the name of the file to be exported.
   */
  protected abstract String getFileName();
}
