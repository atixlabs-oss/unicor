package com.atixlabs.modules.pdfexport.example;

import com.atixlabs.app.services.dto.ExportResultDTO;
import com.atixlabs.security.exceptions.UserNotFoundException;
import com.atixlabs.security.model.User;
import com.atixlabs.security.repositories.UserRepository;
import com.lowagie.text.DocumentException;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceExample {

  private UserRepository repository;

  private UserExportPdfServiceExample pdfService;

  @Autowired
  public UserServiceExample(UserRepository repository, UserExportPdfServiceExample pdfService) {
    this.repository = repository;
    this.pdfService = pdfService;
  }

  public ExportResultDTO exportUserToPDF(Long id) throws IOException, DocumentException {
    User user = this.findUserById(id);
    pdfService.setUser(user);
    return pdfService.generatePdf();
  }

  private User findUserById(Long id) {
    return repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
  }
}
