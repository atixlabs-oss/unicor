package com.atixlabs.app.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class MessageWordId implements Serializable {

  private Message message;

  private Word word;
}
