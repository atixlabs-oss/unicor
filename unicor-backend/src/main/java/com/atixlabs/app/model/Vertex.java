package com.atixlabs.app.model;

import com.atixlabs.app.model.embedables.Coordinate;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "vertex")
@Entity
public class Vertex {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Embedded private Coordinate coordinate;

  @JoinColumn(name = "id_super_square", nullable = false)
  @ManyToOne
  private SuperSquare superSquare;
}
