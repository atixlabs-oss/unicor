package com.atixlabs.app.model.enums;

public enum SentimentState {
  NO_STATE,
  GOOD,
  NEUTRAL,
  BAD;
}
