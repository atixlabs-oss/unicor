package com.atixlabs.app.model;

import java.util.List;
import javax.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
@Table(name = "super_square")
@Entity
public class SuperSquare extends PlaceEntity {

  @OneToMany(mappedBy = "superSquare", fetch = FetchType.EAGER)
  private List<Vertex> vertices;

  @JoinColumn(name = "id_city", nullable = false)
  @ManyToOne
  private City city;

  @Builder
  public SuperSquare(Long id, String name, List<Vertex> vertices, City city) {
    super(id, name);
    this.vertices = vertices;
    this.city = city;
  }
}
