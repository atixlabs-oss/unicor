package com.atixlabs.app.model.embedables;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PlaceSocialMediaSourceKey implements Serializable {

  @Column(name = "id_place")
  private Long placeId;

  @Column(name = "id_social_media_source")
  private Long socialMediaSourceId;
}
