package com.atixlabs.app.model;

import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "social_media_source")
@Entity
public class SocialMediaSource {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false, unique = true)
  private SocialMediaSourceCode code;

  private String name;
}
