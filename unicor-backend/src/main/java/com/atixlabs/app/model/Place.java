package com.atixlabs.app.model;

import java.util.List;
import javax.persistence.*;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Table(name = "place")
@Entity
public class Place extends PlaceEntity {

  @JoinColumn(name = "id_super_square", nullable = false)
  @ManyToOne
  private SuperSquare superSquare;

  @OneToMany(mappedBy = "place")
  private List<PlaceSocialMediaSource> sources;

  @Builder
  public Place(Long id, String name, SuperSquare superSquare) {
    super(id, name);
    this.superSquare = superSquare;
  }
}
