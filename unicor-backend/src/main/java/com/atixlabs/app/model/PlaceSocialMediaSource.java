package com.atixlabs.app.model;

import com.atixlabs.app.model.embedables.PlaceSocialMediaSourceKey;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PlaceSocialMediaSource {

  @EmbeddedId private PlaceSocialMediaSourceKey id;

  @MapsId("placeId")
  @JoinColumn(name = "id_place")
  @ManyToOne
  private Place place;

  @MapsId("socialMediaSourceId")
  @JoinColumn(name = "id_social_media_source")
  @ManyToOne
  private SocialMediaSource socialMediaSource;

  @Column(columnDefinition = "TEXT", name = "user_key")
  private String userKey;
}
