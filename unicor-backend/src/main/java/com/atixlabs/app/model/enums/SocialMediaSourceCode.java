package com.atixlabs.app.model.enums;

public enum SocialMediaSourceCode {
  GOOGLE_REVIEW,
  TWITTER,
  INSTAGRAM,
  FACEBOOK;
}
