package com.atixlabs.app.model.embedables;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Embeddable
public class Coordinate {

  @Column(nullable = false, precision = 19, scale = 15)
  private BigDecimal latitude;

  @Column(nullable = false, precision = 19, scale = 15)
  private BigDecimal longitude;
}
