package com.atixlabs.app.model.configuration;

import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.security.model.AuditableEntity;
import java.math.BigDecimal;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "sentiment_state_configuration")
@Entity
public class SentimentStateConfiguration extends AuditableEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Enumerated(EnumType.STRING)
  private SentimentState state;

  @Column(name = "maximum_value", nullable = false, precision = 3, scale = 2)
  private BigDecimal maximumValue;

  @Column(name = "minimum_value", nullable = false, precision = 3, scale = 2)
  private BigDecimal minimumValue;
}
