package com.atixlabs.app.model;

import com.atixlabs.security.model.AuditableEntity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import lombok.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "message")
@Entity
public class Message extends AuditableEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @JoinColumn(name = "id_social_media_source", nullable = false)
  @ManyToOne
  private SocialMediaSource source;

  @Column(name = "url", columnDefinition = "TEXT", nullable = false)
  private String url;

  @Column(name = "source_message_id", nullable = false)
  private String sourceMessageId;

  @Column(name = "user_id")
  private String userId;

  private String username;

  @Column(columnDefinition = "TEXT")
  private String avatar;

  private String keyword;

  @Column(columnDefinition = "TEXT", nullable = false)
  private String content;

  private BigDecimal sentiment;

  private BigDecimal magnitude;

  private BigDecimal score;

  private Boolean categorizable;

  @Column(name = "post_date")
  private LocalDateTime postDate;

  @JoinColumn(name = "id_place")
  @ManyToOne
  private Place place;

  @JoinTable(
      name = "message_category",
      joinColumns = {@JoinColumn(name = "id_message")},
      inverseJoinColumns = {@JoinColumn(name = "id_category")})
  @ManyToMany(cascade = CascadeType.MERGE)
  private List<Category> categories = new ArrayList<>();

  @OneToMany(
      mappedBy = "message",
      cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  private List<MessageWord> words = new ArrayList<>();

  public Message(
      String username,
      String postId,
      SocialMediaSource source,
      String content,
      LocalDateTime dateTime,
      Place place,
      String avatar,
      Boolean categorizable,
      String sourceMessageId) {
    this.username = username;
    this.url = postId;
    this.source = source;
    this.content = content;
    this.postDate = dateTime;
    this.place = place;
    this.avatar = avatar;
    this.categorizable = categorizable;
    this.sourceMessageId = sourceMessageId;
  }

  public void addCategory(Category category) {
    this.getCategories().add(category);
  }

  public void addWord(MessageWord word) {
    this.getWords().add(word);
  }
}
