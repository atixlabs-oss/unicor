package com.atixlabs.app.model;

import com.atixlabs.app.services.dto.WordDTO;
import java.math.BigDecimal;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@IdClass(MessageWordId.class)
@Table(name = "word_message")
@Entity
public class MessageWord {

  @Id
  @JoinColumn(name = "id_message")
  @ManyToOne
  private Message message;

  @Id
  @MapsId("wordId")
  @JoinColumn(name = "id_word")
  @ManyToOne
  private Word word;

  private BigDecimal sentiment;

  private BigDecimal magnitude;

  private BigDecimal score;

  public MessageWord(Message message, Word word, WordDTO wordDTO) {
    this.message = message;
    this.word = word;
    this.sentiment = wordDTO.getSentiment();
    this.magnitude = wordDTO.getMagnitude();
    this.score = wordDTO.getScore();
  }
}
