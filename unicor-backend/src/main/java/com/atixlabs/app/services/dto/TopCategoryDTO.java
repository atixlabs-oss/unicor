package com.atixlabs.app.services.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class TopCategoryDTO {
  private String name;
  private Long count;
  private Double average;
  private Double percentage;
  private String state;

  private BigDecimal round(Double value) {
    return new BigDecimal(Double.toString(value)).setScale(2, RoundingMode.HALF_UP);
  }

  public BigDecimal getAverage() {
    return this.round(this.average);
  }

  public BigDecimal getPercentage() {
    return this.round(this.percentage);
  }
}
