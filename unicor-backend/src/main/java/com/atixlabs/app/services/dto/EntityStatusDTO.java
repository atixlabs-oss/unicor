package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.PlaceEntity;
import com.atixlabs.app.model.enums.SentimentState;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@AllArgsConstructor
@Setter
@Getter
public class EntityStatusDTO {

  private Long id;

  private String name;

  private Long numberMessages;

  private SentimentState state;

  private List<CategoryStatusDTO> categories;

  public EntityStatusDTO(PlaceEntity entityPlace, List<CategoryStatusDTO> categories) {
    this.id = entityPlace.getId();
    this.name = entityPlace.getName();
    this.categories = categories;
  }
}
