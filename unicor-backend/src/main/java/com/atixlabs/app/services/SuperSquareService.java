package com.atixlabs.app.services;

import static com.atixlabs.app.repositories.specifications.SuperSquareSpecifications.byId;

import com.atixlabs.app.model.*;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.repositories.SuperSquareRepository;
import com.atixlabs.app.services.auxiliary.GeneralStatusEntity;
import com.atixlabs.app.services.auxiliary.NumberMessagesCount;
import com.atixlabs.app.services.auxiliary.StatusEntity;
import com.atixlabs.app.services.auxiliary.SuperSquareGeneralStatus;
import com.atixlabs.app.services.dto.*;
import com.atixlabs.security.utils.SentimentStateUtils;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Getter
@Service
public class SuperSquareService {

  private SuperSquareRepository superSquareRepository;

  private CategoryService categoryService;

  private PlaceService placeService;

  private WordService wordService;

  private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Value("${app.top-words}")
  private Integer topValue;

  @Autowired
  public SuperSquareService(
      SuperSquareRepository superSquareRepository,
      CategoryService categoryService,
      PlaceService placeService,
      WordService wordService,
      SentimentStateConfigurationService sentimentStateConfigurationService) {
    this.superSquareRepository = superSquareRepository;
    this.categoryService = categoryService;
    this.placeService = placeService;
    this.wordService = wordService;
    this.sentimentStateConfigurationService = sentimentStateConfigurationService;
  }

  public List<SuperSquareListDTO> getSuperSquaresList() {
    List<SuperSquare> superSquares = superSquareRepository.findAll();
    return this.mapToSuperSquareListDTO(superSquares);
  }

  private List<SuperSquareListDTO> mapToSuperSquareListDTO(List<SuperSquare> superSquares) {
    return superSquares.stream().map(SuperSquareListDTO::new).collect(Collectors.toList());
  }

  public List<SuperSquareDTO> getSuperSquares(
      Optional<Long> categoryId, Optional<Long> superSquareId) {
    List<SuperSquare> superSquares = superSquareRepository.findAll(byId(superSquareId));
    List<SuperSquareGeneralStatus> superSquaresStatus =
        superSquareRepository.findSuperSquareStatusBy(categoryId, superSquareId);
    return this.mapToSuperSquareDTO(superSquares, superSquaresStatus);
  }

  private List<SuperSquareDTO> mapToSuperSquareDTO(
      List<SuperSquare> superSquares, List<SuperSquareGeneralStatus> superSquaresStatuses) {
    List<SentimentStateConfiguration> configuredParameters =
        sentimentStateConfigurationService.getConfiguredParameters();
    return superSquares.stream()
        .map(
            superSquare ->
                this.toSquareDTO(superSquare, superSquaresStatuses, configuredParameters))
        .collect(Collectors.toList());
  }

  private SuperSquareDTO toSquareDTO(
      SuperSquare superSquare,
      List<SuperSquareGeneralStatus> superSquaresStatuses,
      List<SentimentStateConfiguration> configuredParameters) {
    return new SuperSquareDTO(
        superSquare,
        this.getPolygon(superSquare),
        this.getStatus(superSquare, superSquaresStatuses, configuredParameters));
  }

  public List<CoordinateDTO> getPolygon(SuperSquare superSquare) {
    List<Vertex> polygon = superSquare.getVertices();
    return this.getCoordinatesAndMapToDTO(polygon);
  }

  private List<CoordinateDTO> getCoordinatesAndMapToDTO(List<Vertex> vertices) {
    return vertices.stream()
        .map(Vertex::getCoordinate)
        .map(CoordinateDTO::new)
        .collect(Collectors.toList());
  }

  public StatusDTO getStatus(
      SuperSquare superSquare,
      List<SuperSquareGeneralStatus> superSquaresStatuses,
      List<SentimentStateConfiguration> configuredParameters) {
    Optional<SuperSquareGeneralStatus> superSquareStatusOp =
        superSquaresStatuses.stream()
            .filter(superSquareStatus -> superSquare.equals(superSquareStatus.getSuperSquare()))
            .findFirst();
    StatusDTO status;
    if (superSquareStatusOp.isPresent()) {
      status =
          new StatusDTO(
              SentimentStateUtils.getState(
                  superSquareStatusOp.get().getAverageScore(), configuredParameters),
              superSquareStatusOp.get().getAverageScore());
    } else {
      status = new StatusDTO(SentimentState.NO_STATE, new BigDecimal("0.0"));
    }
    return status;
  }

  public List<NumberMessagesDTO> getNumberMessages(
      Optional<Long> categoryId, Optional<Long> superSquareId) {
    List<SentimentStateConfiguration> configuredParameters =
        sentimentStateConfigurationService.getConfiguredParameters();

    return configuredParameters.stream()
        .map(
            sentimentStateConfiguration -> {
              NumberMessagesCount numberMessagesCount =
                  superSquareRepository.findMessagesBySuperSquareOrCategory(
                      superSquareId, categoryId, sentimentStateConfiguration);

              return NumberMessagesDTO.builder()
                  .state(sentimentStateConfiguration.getState().name())
                  .numberMessages(numberMessagesCount.getCount())
                  .avarage(numberMessagesCount.getAverage())
                  .build();
            })
        .collect(Collectors.toList());
  }

  public List<EntityStatusDTO> getSuperSquaresStatus(
      Optional<Long> categoryId, Optional<Long> superSquareId) {

    List<EntityStatusDTO> status;
    List<SentimentStateConfiguration> configuredParameters =
        sentimentStateConfigurationService.getConfiguredParameters();

    if (superSquareId.isEmpty()) {
      List<? extends StatusEntity> superSquareStatusList =
          superSquareRepository.findSuperSquaresStatus(categoryId);
      List<? extends PlaceEntity> entitiesPlaces = superSquareRepository.findAll();
      status =
          this.processAndMapToEntityDTO(
              superSquareStatusList, entitiesPlaces, configuredParameters);
      this.setGeneralStateAndNumberMessagesSuperSquares(status, configuredParameters);
    } else {
      List<? extends StatusEntity> placesStatusList =
          superSquareRepository.findSuperSquareStatus(superSquareId.get(), categoryId);
      List<? extends PlaceEntity> entitiesPlaces =
          placeService.findBySuperSquareId(superSquareId.get());
      status =
          this.processAndMapToEntityDTO(placesStatusList, entitiesPlaces, configuredParameters);
      this.setGeneralStateAndNumberMessagesPlaces(
          status, superSquareId.get(), configuredParameters);
    }
    sortListByNumberMessages(status);
    return status;
  }

  public List<EntityStatusDTO> processAndMapToEntityDTO(
      List<? extends StatusEntity> statusEntityList,
      List<? extends PlaceEntity> placeEntities,
      List<SentimentStateConfiguration> configuredParameters) {
    List<EntityStatusDTO> entitiesStatus = new ArrayList<>();
    List<Category> mainCategories = categoryService.findMainCategories();

    // EntityPlace -> SuperSquare or Place
    for (PlaceEntity placeEntity : placeEntities) {
      List<StatusEntity> entitiesStatusFiltered =
          this.filterStatusByEntityPlace(statusEntityList, placeEntity);
      List<CategoryStatusDTO> categories =
          this.mapToCategoryStatus(entitiesStatusFiltered, mainCategories, configuredParameters);
      EntityStatusDTO entityStatusDTO = new EntityStatusDTO(placeEntity, categories);
      entitiesStatus.add(entityStatusDTO);
    }
    return entitiesStatus;
  }

  private List<StatusEntity> filterStatusByEntityPlace(
      List<? extends StatusEntity> statusEntityList, PlaceEntity placeEntity) {
    return statusEntityList.stream()
        .filter(statusEntity -> placeEntity.equals(statusEntity.getEntity()))
        .collect(Collectors.toList());
  }

  private List<CategoryStatusDTO> mapToCategoryStatus(
      List<StatusEntity> statusEntities,
      List<Category> mainCategories,
      List<SentimentStateConfiguration> configuredParameters) {
    List<CategoryStatusDTO> categories =
        this.mapToCategoryStatusDTO(statusEntities, configuredParameters);
    return this.fillMissingCategories(categories, mainCategories);
  }

  private List<CategoryStatusDTO> mapToCategoryStatusDTO(
      List<StatusEntity> statusEntities, List<SentimentStateConfiguration> configuredParameters) {
    return statusEntities.stream()
        .map(
            statusEntity ->
                new CategoryStatusDTO(
                    statusEntity,
                    SentimentStateUtils.getState(
                        statusEntity.getAverageScore(), configuredParameters)))
        .collect(Collectors.toList());
  }

  private List<CategoryStatusDTO> fillMissingCategories(
      List<CategoryStatusDTO> categories, List<Category> mainCategories) {
    List<CategoryStatusDTO> missingCategories =
        this.calculateMissingCategories(mainCategories, categories);
    categories.addAll(missingCategories);
    this.sortCollection(categories);
    return categories;
  }

  List<CategoryStatusDTO> calculateMissingCategories(
      List<Category> mainCategories, List<CategoryStatusDTO> categories) {
    Stream<Category> categoryStream = this.filterPresentCategories(mainCategories, categories);
    return this.mapToCategoryStatusDTO_NoState(categoryStream);
  }

  private Stream<Category> filterPresentCategories(
      List<Category> mainCategories, List<CategoryStatusDTO> categories) {
    List<Long> presentCategories =
        categories.stream().map(CategoryStatusDTO::getId).collect(Collectors.toList());
    return mainCategories.stream()
        .filter(mainCategory -> !presentCategories.contains(mainCategory.getId()));
  }

  private List<CategoryStatusDTO> mapToCategoryStatusDTO_NoState(Stream<Category> categoryStream) {
    return categoryStream
        .map(mainCategory -> new CategoryStatusDTO(mainCategory, SentimentState.NO_STATE))
        .collect(Collectors.toList());
  }

  private void sortCollection(List<CategoryStatusDTO> categories) {
    categories.sort((x, y) -> x.getName().compareToIgnoreCase(y.getName()));
  }

  private void setGeneralStateAndNumberMessagesSuperSquares(
      List<EntityStatusDTO> status, List<SentimentStateConfiguration> configuredParameters) {
    List<? extends GeneralStatusEntity> generalStatus =
        superSquareRepository.findGeneralStatusSuperSquares();
    status.forEach(
        entityStatus ->
            this.setGeneralStateAndNumberMessages(
                entityStatus, generalStatus, configuredParameters));
  }

  private void setGeneralStateAndNumberMessagesPlaces(
      List<EntityStatusDTO> status,
      Long superSquareId,
      List<SentimentStateConfiguration> configuredParameters) {
    List<? extends GeneralStatusEntity> generalStatus =
        superSquareRepository.findGeneralStatusBySuperSquareId(superSquareId);
    status.forEach(
        entityStatus ->
            this.setGeneralStateAndNumberMessages(
                entityStatus, generalStatus, configuredParameters));
  }

  public void setGeneralStateAndNumberMessages(
      EntityStatusDTO entityStatus,
      List<? extends GeneralStatusEntity> generalStatus,
      List<SentimentStateConfiguration> configuredParameters) {
    Optional<? extends GeneralStatusEntity> generalState =
        generalStatus.stream()
            .filter(status -> status.getEntity().getId().equals(entityStatus.getId()))
            .findFirst();
    if (generalState.isPresent()) {
      entityStatus.setState(
          SentimentStateUtils.getState(generalState.get().getAverageScore(), configuredParameters));
      entityStatus.setNumberMessages(generalState.get().getNumberMessages());
    } else {
      entityStatus.setState(SentimentState.NO_STATE);
      entityStatus.setNumberMessages(0L);
    }
  }

  private void sortListByNumberMessages(List<EntityStatusDTO> status) {
    status.sort(Comparator.comparingLong(EntityStatusDTO::getNumberMessages).reversed());
  }

  public List<WordStatusDTO> getTopWords(Optional<Long> categoryId, Optional<Long> superSquareId) {
    List<WordStatus> topWords =
        wordService.getTopWords(this.getTopValue(), categoryId, superSquareId);
    return this.mapToWordDTO(topWords);
  }

  private List<WordStatusDTO> mapToWordDTO(List<WordStatus> topWords) {
    List<SentimentStateConfiguration> configuredParameters =
        sentimentStateConfigurationService.getConfiguredParameters();
    return topWords.stream()
        .map(
            word ->
                new WordStatusDTO(
                    word,
                    SentimentStateUtils.getState(word.getAverageScore(), configuredParameters)))
        .collect(Collectors.toList());
  }
}
