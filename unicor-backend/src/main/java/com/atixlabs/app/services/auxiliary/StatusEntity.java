package com.atixlabs.app.services.auxiliary;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.PlaceEntity;
import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.Getter;
import org.apache.commons.math3.util.Precision;

@Getter
public abstract class StatusEntity {

  protected Category category;

  protected Double averageScore;

  public BigDecimal getAverageScore() {
    return new BigDecimal(Double.toString(Precision.round(this.averageScore, 2)))
        .setScale(1, RoundingMode.HALF_UP);
  }

  public abstract PlaceEntity getEntity();
}
