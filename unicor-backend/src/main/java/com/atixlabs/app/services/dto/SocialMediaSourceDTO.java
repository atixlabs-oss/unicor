package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class SocialMediaSourceDTO {

  private Long id;

  private SocialMediaSourceCode code;

  private String name;

  public SocialMediaSourceDTO(SocialMediaSource socialMediaSource) {
    this.id = socialMediaSource.getId();
    this.code = socialMediaSource.getCode();
    this.name = socialMediaSource.getName();
  }
}
