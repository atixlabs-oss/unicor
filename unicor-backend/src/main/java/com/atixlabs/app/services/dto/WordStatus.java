package com.atixlabs.app.services.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.math3.util.Precision;

@AllArgsConstructor
@Getter
public class WordStatus {

  private String word;

  private Long numberMessages;

  private Double averageScore;

  public BigDecimal getAverageScore() {
    return new BigDecimal(Precision.round(this.averageScore, 2)).setScale(1, RoundingMode.HALF_UP);
  }
}
