package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.SuperSquare;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class SuperSquareDTO {

  private Long id;

  private String name;

  private List<CoordinateDTO> polygon;

  private StatusDTO status;

  public SuperSquareDTO(SuperSquare superSquare, List<CoordinateDTO> polygon, StatusDTO status) {
    this.id = superSquare.getId();
    this.name = superSquare.getName();
    this.polygon = polygon;
    this.status = status;
  }
}
