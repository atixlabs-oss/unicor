package com.atixlabs.app.services;

import com.atixlabs.app.model.Word;
import com.atixlabs.app.repositories.WordRepository;
import com.atixlabs.app.services.dto.WordStatus;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WordService {

  private WordRepository wordRepository;

  @Autowired
  public WordService(WordRepository wordRepository) {
    this.wordRepository = wordRepository;
  }

  public List<WordStatus> getTopWords(
      Integer top, Optional<Long> categoryId, Optional<Long> superSquareId) {
    return wordRepository.getTopWords(top, categoryId, superSquareId);
  }

  public List<Word> findAll() {
    return wordRepository.findAll();
  }

  public Word createWord(String name) {
    return wordRepository.save(new Word(name));
  }
}
