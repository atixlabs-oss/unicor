package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.enums.SentimentState;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class MessageStatusDTO {

  protected Long id;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  protected LocalDateTime postDate;

  protected String avatar;

  protected String username;

  protected SocialMediaSourceDTO source;

  protected String content;

  protected SentimentState state = SentimentState.NO_STATE;

  protected String url;

  public MessageStatusDTO(Message message, SocialMediaSourceDTO source, SentimentState state) {
    this.id = message.getId();
    this.postDate = message.getPostDate();
    this.avatar = message.getAvatar();
    this.username = message.getUsername();
    this.source = source;
    this.content = message.getContent();
    this.state = state;
    this.url = message.getUrl();
  }
}
