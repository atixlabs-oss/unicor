package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class FilterMessageDTO {

  private Optional<Long> categoryId;

  private Optional<Long> superSquareId;

  private Optional<List<Long>> socialMediaSourceIds;

  private Optional<List<SentimentStateConfiguration>> sentimentStateConfigurations;
}
