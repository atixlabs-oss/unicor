package com.atixlabs.app.services.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class StateDTO {

  private String state;

  private Long numberMessages;
}
