package com.atixlabs.app.services.auxiliary;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.SuperSquare;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SuperSquareStatus extends StatusEntity {

  private SuperSquare superSquare;

  public SuperSquare getEntity() {
    return this.superSquare;
  }

  public SuperSquareStatus(SuperSquare superSquare, Category category, Double averageScore) {
    this.superSquare = superSquare;
    this.category = category;
    this.averageScore = averageScore;
  }
}
