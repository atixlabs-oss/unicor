package com.atixlabs.app.services.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WordDTO {

  private String name;

  private BigDecimal magnitude;

  private BigDecimal sentiment;

  private BigDecimal score;
}
