package com.atixlabs.app.services;

import static com.atixlabs.app.repositories.specifications.MessageSpecifications.byFilter;

import com.atixlabs.app.exceptions.InvalidMessageScore;
import com.atixlabs.app.exceptions.InvalidWordScore;
import com.atixlabs.app.exceptions.MessageNotFoundException;
import com.atixlabs.app.exceptions.SourceMessageIdAlreadyExistsException;
import com.atixlabs.app.model.*;
import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.Message_;
import com.atixlabs.app.model.Place;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.repositories.MessageRepository;
import com.atixlabs.app.services.auxiliary.ConfiguredScoreRange;
import com.atixlabs.app.services.dto.*;
import com.atixlabs.security.utils.SentimentStateUtils;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MessageService {

  private MessageRepository messageRepository;

  private CategoryService categoryService;

  private WordService wordService;

  private SocialMediaSourceService socialMediaSourceService;

  private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Autowired
  public MessageService(
      MessageRepository messageRepository,
      CategoryService categoryService,
      WordService wordService,
      SocialMediaSourceService socialMediaSourceService,
      SentimentStateConfigurationService sentimentStateConfigurationService) {
    this.messageRepository = messageRepository;
    this.categoryService = categoryService;
    this.wordService = wordService;
    this.socialMediaSourceService = socialMediaSourceService;
    this.sentimentStateConfigurationService = sentimentStateConfigurationService;
  }

  public Page<MessageStatusDTO> getMessagesFilteredAndPaginated(
      Integer page,
      Integer size,
      Optional<Long> categoryId,
      Optional<Long> superSquareId,
      Optional<List<Long>> socialMediaSourceIds,
      Optional<List<Long>> sentimentalIds) {

    Optional<List<SentimentStateConfiguration>> configuredParameters =
        this.getSentimentFiltered(sentimentalIds);

    Pageable pageRequest = PageRequest.of(page, size, Sort.by(Message_.POST_DATE).descending());
    FilterMessageDTO filter =
        new FilterMessageDTO(categoryId, superSquareId, socialMediaSourceIds, configuredParameters);
    Page<Message> messages = messageRepository.findAll(byFilter(filter), pageRequest);
    return this.mapToMessageStatusDTO(messages);
  }

  private Optional<List<SentimentStateConfiguration>> getSentimentFiltered(
      Optional<List<Long>> sentimentalIds) {
    List<SentimentStateConfiguration> configuredParameters =
        sentimentStateConfigurationService.getConfiguredParameters();

    return sentimentalIds.map(
        ids ->
            configuredParameters.stream()
                .filter(
                    sentimentStateConfiguration ->
                        ids.stream().anyMatch(id -> sentimentStateConfiguration.getId().equals(id)))
                .collect(Collectors.toList()));
  }

  private Page<MessageStatusDTO> mapToMessageStatusDTO(Page<Message> messages) {
    List<SentimentStateConfiguration> configuredParameters =
        sentimentStateConfigurationService.getConfiguredParameters();
    return messages.map(
        message ->
            new MessageStatusDTO(
                message,
                new SocialMediaSourceDTO(message.getSource()),
                SentimentStateUtils.getState(message.getScore(), configuredParameters)));
  }

  public List<Message> saveAll(Collection<Message> socialMediaPosts) {
    return messageRepository.saveAll(socialMediaPosts);
  }

  public Collection<Message> getAllFromSource(SocialMediaSource source) {
    return messageRepository.findAllBySource(source);
  }

  public boolean existsMessage(String idReview) {
    return messageRepository.findMessageBySourceMessageId(idReview).isPresent();
  }

  public Optional<LocalDateTime> getLastTimeUpdated(SocialMediaSource source, Place place) {
    List<Message> messages = messageRepository.findLastMomentPlaceReviewUploaded(source, place);
    if (!messages.isEmpty()) {
      return Optional.of(messages.get(0).getPostDate());
    }
    return Optional.empty();
  }

  public List<MessageDTO> getUnprocessedMessages() {
    List<Message> messages = messageRepository.findByScoreIsNull();
    return this.mapToMessageDTO(messages);
  }

  private List<MessageDTO> mapToMessageDTO(List<Message> messages) {
    return messages.stream()
        .map(message -> new MessageDTO(message, new SocialMediaSourceDTO(message.getSource())))
        .collect(Collectors.toList());
  }

  private Message findMessageById(Long id) {
    return messageRepository.findById(id).orElseThrow(() -> new MessageNotFoundException(id));
  }

  public ProcessMessagesResult createOrUpdateProcessedMessages(List<MessageDTO> processedMessages) {

    if (processedMessages != null) log.info("{} mensajes a procesar", processedMessages.size());

    ProcessMessagesResult processMessagesResult = new ProcessMessagesResult();

    ConfiguredScoreRange configuredScoreRange =
        sentimentStateConfigurationService.getConfiguredScoreRange();

    processedMessages.forEach(
        processedMessage ->
            this.createOrUpdateMessage(
                processedMessage, configuredScoreRange, processMessagesResult));

    processMessagesResult.calculateAndSetTotalMessages();

    return processMessagesResult;
  }

  private void createOrUpdateMessage(
      MessageDTO processedMessage,
      ConfiguredScoreRange configuredScoreRange,
      ProcessMessagesResult processMessagesResult) {
    try {

      List<Word> words = wordService.findAll();
      List<Category> categories = categoryService.findAll();

      log.info("existing words {}", words.size());
      log.info("existing categories {}", categories.size());

      Message message = this.getMessage(processedMessage);
      this.validateScores(processedMessage, configuredScoreRange);
      this.updateAndSaveMessage(message, processedMessage, categories, words);
      processMessagesResult.addValidMessage();

    } catch (Exception ex) {

      log.error("Error while processing message {}.", processedMessage.getId(), ex);
      processMessagesResult.addMessageError(
          processedMessage.getId(), processedMessage.getSourceMessageId(), ex.getMessage());
    }
  }

  private Message getMessage(MessageDTO processedMessage) {

    Message message;

    if (processedMessage.getId() != null) {

      message = this.findMessageById(processedMessage.getId());

    } else {

      message = this.createMessage(processedMessage);
    }

    return message;
  }

  private Message createMessage(MessageDTO processedMessage) {
    this.validateSourceMessageId(processedMessage.getSourceMessageId());
    SocialMediaSource socialMediaSource =
        socialMediaSourceService.findSocialMediaSourceByCode(
            processedMessage.getSource().getCode());
    Message message =
        Message.builder()
            .source(socialMediaSource)
            .postDate(processedMessage.getPostDate())
            .sourceMessageId(processedMessage.getSourceMessageId())
            .url(processedMessage.getUrl())
            .username(processedMessage.getUsername())
            .avatar(processedMessage.getAvatar())
            .magnitude(processedMessage.getMagnitude())
            .sentiment(processedMessage.getSentiment())
            .score(processedMessage.getScore())
            .content(processedMessage.getContent())
            .categorizable(processedMessage.getCategorizable())
            .categories(new ArrayList<>())
            .words(new ArrayList<>())
            .build();
    return message;
  }

  private void validateSourceMessageId(String sourceMessageId) {
    messageRepository
        .findMessageBySourceMessageId(sourceMessageId)
        .ifPresent(
            message -> {
              throw new SourceMessageIdAlreadyExistsException(message.getSourceMessageId());
            });
  }

  private void updateAndSaveMessage(
      Message message,
      MessageDTO processedMessage,
      List<Category> existingCategories,
      List<Word> existingWords) {
    this.setMagnitudeSentimentAndScore(message, processedMessage);
    this.addCategories(message, existingCategories, processedMessage.getCategories());
    this.addWords(message, existingWords, processedMessage.getWords());
    messageRepository.save(message);
  }

  private void setMagnitudeSentimentAndScore(Message message, MessageDTO processedMessage) {
    message.setMagnitude(processedMessage.getMagnitude());
    message.setSentiment(processedMessage.getSentiment());
    message.setScore(processedMessage.getScore());
  }

  private void validateScores(
      MessageDTO processedMessage, ConfiguredScoreRange configuredScoreRange) {
    this.validateMessageScore(processedMessage.getScore(), configuredScoreRange);
    this.validateWordScores(processedMessage.getWords(), configuredScoreRange);
  }

  private void validateMessageScore(BigDecimal score, ConfiguredScoreRange configuredScoreRange) {
    if (!this.isValid(score, configuredScoreRange))
      throw new InvalidMessageScore(score, configuredScoreRange);
  }

  private void validateWordScores(List<WordDTO> words, ConfiguredScoreRange configuredScoreRange) {
    words.forEach(word -> this.validateWordScore(word, configuredScoreRange));
  }

  private void validateWordScore(WordDTO word, ConfiguredScoreRange configuredScoreRange) {
    if (!this.isValid(word.getScore(), configuredScoreRange))
      throw new InvalidWordScore(word, configuredScoreRange);
  }

  private boolean isValid(BigDecimal score, ConfiguredScoreRange configuredScoreRange) {
    return this.isBetween(
        score, configuredScoreRange.getMinimumValue(), configuredScoreRange.getMaximumValue());
  }

  private void addCategories(
      Message message, List<Category> existingCategories, List<String> categories) {

    for (String categoryName : categories) {

      Category category;

      if (this.isExistingCategory(categoryName, existingCategories)) {

        category = this.getExistingCategoryByName(categoryName, existingCategories);

      } else {

        category = categoryService.createCategory(categoryName);
        existingCategories.add(category);
      }

      message.addCategory(category);
    }
  }

  private boolean isExistingCategory(String category, List<Category> existingCategories) {
    return existingCategories.stream()
        .anyMatch(existingCategory -> category.equalsIgnoreCase(existingCategory.getName()));
  }

  private Category getExistingCategoryByName(String category, List<Category> existingCategories) {
    return existingCategories.stream()
        .filter(existingCategory -> category.equalsIgnoreCase(existingCategory.getName()))
        .findFirst()
        .get();
  }

  private void addWords(Message message, List<Word> existingWords, List<WordDTO> words) {

    if (words != null)
      log.info("get {} words for message {}", words.size(), message.getSourceMessageId());

    for (WordDTO wordDTO : words) {

      MessageWord messageWord;

      if (this.isExistingWord(wordDTO.getName(), existingWords)) {

        Word existingWord = this.getExistingWordByName(wordDTO.getName(), existingWords);
        messageWord = new MessageWord(message, existingWord, wordDTO);

      } else {

        Word newWord = wordService.createWord(wordDTO.getName());
        existingWords.add(newWord);
        messageWord = new MessageWord(message, newWord, wordDTO);
      }

      message.addWord(messageWord);
    }
  }

  private boolean isExistingWord(String word, List<Word> existingWords) {
    return existingWords.stream()
        .anyMatch(existingWord -> word.equalsIgnoreCase(existingWord.getName()));
  }

  private Word getExistingWordByName(String word, List<Word> existingWords) {
    return existingWords.stream()
        .filter(existingWord -> word.equalsIgnoreCase(existingWord.getName()))
        .findFirst()
        .get();
  }

  public <T extends Comparable<T>> boolean isBetween(T value, T start, T end) {
    return value.compareTo(start) >= 0 && value.compareTo(end) <= 0;
  }
}
