package com.atixlabs.app.services;

import com.atixlabs.app.exceptions.SocialMediaSourceNotFoundException;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.repositories.SocialMediaSourceRepository;
import com.atixlabs.app.services.dto.SocialMediaSourceDTO;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SocialMediaSourceService {

  private SocialMediaSourceRepository socialMediaSourceRepository;

  @Autowired
  public SocialMediaSourceService(SocialMediaSourceRepository socialMediaSourceRepository) {
    this.socialMediaSourceRepository = socialMediaSourceRepository;
  }

  public List<SocialMediaSourceDTO> getAllSocialMediaSource() {
    List<SocialMediaSource> socialMediaSourceList = socialMediaSourceRepository.findAll();
    return this.mapToSocialMediaSourceDTO(socialMediaSourceList);
  }

  private List<SocialMediaSourceDTO> mapToSocialMediaSourceDTO(
      List<SocialMediaSource> socialMediaSourceList) {
    return socialMediaSourceList.stream()
        .map(socialMediaSource -> new SocialMediaSourceDTO(socialMediaSource))
        .collect(Collectors.toList());
  }

  public SocialMediaSource findSocialMediaSourceByCode(SocialMediaSourceCode code) {
    return socialMediaSourceRepository
        .findByCode(code)
        .orElseThrow(() -> new SocialMediaSourceNotFoundException(code));
  }
}
