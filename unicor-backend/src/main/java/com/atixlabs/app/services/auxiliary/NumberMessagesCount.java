package com.atixlabs.app.services.auxiliary;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NumberMessagesCount {
  private Long count;
  private Double average;
}
