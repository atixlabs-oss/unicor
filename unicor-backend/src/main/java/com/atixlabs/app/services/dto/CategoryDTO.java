package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class CategoryDTO {

  private Long id;

  private String name;

  public CategoryDTO(Category category) {
    this.id = category.getId();
    this.name = category.getName();
  }
}
