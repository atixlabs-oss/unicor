package com.atixlabs.app.services.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
public class ProcessMessagesResult {

  public Integer totalMessages = 0;

  public Integer totalValidMessages = 0;

  private Integer totalErrorMessages = 0;

  private List<MessageErrorDetail> errorMessages = new ArrayList<>();

  public void addMessageError(Long messageId, String sourceMessageId, String error) {
    this.errorMessages.add(new MessageErrorDetail(messageId, sourceMessageId, error));
    this.totalErrorMessages++;
  }

  public void addValidMessage() {
    this.totalValidMessages++;
  }

  public void calculateAndSetTotalMessages() {
    Integer totalMessages = this.getTotalErrorMessages() + this.getTotalValidMessages();
    this.setTotalMessages(totalMessages);
  }
}
