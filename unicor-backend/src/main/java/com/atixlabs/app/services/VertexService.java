package com.atixlabs.app.services;

import com.atixlabs.app.model.Vertex;
import com.atixlabs.app.repositories.VertexRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VertexService {

  private VertexRepository vertexRepository;

  @Autowired
  public VertexService(VertexRepository vertexRepository) {
    this.vertexRepository = vertexRepository;
  }

  public List<Vertex> findBySuperSquareId(Long id) {
    return vertexRepository.findBySuperSquareId(id);
  }
}
