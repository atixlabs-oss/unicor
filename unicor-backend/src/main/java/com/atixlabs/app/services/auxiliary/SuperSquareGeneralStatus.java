package com.atixlabs.app.services.auxiliary;

import com.atixlabs.app.model.SuperSquare;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SuperSquareGeneralStatus extends GeneralStatusEntity {

  private SuperSquare superSquare;

  public SuperSquare getEntity() {
    return this.superSquare;
  }

  public SuperSquareGeneralStatus(
      SuperSquare superSquare, Long numberMessages, Double averageScore) {
    this.superSquare = superSquare;
    this.numberMessages = numberMessages;
    this.averageScore = averageScore;
  }
}
