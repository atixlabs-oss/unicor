package com.atixlabs.app.services;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.repositories.CategoryRepository;
import com.atixlabs.app.services.dto.CategoryDTO;
import com.atixlabs.app.services.dto.TopCategoryDTO;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

  private CategoryRepository categoryRepository;

  private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Autowired
  public CategoryService(
      CategoryRepository categoryRepository,
      SentimentStateConfigurationService sentimentStateConfigurationService) {
    this.categoryRepository = categoryRepository;
    this.sentimentStateConfigurationService = sentimentStateConfigurationService;
  }

  public List<CategoryDTO> getAllCategories() {
    List<Category> categories = categoryRepository.findAll();
    return this.mapCategoriesToDTO(categories);
  }

  public List<TopCategoryDTO> getTop(Optional<Long> superSquareId, Optional<Integer> limit) {
    List<SentimentStateConfiguration> sentimentStateConfigurations =
        this.sentimentStateConfigurationService.getConfiguredParameters();
    return categoryRepository.findTop(superSquareId, sentimentStateConfigurations, limit);
  }

  private List<CategoryDTO> mapCategoriesToDTO(List<Category> categories) {
    return categories.stream().map(CategoryDTO::new).collect(Collectors.toList());
  }

  List<Category> findMainCategories() {
    return categoryRepository.findByMainIsTrue();
  }

  public List<Category> findAll() {
    return categoryRepository.findAll();
  }

  public Category createCategory(String name) {
    return categoryRepository.save(new Category(name));
  }
}
