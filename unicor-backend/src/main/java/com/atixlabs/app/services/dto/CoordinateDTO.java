package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.embedables.Coordinate;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class CoordinateDTO {

  private BigDecimal latitude;

  private BigDecimal longitude;

  public CoordinateDTO(Coordinate coordinate) {
    this.latitude = coordinate.getLatitude();
    this.longitude = coordinate.getLongitude();
  }
}
