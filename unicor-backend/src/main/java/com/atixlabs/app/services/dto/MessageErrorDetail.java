package com.atixlabs.app.services.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MessageErrorDetail {

  private Long messageId;

  private String sourceMessageId;

  private String error;
}
