package com.atixlabs.app.services.auxiliary;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ConfiguredScoreRange {

  private BigDecimal maximumValue;

  private BigDecimal minimumValue;
}
