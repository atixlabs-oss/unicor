package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.enums.SentimentState;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StatusDTO {

  private SentimentState state;

  private BigDecimal score;
}
