package com.atixlabs.app.services.task;

import com.atixlabs.app.exceptions.GoogleReviewSourceURLNotExistsException;
import com.atixlabs.app.exceptions.InvalidGoogleReviewsException;
import com.atixlabs.app.exceptions.PlaceDoesntExistException;
import com.atixlabs.app.exceptions.RelativeDateNotSupportedException;
import com.atixlabs.app.model.PlaceSocialMediaSource;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.services.MessageService;
import com.atixlabs.app.services.SocialMediaSourceService;
import com.atixlabs.app.socialmedia.googlereviews.service.ReviewImporterService;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ProcessGoogleReviewsTask {

  private ReviewImporterService reviewImporterService;
  private DefaultExecutor defaultExecutor;
  private MessageService messageService;
  private SocialMediaSourceService socialMediaSourceService;

  @Value("${scraper.googlereviews.src}")
  private String scrapperSrc;

  @Value("${scraper.googlereviews.params}")
  private String scrapperParams;

  // TO DO: Move this constant to python scrapper
  @Value("${maps.googlereviews.url}")
  private String googleReviewsUrl;

  @Autowired
  public ProcessGoogleReviewsTask(
      ReviewImporterService reviewImporterService,
      MessageService messageService,
      SocialMediaSourceService socialMediaSourceService) {
    this.reviewImporterService = reviewImporterService;
    this.messageService = messageService;
    this.defaultExecutor = new DefaultExecutor();
    this.socialMediaSourceService = socialMediaSourceService;
  }

  public int scrapGoogleReviews(Long placeId)
      throws PlaceDoesntExistException, GoogleReviewSourceURLNotExistsException, IOException {
    Optional<PlaceSocialMediaSource> placeSocialMediaSource =
        reviewImporterService.getGoogleMapUrl(placeId);
    if (!placeSocialMediaSource.isPresent()) {
      throw new GoogleReviewSourceURLNotExistsException(
          "Couldnt find a google reviews url for place" + placeId.toString());
    }
    return scrapGoogleReviewsForSource(placeSocialMediaSource.get());
  }

  @Scheduled(cron = "${cron.googlereviews.expression}")
  // DEBUG ONLY
  // @Scheduled(initialDelay = 0, fixedDelay = 1000)
  public int scrapGoogleReviews() throws IOException {
    log.info("Executing processGoogleReviews");
    List<PlaceSocialMediaSource> googleReviewURLs = reviewImporterService.getAllGoogleMapsUrls();
    int messagesProcessed = 0;
    for (PlaceSocialMediaSource googleReviewURL : googleReviewURLs) {
      messagesProcessed += scrapGoogleReviewsForSource(googleReviewURL);
    }
    log.info("Finished scrapping all google reviews data");
    return messagesProcessed;
  }

  private int scrapGoogleReviewsForSource(PlaceSocialMediaSource googleReviewURL)
      throws IOException {
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      log.error("InterruptedException - {}", e.getLocalizedMessage());
    }
    Optional<LocalDateTime> lastCommentTime =
        messageService.getLastTimeUpdated(
            socialMediaSourceService.findSocialMediaSourceByCode(
                SocialMediaSourceCode.GOOGLE_REVIEW),
            googleReviewURL.getPlace());
    String timestamp = "1900-01-01T00:00:00.000000";
    if (lastCommentTime.isPresent()) {
      timestamp = lastCommentTime.get().toString();
    }

    File file = new ClassPathResource(scrapperSrc).getFile();

    log.info(file.getAbsolutePath());

    String script = file.getAbsolutePath();

    String command =
        new StringBuilder()
            .append("python3 ")
            .append(script)
            .append(" ")
            .append(scrapperParams)
            .append(" --url ")
            .append(googleReviewsUrl + googleReviewURL.getUserKey())
            .append(" --until ")
            .append(timestamp)
            .toString();
    CommandLine cmdLine = CommandLine.parse(command);
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
    defaultExecutor.setStreamHandler(streamHandler);
    try {
      log.info("Executing command: {}", command);
      defaultExecutor.execute(cmdLine);
    } catch (IOException e) {
      log.error(
          "Error running processGoogleReviews - error trying to execute command {}",
          e.getLocalizedMessage());
      log.error("Python log: {}", outputStream.toString());
      return -1;
    }
    int result = processResults(outputStream.toString(), googleReviewURL.getUserKey());
    log.info("Finished scrapping google reviews for url {}", googleReviewURL.getUserKey());
    return result;
  }

  private int processResults(String output, String placeUrl) {

    try {
      return reviewImporterService.processOutput(output, placeUrl);
    } catch (InvalidGoogleReviewsException
        | RelativeDateNotSupportedException
        | GoogleReviewSourceURLNotExistsException e) {
      log.error("Error processing google reviews results - {}", e.getLocalizedMessage());
      return -1;
    }
  }
}
