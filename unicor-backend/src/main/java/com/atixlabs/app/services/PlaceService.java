package com.atixlabs.app.services;

import com.atixlabs.app.model.Place;
import com.atixlabs.app.repositories.PlaceRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaceService {

  private PlaceRepository placeRepository;

  @Autowired
  public PlaceService(PlaceRepository placeRepository) {
    this.placeRepository = placeRepository;
  }

  public List<Place> findBySuperSquareId(Long superSquareId) {
    return this.placeRepository.findBySuperSquareId(superSquareId);
  }

  public Optional<Place> findById(Long placeId) {
    return this.placeRepository.findById(placeId);
  }
}
