package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.enums.SentimentState;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class MessageDTO extends MessageStatusDTO {

  private String sourceMessageId;

  private BigDecimal magnitude = new BigDecimal(0);

  private BigDecimal sentiment = new BigDecimal(0);

  private BigDecimal score = new BigDecimal(0);

  private List<WordDTO> words = new ArrayList<>();

  private List<String> categories = new ArrayList<>();

  private Boolean categorizable;

  @Builder
  public MessageDTO(
      Long id,
      LocalDateTime postDate,
      String sourceMessageId,
      String url,
      String avatar,
      String username,
      SocialMediaSourceDTO source,
      String content,
      SentimentState state,
      BigDecimal magnitude,
      BigDecimal sentiment,
      BigDecimal score,
      List<WordDTO> words,
      List<String> categories,
      Boolean categorizable) {
    super(id, postDate, avatar, username, source, content, state, url);
    this.sourceMessageId = sourceMessageId;
    this.magnitude = magnitude;
    this.sentiment = sentiment;
    this.score = score;
    this.words = words;
    this.categories = categories;
    this.categorizable = categorizable;
  }

  public MessageDTO(Message message, SocialMediaSourceDTO source) {
    super(message, source, SentimentState.NO_STATE);
    this.sourceMessageId = message.getSourceMessageId();
    this.url = message.getUrl();
    this.categorizable = message.getCategorizable();
  }
}
