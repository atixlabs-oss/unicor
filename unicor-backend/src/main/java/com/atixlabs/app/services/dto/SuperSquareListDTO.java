package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.SuperSquare;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class SuperSquareListDTO {

  private Long id;

  private String name;

  public SuperSquareListDTO(SuperSquare superSquare) {
    this.id = superSquare.getId();
    this.name = superSquare.getName();
  }
}
