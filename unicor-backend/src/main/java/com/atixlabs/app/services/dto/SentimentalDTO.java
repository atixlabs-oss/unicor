package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Builder
public class SentimentalDTO {
  private Long id;
  private String state;

  public SentimentalDTO(SentimentStateConfiguration sentimentStateConfiguration) {
    this.id = sentimentStateConfiguration.getId();
    this.state = sentimentStateConfiguration.getState().name();
  }
}
