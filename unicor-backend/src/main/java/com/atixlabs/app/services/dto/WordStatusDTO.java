package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.enums.SentimentState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class WordStatusDTO {

  private String word;

  private SentimentState state;

  private Long numberMessages;

  public WordStatusDTO(WordStatus wordStatus, SentimentState state) {
    this.word = wordStatus.getWord();
    this.state = state;
    this.numberMessages = wordStatus.getNumberMessages();
  }
}
