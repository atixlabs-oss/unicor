package com.atixlabs.app.services.auxiliary;

import com.atixlabs.app.model.Place;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PlaceGeneralStatus extends GeneralStatusEntity {

  private Place place;

  public Place getEntity() {
    return this.place;
  }

  public PlaceGeneralStatus(Place place, Long numberMessages, Double averageScore) {
    this.place = place;
    this.numberMessages = numberMessages;
    this.averageScore = averageScore;
  }
}
