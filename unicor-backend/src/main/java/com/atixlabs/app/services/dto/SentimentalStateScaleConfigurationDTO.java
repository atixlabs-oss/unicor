package com.atixlabs.app.services.dto;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SentimentalStateScaleConfigurationDTO {
  private BigDecimal max;
  private BigDecimal min;
}
