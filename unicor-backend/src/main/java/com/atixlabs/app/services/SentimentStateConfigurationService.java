package com.atixlabs.app.services;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.repositories.SentimentStateConfigurationRepository;
import com.atixlabs.app.services.auxiliary.ConfiguredScoreRange;
import com.atixlabs.app.services.dto.SentimentalDTO;
import com.atixlabs.app.services.dto.SentimentalStateScaleConfigurationDTO;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SentimentStateConfigurationService {

  private SentimentStateConfigurationRepository sentimentStateConfigurationRepository;

  @Autowired
  public SentimentStateConfigurationService(
      SentimentStateConfigurationRepository sentimentStateConfigurationRepository) {
    this.sentimentStateConfigurationRepository = sentimentStateConfigurationRepository;
  }

  public List<SentimentStateConfiguration> getConfiguredParameters() {
    return sentimentStateConfigurationRepository.findAll();
  }

  public List<SentimentalDTO> getAllSentiments() {
    return sentimentStateConfigurationRepository.findAll().stream()
        .map(SentimentalDTO::new)
        .collect(Collectors.toList());
  }

  public SentimentalStateScaleConfigurationDTO getRange() {
    return new SentimentalStateScaleConfigurationDTO(
        sentimentStateConfigurationRepository
            .findByState(SentimentState.GOOD)
            .get()
            .getMaximumValue(),
        sentimentStateConfigurationRepository
            .findByState(SentimentState.BAD)
            .get()
            .getMinimumValue());
  }

  public ConfiguredScoreRange getConfiguredScoreRange() {
    return sentimentStateConfigurationRepository.getConfiguredScoreRange();
  }
}
