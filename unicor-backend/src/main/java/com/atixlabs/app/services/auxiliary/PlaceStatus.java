package com.atixlabs.app.services.auxiliary;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.Place;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PlaceStatus extends StatusEntity {

  private Place place;

  public Place getEntity() {
    return this.place;
  }

  public PlaceStatus(Place place, Category category, Double averageScore) {
    this.place = place;
    this.category = category;
    this.averageScore = averageScore;
  }
}
