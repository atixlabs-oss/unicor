package com.atixlabs.app.services.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.core.io.ByteArrayResource;

@Getter
@Setter
public class ExportResultDTO {

  private String name;

  private ByteArrayResource resource;

  public ExportResultDTO(String name, ByteArrayResource resource) {
    this.name = name;
    this.resource = resource;
  }
}
