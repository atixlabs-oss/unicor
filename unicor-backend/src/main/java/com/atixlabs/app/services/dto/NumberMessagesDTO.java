package com.atixlabs.app.services.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class NumberMessagesDTO {
  private Long numberMessages;
  private Double avarage;
  private String state;
}
