package com.atixlabs.app.services.dto;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.services.auxiliary.StatusEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@AllArgsConstructor
@Getter
public class CategoryStatusDTO {

  private Long id;

  private String name;

  private SentimentState state;

  public CategoryStatusDTO(StatusEntity entityStatus, SentimentState state) {
    this.id = entityStatus.getCategory().getId();
    this.name = entityStatus.getCategory().getName();
    this.state = state;
  }

  public CategoryStatusDTO(Category category, SentimentState state) {
    this.id = category.getId();
    this.name = category.getName();
    this.state = state;
  }
}
