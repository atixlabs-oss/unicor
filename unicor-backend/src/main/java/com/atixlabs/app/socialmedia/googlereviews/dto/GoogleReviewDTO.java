package com.atixlabs.app.socialmedia.googlereviews.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class GoogleReviewDTO {

  @JsonProperty("url_review")
  private String urlReview;

  @JsonProperty("id_review")
  private String idReview;

  private String caption;

  @JsonProperty("relative_date")
  private String relativeDate;

  @JsonProperty("retrieval_date")
  private String retrievalDate;

  private Double rating;

  private String username;

  @JsonProperty("url_user")
  private String urlUser;

  @JsonProperty("url_image")
  private String urlImage;

  public boolean isValid() {
    if (urlReview == null
        || caption == null
        || relativeDate == null
        || retrievalDate == null
        || rating == null
        || username == null
        || idReview == null) {
      return false;
    }
    return true;
  }
}
