package com.atixlabs.app.socialmedia.googlereviews.service;

import com.atixlabs.app.exceptions.GoogleReviewSourceURLNotExistsException;
import com.atixlabs.app.exceptions.InvalidGoogleReviewsException;
import com.atixlabs.app.exceptions.PlaceDoesntExistException;
import com.atixlabs.app.exceptions.RelativeDateNotSupportedException;
import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.Place;
import com.atixlabs.app.model.PlaceSocialMediaSource;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.repositories.PlaceSocialMediaSourceRepository;
import com.atixlabs.app.services.MessageService;
import com.atixlabs.app.services.PlaceService;
import com.atixlabs.app.services.SocialMediaSourceService;
import com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ReviewImporterService {

  private MessageService messageService;
  private static final String A_MINUTE_AGO = "a minute ago";
  private static final String MINUTES_AGO = "minutes ago";
  private static final String HOURS_AGO = "hours ago";
  private static final String AN_HOUR_AGO = "an hour ago";
  private static final String DAYS_AGO = "days ago";
  private static final String A_DAY_AGO = "a day ago";
  private static final String WEEKS_AGO = "weeks ago";
  private static final String A_WEEK_AGO = "a week ago";
  private static final String MONTHS_AGO = "months ago";
  private static final String A_MONTH_AGO = "a month ago";
  private static final String YEARS_AGO = "years ago";
  private static final String A_YEAR_AGO = "a year ago";

  private static final Integer MINIMUM_MESSAGE_SIZE = 1;
  private static final Integer CATEGORIZABLE_MESSAGE_SIZE = 20;

  private PlaceSocialMediaSourceRepository placeSocialMediaSourceRepository;
  private SocialMediaSourceService socialMediaSourceService;
  private PlaceService placeService;

  @Value("${maps.googlereviews.url}")
  private String googleReviewsUrl;

  @Autowired
  public ReviewImporterService(
      MessageService messageService,
      PlaceSocialMediaSourceRepository placeSocialMediaSourceRepository,
      SocialMediaSourceService socialMediaSourceService,
      PlaceService placeService) {
    this.messageService = messageService;
    this.placeSocialMediaSourceRepository = placeSocialMediaSourceRepository;
    this.socialMediaSourceService = socialMediaSourceService;
    this.placeService = placeService;
  }

  public int processOutput(String output, String placeUrl)
      throws InvalidGoogleReviewsException, RelativeDateNotSupportedException,
          GoogleReviewSourceURLNotExistsException {
    // log.info("Procesing message result {}", output);
    String[] reviews = output.split("\\r?\\n");
    log.info("Procesing message count {}", reviews.length);
    List<GoogleReviewDTO> googleReviewDTOS = new ArrayList<>();
    Place place = findAssociatedPlace(placeUrl);
    for (String r : reviews) {
      try {
        googleReviewDTOS.add(new ObjectMapper().readValue(r, GoogleReviewDTO.class));
      } catch (JsonProcessingException e) {
        log.error("Jackson couldnt map json to GoogleReviewDTO class {}", e.getLocalizedMessage());
      }
    }
    if (googleReviewDTOS.stream().anyMatch(googleReviewDTO -> !googleReviewDTO.isValid())) {
      log.error(
          "Invalid google reviews: idReview, caption, relativeDate,"
              + " retrievalDate, rating or username were not found");
      throw new InvalidGoogleReviewsException(
          "Invalid google reviews: idReview, caption,"
              + " relativeDate, retrievalDate, rating or username were not found");
    }
    List<Message> messages = messageService.saveAll(processReviews(googleReviewDTOS, place));
    log.info("Successfully processed google reviews");
    return messages.size();
  }

  public List<Message> processReviews(List<GoogleReviewDTO> reviews, Place place)
      throws RelativeDateNotSupportedException, GoogleReviewSourceURLNotExistsException {
    log.info("Procesing {} message for place {}", reviews.size(), place.getName());
    List<Message> messages = new ArrayList<>();
    reviews =
        reviews.stream()
            .filter(googleReviewDTO -> !googleReviewDTO.getCaption().isEmpty())
            .collect(Collectors.toList());
    SocialMediaSource src =
        socialMediaSourceService.findSocialMediaSourceByCode(SocialMediaSourceCode.GOOGLE_REVIEW);
    for (GoogleReviewDTO googleReviewDTO : reviews) {
      // NOTE: To improve this we can get the latest date messages were retrieved for a place and
      // compare in memory
      if (messageService.existsMessage(googleReviewDTO.getIdReview())) {
        // message could have been edited, continue checking next messages
        continue;
      }
      try {
        LocalDateTime postedTime = null;
        String message = getReviewMessage(googleReviewDTO);
        if (message.trim().split(" ").length < MINIMUM_MESSAGE_SIZE) {
          continue;
        }
        postedTime =
            getLocalDateTime(googleReviewDTO.getRelativeDate(), googleReviewDTO.getRetrievalDate());
        messages.add(
            new Message(
                googleReviewDTO.getUsername(),
                googleReviewDTO.getUrlReview(),
                src,
                message,
                postedTime,
                place,
                googleReviewDTO.getUrlImage(),
                message.trim().split(" ").length >= CATEGORIZABLE_MESSAGE_SIZE,
                googleReviewDTO.getIdReview()));
      } catch (RelativeDateNotSupportedException e) {
        log.error("ERROR - couldnt process csv due to invalid relative dates or invalid URL");
        throw e;
      }
    }
    return messages;
  }

  private Place findAssociatedPlace(String urlSource)
      throws GoogleReviewSourceURLNotExistsException {
    urlSource = urlSource.replace(googleReviewsUrl, "");
    Optional<PlaceSocialMediaSource> googleReviewURL =
        placeSocialMediaSourceRepository.findOneByUserKey(urlSource);
    if (!googleReviewURL.isPresent()) {
      throw new GoogleReviewSourceURLNotExistsException(
          "URL was not uploaded to the system: " + urlSource);
    }
    return googleReviewURL.get().getPlace();
  }

  public LocalDateTime getLocalDateTime(String relativeDate, String retrievalDate)
      throws RelativeDateNotSupportedException {
    int minutes;
    int hours;
    int days;
    int weeks;
    int months;
    int years;
    minutes = hours = days = weeks = months = years = 0;
    String firstCharacters = relativeDate.split(" ")[0];
    if (relativeDate.toLowerCase().contains(MINUTES_AGO)) {
      minutes += Integer.parseInt(firstCharacters);
    } else if (relativeDate.toLowerCase().equals(A_MINUTE_AGO)) {
      minutes += 1;
    } else if (relativeDate.toLowerCase().contains(HOURS_AGO)) {
      hours += Integer.parseInt(firstCharacters);
    } else if (relativeDate.toLowerCase().equals(AN_HOUR_AGO)) {
      hours += 1;
    } else if (relativeDate.toLowerCase().contains(DAYS_AGO)) {
      days += Integer.parseInt(firstCharacters);
    } else if (relativeDate.toLowerCase().equals(A_DAY_AGO)) {
      days += 1;
    } else if (relativeDate.toLowerCase().contains(WEEKS_AGO)) {
      weeks += Integer.parseInt(firstCharacters);
    } else if (relativeDate.toLowerCase().equals(A_WEEK_AGO)) {
      weeks += 1;
    } else if (relativeDate.toLowerCase().contains(MONTHS_AGO)) {
      months += Integer.parseInt(firstCharacters);
    } else if (relativeDate.toLowerCase().equals(A_MONTH_AGO)) {
      months += 1;
    } else if (relativeDate.toLowerCase().contains(YEARS_AGO)) {
      years += Integer.parseInt(firstCharacters);
    } else if (relativeDate.toLowerCase().equals(A_YEAR_AGO)) {
      years += 1;
    } else {
      throw new RelativeDateNotSupportedException(relativeDate);
    }

    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXXXX");
    LocalDateTime oldDateTime = LocalDateTime.parse(retrievalDate, format);
    ZoneId oldZone = ZoneId.of("UTC");
    ZoneId newZone = ZoneId.of("America/Argentina/Cordoba");
    LocalDateTime newDateTime =
        oldDateTime.atZone(oldZone).withZoneSameInstant(newZone).toLocalDateTime();

    return newDateTime
        .minusMinutes(minutes)
        .minusHours(hours)
        .minusDays(days)
        .minusWeeks(weeks)
        .minusMonths(months)
        .minusYears(years);
  }

  private String getReviewMessage(GoogleReviewDTO googleReviewDTO) {
    if (googleReviewDTO.getCaption().contains("(Translated by Google)")
        && googleReviewDTO.getCaption().contains("(Original)")) {
      return googleReviewDTO.getCaption().split("\\(Original\\) ")[1];
    } else if (googleReviewDTO.getCaption().contains("(Translated by Google)")) {
      log.error(
          "Message containing translation, but not original message {}",
          googleReviewDTO.getCaption());
    }
    return googleReviewDTO.getCaption();
  }

  public List<PlaceSocialMediaSource> getAllGoogleMapsUrls() {
    return placeSocialMediaSourceRepository.findAllBySocialMediaSource(
        socialMediaSourceService.findSocialMediaSourceByCode(SocialMediaSourceCode.GOOGLE_REVIEW));
  }

  public Optional<PlaceSocialMediaSource> getGoogleMapUrl(Long placeId)
      throws PlaceDoesntExistException {
    Optional<Place> place = placeService.findById(placeId);
    if (place.isPresent()) {
      return placeSocialMediaSourceRepository.findOneByPlace(place);
    }
    throw new PlaceDoesntExistException("Couldnt find a place with id: " + placeId.toString());
  }
}
