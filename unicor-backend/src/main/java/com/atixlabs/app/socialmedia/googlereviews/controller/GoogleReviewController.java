package com.atixlabs.app.socialmedia.googlereviews.controller;

import com.atixlabs.app.exceptions.GoogleReviewSourceURLNotExistsException;
import com.atixlabs.app.exceptions.InvalidGoogleReviewsException;
import com.atixlabs.app.exceptions.PlaceDoesntExistException;
import com.atixlabs.app.exceptions.RelativeDateNotSupportedException;
import com.atixlabs.app.services.task.ProcessGoogleReviewsTask;
import com.atixlabs.app.socialmedia.googlereviews.service.ReviewImporterService;
import java.io.*;
import java.nio.charset.StandardCharsets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("/google-reviews")
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.POST},
    exposedHeaders = {"Content-Disposition"})
public class GoogleReviewController {
  /** * Solo para testear procesamiento de comentarios en caso de algun inconveniente. * */
  private ReviewImporterService reviewImporterService;

  private ProcessGoogleReviewsTask task;

  @Autowired
  public GoogleReviewController(
      ReviewImporterService reviewImporterService, ProcessGoogleReviewsTask task) {
    this.reviewImporterService = reviewImporterService;
    this.task = task;
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping("/process")
  @ResponseBody
  public ResponseEntity<?> processComments(
      @RequestParam String commentsData, @RequestParam String placeUrl) {

    log.info("processComments executed");

    if (commentsData.isEmpty()) {
      return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("Empty data");
    }
    ResponseEntity<?> response;
    try {
      reviewImporterService.processOutput(commentsData, placeUrl);
    } catch (RelativeDateNotSupportedException e) {
      log.error("RelativeDateNotSupportedException: " + e.getLocalizedMessage());
      return ResponseEntity.badRequest().build();
    } catch (InvalidGoogleReviewsException e) {
      log.error("InvalidGoogleReviewsException: " + e.getLocalizedMessage());
      return ResponseEntity.badRequest().build();
    } catch (GoogleReviewSourceURLNotExistsException e) {
      log.error("GoogleReviewSourceURLNotExistsException: " + e.getLocalizedMessage());
      return ResponseEntity.badRequest().build();
    }
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping("/processFile")
  @ResponseBody
  public ResponseEntity<?> importFile(
      @RequestParam("file") MultipartFile multipartFile, @RequestParam String placeUrl) {
    try {
      InputStream inputStream = multipartFile.getInputStream();
      new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))
          .lines()
          .forEach(
              l -> {
                try {
                  reviewImporterService.processOutput(l, placeUrl);
                } catch (RelativeDateNotSupportedException e) {
                  log.error("RelativeDateNotSupportedException: " + e.getLocalizedMessage());
                } catch (InvalidGoogleReviewsException e) {
                  log.error("InvalidGoogleReviewsException: " + e.getLocalizedMessage());
                } catch (GoogleReviewSourceURLNotExistsException e) {
                  log.error("GoogleReviewSourceURLNotExistsException: " + e.getLocalizedMessage());
                }
              });
    } catch (IOException e) {
      log.error("IOException: " + e.getLocalizedMessage());
      return ResponseEntity.badRequest().build();
    }
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/scrap-google-reviews-for-place")
  @ResponseBody
  public ResponseEntity<?> runGoogleReviewsScrapperTaskForPlace(@RequestParam Long id) {
    int result = -1;
    try {
      result = task.scrapGoogleReviews(id);
    } catch (PlaceDoesntExistException
        | GoogleReviewSourceURLNotExistsException
        | FileNotFoundException e) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getLocalizedMessage());
    } catch (IOException e) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getLocalizedMessage());
    }
    return ResponseEntity.status(HttpStatus.OK).body(result);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/scrap-all-google-reviews")
  @ResponseBody
  public ResponseEntity<?> runGoogleReviewsScrapperTaskForAllPlaces() throws IOException {
    return ResponseEntity.status(HttpStatus.OK).body(task.scrapGoogleReviews());
  }
}
