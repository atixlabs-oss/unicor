package com.atixlabs.app.repositories;

import com.atixlabs.app.services.dto.WordStatus;
import java.util.List;
import java.util.Optional;

public interface WordRepositoryCustom {

  List<WordStatus> getTopWords(
      Integer top, Optional<Long> categoryId, Optional<Long> superSquareId);
}
