package com.atixlabs.app.repositories.specifications;

import com.atixlabs.app.model.*;
import com.atixlabs.app.services.dto.FilterMessageDTO;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

public class MessageSpecifications {

  public static Specification<Message> byFilter(FilterMessageDTO filter) {
    return (Specification<Message>)
        (root, query, cb) -> {
          List<Predicate> predicates = new ArrayList<>();
          predicates.add(cb.isNotNull(root.get(Message_.SCORE)));

          if (filter.getCategoryId().isPresent()) {
            predicates.add(
                cb.equal(
                    root.join(Message_.CATEGORIES).get(Category_.ID),
                    filter.getCategoryId().get()));
          }

          if (filter.getSuperSquareId().isPresent()) {
            predicates.add(
                cb.equal(
                    root.get(Message_.PLACE).get(Place_.SUPER_SQUARE).get(SuperSquare_.ID),
                    filter.getSuperSquareId().get()));
          }

          if (filter.getSocialMediaSourceIds().isPresent()) {
            predicates.add(
                cb.in(root.get(Message_.SOURCE).get(SocialMediaSource_.ID))
                    .value(filter.getSocialMediaSourceIds().get()));
          }

          if (filter.getSentimentStateConfigurations().isPresent()) {
            List<Predicate> orPredicates = new ArrayList<>();
            filter
                .getSentimentStateConfigurations()
                .get()
                .forEach(
                    sentimentStateConfiguration -> {
                      orPredicates.add(
                          cb.between(
                              root.get(Message_.SCORE),
                              sentimentStateConfiguration.getMinimumValue(),
                              sentimentStateConfiguration.getMaximumValue()));
                    });
            predicates.add(cb.or(orPredicates.toArray(new Predicate[orPredicates.size()])));
          }

          return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
  }
}
