package com.atixlabs.app.repositories;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration_;
import com.atixlabs.app.services.auxiliary.ConfiguredScoreRange;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;

public class SentimentStateConfigurationRepositoryImpl
    implements SentimentStateConfigurationRepositoryCustom {

  @PersistenceContext protected EntityManager entityManager;

  @Override
  public ConfiguredScoreRange getConfiguredScoreRange() {

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<ConfiguredScoreRange> query = cb.createQuery(ConfiguredScoreRange.class);
    Root<SentimentStateConfiguration> sentimentStateConfiguration =
        query.from(SentimentStateConfiguration.class);

    Expression maximumValue =
        cb.max(sentimentStateConfiguration.get(SentimentStateConfiguration_.MAXIMUM_VALUE));

    Expression minimumValue =
        cb.min(sentimentStateConfiguration.get(SentimentStateConfiguration_.MINIMUM_VALUE));

    query.multiselect(maximumValue, minimumValue);

    return entityManager.createQuery(query).getSingleResult();
  }
}
