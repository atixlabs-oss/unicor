package com.atixlabs.app.repositories;

import com.atixlabs.app.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {}
