package com.atixlabs.app.repositories;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.services.auxiliary.GeneralStatusEntity;
import com.atixlabs.app.services.auxiliary.NumberMessagesCount;
import com.atixlabs.app.services.auxiliary.StatusEntity;
import com.atixlabs.app.services.auxiliary.SuperSquareGeneralStatus;
import java.util.List;
import java.util.Optional;

public interface SuperSquareRepositoryCustom {
  List<? extends StatusEntity> findSuperSquaresStatus(Optional<Long> categoryId);

  List<? extends StatusEntity> findSuperSquareStatus(Long superSquareId, Optional<Long> categoryId);

  List<? extends GeneralStatusEntity> findGeneralStatusSuperSquares();

  List<? extends GeneralStatusEntity> findGeneralStatusBySuperSquareId(Long superSquareId);

  List<SuperSquareGeneralStatus> findSuperSquareStatusBy(
      Optional<Long> categoryId, Optional<Long> superSquareId);

  NumberMessagesCount findMessagesBySuperSquareOrCategory(
      Optional<Long> superSquareId,
      Optional<Long> categoryId,
      SentimentStateConfiguration sentimentStateConfiguration);
}
