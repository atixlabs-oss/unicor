package com.atixlabs.app.repositories.custom;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.services.dto.TopCategoryDTO;
import java.util.List;
import java.util.Optional;

public interface CategoryRepositoryCustom {
  List<TopCategoryDTO> findTop(
      Optional<Long> superSquareId,
      List<SentimentStateConfiguration> sentimentStateConfigurations,
      Optional<Integer> limit);
}
