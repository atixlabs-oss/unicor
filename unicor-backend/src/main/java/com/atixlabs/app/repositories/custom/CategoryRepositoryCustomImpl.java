package com.atixlabs.app.repositories.custom;

import com.atixlabs.app.model.*;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.services.dto.TopCategoryDTO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;

public class CategoryRepositoryCustomImpl implements CategoryRepositoryCustom {
  @PersistenceContext protected EntityManager entityManager;

  @Override
  public List<TopCategoryDTO> findTop(
      Optional<Long> superSquareId,
      List<SentimentStateConfiguration> sentimentStateConfigurations,
      Optional<Integer> limit) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<TopCategoryDTO> query = cb.createQuery(TopCategoryDTO.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, Category> category = message.join(Message_.CATEGORIES);

    List<Predicate> predicates = new ArrayList<>();

    superSquareId.ifPresent(
        id -> {
          Join<Message, Place> place = message.join(Message_.PLACE);
          predicates.add(cb.equal(place.get(Place_.SUPER_SQUARE), id));
        });

    Expression<?> count = cb.count(message.get(Message_.ID));
    Expression<?> name = category.get(Category_.NAME);
    Expression<?> average = cb.avg(message.get(Message_.SCORE));

    CriteriaBuilder.Case<String> stateCase = cb.selectCase();

    sentimentStateConfigurations.forEach(
        sentimentStateConfiguration -> {
          stateCase.when(
              cb.between(
                  cb.avg(message.get(Message_.SCORE)).as(BigDecimal.class),
                  sentimentStateConfiguration.getMinimumValue(),
                  sentimentStateConfiguration.getMaximumValue()),
              sentimentStateConfiguration.getState().name());
        });

    Expression<?> state = stateCase.otherwise(cb.nullLiteral(String.class));

    // The percentage is calculated for the range between -1 y 1: (x + 1 * 100) / 2
    Expression<?> percentage =
        cb.quot(cb.prod(cb.sum(cb.avg(message.get(Message_.SCORE)), 1), 100), 2);

    query
        .multiselect(name, count, average, percentage, state)
        .where(predicates.toArray(new Predicate[0]))
        .groupBy(category.get(Category_.NAME))
        .orderBy(cb.desc(count));

    if (limit.isPresent()) {
      return entityManager.createQuery(query).setMaxResults(limit.get()).getResultList();
    }
    return entityManager.createQuery(query).getResultList();
  }
}
