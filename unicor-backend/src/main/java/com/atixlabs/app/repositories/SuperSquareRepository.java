package com.atixlabs.app.repositories;

import com.atixlabs.app.model.SuperSquare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface SuperSquareRepository
    extends JpaRepository<SuperSquare, Long>,
        JpaSpecificationExecutor<SuperSquare>,
        SuperSquareRepositoryCustom {}
