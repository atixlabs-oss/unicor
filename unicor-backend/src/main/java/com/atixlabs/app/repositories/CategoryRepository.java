package com.atixlabs.app.repositories;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.repositories.custom.CategoryRepositoryCustom;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository
    extends JpaRepository<Category, Long>, CategoryRepositoryCustom {
  List<Category> findByMainIsTrue();
}
