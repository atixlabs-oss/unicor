package com.atixlabs.app.repositories;

import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SocialMediaSourceRepository extends JpaRepository<SocialMediaSource, Long> {

  Optional<SocialMediaSource> findByCode(SocialMediaSourceCode code);
}
