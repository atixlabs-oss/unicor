package com.atixlabs.app.repositories;

import com.atixlabs.app.model.Place;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceRepository extends JpaRepository<Place, Long> {
  List<Place> findBySuperSquareId(Long superSquareId);
}
