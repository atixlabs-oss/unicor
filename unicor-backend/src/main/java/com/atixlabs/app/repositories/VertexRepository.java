package com.atixlabs.app.repositories;

import com.atixlabs.app.model.Vertex;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VertexRepository extends JpaRepository<Vertex, Long> {
  List<Vertex> findBySuperSquareId(Long id);
}
