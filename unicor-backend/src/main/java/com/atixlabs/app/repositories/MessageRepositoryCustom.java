package com.atixlabs.app.repositories;

import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.Place;
import com.atixlabs.app.model.SocialMediaSource;
import java.util.List;

public interface MessageRepositoryCustom {

  List<Message> findLastMomentPlaceReviewUploaded(SocialMediaSource source, Place place);
}
