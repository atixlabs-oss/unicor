package com.atixlabs.app.repositories;

import com.atixlabs.app.model.Word;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordRepository extends JpaRepository<Word, Long>, WordRepositoryCustom {}
