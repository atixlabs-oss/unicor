package com.atixlabs.app.repositories;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SentimentStateConfigurationRepository
    extends JpaRepository<SentimentStateConfiguration, Long>,
        SentimentStateConfigurationRepositoryCustom {
  Optional<SentimentStateConfiguration> findByState(SentimentState sentimentState);
}
