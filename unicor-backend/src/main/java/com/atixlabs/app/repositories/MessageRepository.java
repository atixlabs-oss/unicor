package com.atixlabs.app.repositories;

import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.SocialMediaSource;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository
    extends JpaRepository<Message, Long>,
        JpaSpecificationExecutor<Message>,
        PagingAndSortingRepository<Message, Long>,
        MessageRepositoryCustom {

  Collection<Message> findAllBySource(SocialMediaSource source);

  List<Message> findByScoreIsNull();

  Optional<Message> findMessageBySourceMessageId(String sourceId);
}
