package com.atixlabs.app.repositories;

import com.atixlabs.app.model.*;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.services.auxiliary.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;

public class SuperSquareRepositoryImpl implements SuperSquareRepositoryCustom {

  @PersistenceContext protected EntityManager entityManager;

  @Override
  public List<SuperSquareStatus> findSuperSquaresStatus(Optional<Long> categoryId) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<SuperSquareStatus> query = cb.createQuery(SuperSquareStatus.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, Category> category = message.join(Message_.CATEGORIES);
    Join<Message, Place> place = message.join(Message_.PLACE);
    final Join<Place, SuperSquare> superSquare = place.join(Place_.SUPER_SQUARE);

    Subquery<Long> sq = query.subquery(Long.class);
    Root<Category> sqCategory = sq.from(Category.class);
    sq.select(sqCategory.get(Category_.ID)).where(cb.isTrue(sqCategory.get(Category_.MAIN)));

    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isNotNull(message.get(Message_.SCORE)));
    predicates.add(cb.in(category.get(Category_.ID)).value(sq));

    categoryId.ifPresent(id -> predicates.add(cb.equal(category.get(Category_.ID), id)));

    Expression averageScore = cb.avg(message.get(Message_.SCORE));

    query
        .multiselect(superSquare, category, averageScore)
        .where(predicates.toArray(new Predicate[0]))
        .groupBy(superSquare.get(SuperSquare_.ID), category.get(Category_.ID));

    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public List<? extends StatusEntity> findSuperSquareStatus(
      Long superSquareId, Optional<Long> categoryId) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<PlaceStatus> query = cb.createQuery(PlaceStatus.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, Category> category = message.join(Message_.CATEGORIES);
    final Join<Message, Place> place = message.join(Message_.PLACE);

    Subquery<Long> sq = query.subquery(Long.class);
    Root<Category> sqCategory = sq.from(Category.class);
    sq.select(sqCategory.get(Category_.ID)).where(cb.isTrue(sqCategory.get(Category_.MAIN)));

    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isNotNull(message.get(Message_.SCORE)));
    predicates.add(cb.in(category.get(Category_.ID)).value(sq));
    predicates.add(cb.equal(place.get(Place_.SUPER_SQUARE).get(SuperSquare_.ID), superSquareId));

    categoryId.ifPresent(id -> predicates.add(cb.equal(category.get(Category_.ID), id)));

    Expression averageScore = cb.avg(message.get(Message_.SCORE));

    query
        .multiselect(place, category, averageScore)
        .where(predicates.toArray(new Predicate[0]))
        .groupBy(place.get(Place_.ID), category.get(Category_.ID));

    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public List<? extends GeneralStatusEntity> findGeneralStatusSuperSquares() {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<SuperSquareGeneralStatus> query = cb.createQuery(SuperSquareGeneralStatus.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, Place> place = message.join(Message_.PLACE);
    Join<Place, SuperSquare> superSquare = place.join(Place_.SUPER_SQUARE);

    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isNotNull(message.get(Message_.SCORE)));

    Expression count = cb.count(message.get(Message_.ID));

    Expression averageScore = cb.avg(message.get(Message_.SCORE));

    query
        .multiselect(superSquare, count, averageScore)
        .where(predicates.toArray(new Predicate[0]))
        .groupBy(superSquare.get(SuperSquare_.ID));

    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public List<? extends GeneralStatusEntity> findGeneralStatusBySuperSquareId(Long superSquareId) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<PlaceGeneralStatus> query = cb.createQuery(PlaceGeneralStatus.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, Place> place = message.join(Message_.PLACE);

    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isNotNull(message.get(Message_.SCORE)));
    predicates.add(cb.equal(place.get(Place_.SUPER_SQUARE).get(SuperSquare_.ID), superSquareId));

    Expression count = cb.count(message.get(Message_.ID));

    Expression averageScore = cb.avg(message.get(Message_.SCORE));

    query
        .multiselect(place, count, averageScore)
        .where(predicates.toArray(new Predicate[0]))
        .groupBy(place.get(Place_.ID));

    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public List<SuperSquareGeneralStatus> findSuperSquareStatusBy(
      Optional<Long> categoryId, Optional<Long> superSquareId) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<SuperSquareGeneralStatus> query = cb.createQuery(SuperSquareGeneralStatus.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, Place> place = message.join(Message_.PLACE);
    final Join<Place, SuperSquare> superSquare = place.join(Place_.SUPER_SQUARE);

    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isNotNull(message.get(Message_.SCORE)));
    superSquareId.ifPresent(
        id -> predicates.add(cb.equal(place.get(Place_.SUPER_SQUARE).get(SuperSquare_.ID), id)));

    if (categoryId.isPresent()) {
      Join<Message, Category> category = message.join(Message_.CATEGORIES);
      predicates.add(cb.equal(category.get(Category_.ID), categoryId.get()));
    }

    Expression count = cb.count(message.get(Message_.ID));

    Expression averageScore = cb.avg(message.get(Message_.SCORE));

    query
        .multiselect(superSquare, count, averageScore)
        .where(predicates.toArray(new Predicate[0]))
        .groupBy(superSquare.get(SuperSquare_.ID));

    return entityManager.createQuery(query).getResultList();
  }

  @Override
  public NumberMessagesCount findMessagesBySuperSquareOrCategory(
      Optional<Long> superSquareId,
      Optional<Long> categoryId,
      SentimentStateConfiguration sentimentStateConfiguration) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<NumberMessagesCount> query = cb.createQuery(NumberMessagesCount.class);
    Root<Message> message = query.from(Message.class);

    List<Predicate> predicates = new ArrayList<>();

    categoryId.ifPresent(
        id -> {
          Join<Message, Category> category = message.join(Message_.CATEGORIES);
          predicates.add(cb.equal(category.get(Category_.ID), id));
        });

    superSquareId.ifPresent(
        id -> {
          Join<Message, Place> place = message.join(Message_.PLACE);
          predicates.add(cb.equal(place.get(Place_.SUPER_SQUARE), id));
        });

    predicates.add(
        cb.between(
            message.get(Message_.SCORE),
            sentimentStateConfiguration.getMinimumValue(),
            sentimentStateConfiguration.getMaximumValue()));

    Expression<?> count = cb.countDistinct(message.get(Message_.ID));
    Expression<Double> averageScore = cb.avg(message.get(Message_.SCORE));

    query.multiselect(count, averageScore).where(predicates.toArray(new Predicate[0]));

    return entityManager.createQuery(query).getSingleResult();
  }
}
