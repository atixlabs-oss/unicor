package com.atixlabs.app.repositories;

import com.atixlabs.app.model.Place;
import com.atixlabs.app.model.PlaceSocialMediaSource;
import com.atixlabs.app.model.SocialMediaSource;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceSocialMediaSourceRepository
    extends JpaRepository<PlaceSocialMediaSource, Long> {
  Optional<PlaceSocialMediaSource> findOneByUserKey(String userKey);

  List<PlaceSocialMediaSource> findAllBySocialMediaSource(SocialMediaSource source);

  Optional<PlaceSocialMediaSource> findOneByPlace(Optional<Place> place);
}
