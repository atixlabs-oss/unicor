package com.atixlabs.app.repositories;

import com.atixlabs.app.services.auxiliary.ConfiguredScoreRange;

public interface SentimentStateConfigurationRepositoryCustom {

  ConfiguredScoreRange getConfiguredScoreRange();
}
