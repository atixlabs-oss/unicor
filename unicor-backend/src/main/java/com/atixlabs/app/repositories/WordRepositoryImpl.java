package com.atixlabs.app.repositories;

import com.atixlabs.app.model.*;
import com.atixlabs.app.services.dto.WordStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;

public class WordRepositoryImpl implements WordRepositoryCustom {

  @PersistenceContext protected EntityManager entityManager;

  @Override
  public List<WordStatus> getTopWords(
      Integer top, Optional<Long> categoryId, Optional<Long> superSquareId) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<WordStatus> query = cb.createQuery(WordStatus.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, Word> word = message.join(Message_.WORDS);

    List<Predicate> predicates = new ArrayList<>();

    predicates.add(cb.isNotNull(word.get(MessageWord_.SCORE)));

    superSquareId.ifPresent(
        id -> {
          Join<Message, Place> place = message.join(Message_.PLACE);
          predicates.add(cb.equal(place.get(Place_.SUPER_SQUARE).get(SuperSquare_.ID), id));
        });

    categoryId.ifPresent(
        id -> {
          Join<Message, Category> category = message.join(Message_.CATEGORIES);
          predicates.add(cb.equal(category.get(Category_.ID), id));
        });

    Expression count = cb.count(word.get(MessageWord_.WORD).get(Word_.ID));

    Expression averageScore = cb.avg(word.get(MessageWord_.SCORE));

    query
        .multiselect(word.get(MessageWord_.WORD).get(Word_.NAME), count, averageScore)
        .where(predicates.toArray(new Predicate[0]))
        .groupBy(word.get(MessageWord_.WORD).get(Word_.NAME))
        .orderBy(cb.desc(count), cb.asc(word.get(MessageWord_.WORD).get(Word_.NAME)));
    return entityManager.createQuery(query).setMaxResults(top).getResultList();
  }
}
