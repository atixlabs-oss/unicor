package com.atixlabs.app.repositories.specifications;

import com.atixlabs.app.model.SuperSquare;
import com.atixlabs.app.model.SuperSquare_;
import java.util.Optional;
import org.springframework.data.jpa.domain.Specification;

public class SuperSquareSpecifications {

  public static Specification<SuperSquare> byId(Optional<Long> superSquareId) {
    return (Specification<SuperSquare>)
        (root, query, cb) -> {
          if (superSquareId.isPresent()) {
            return cb.equal(root.get(SuperSquare_.ID), superSquareId.get());
          }
          return null;
        };
  }
}
