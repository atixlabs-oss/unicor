package com.atixlabs.app.repositories;

import com.atixlabs.app.model.*;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;

public class MessageRepositoryImpl implements MessageRepositoryCustom {

  @PersistenceContext protected EntityManager entityManager;

  @Override
  public List<Message> findLastMomentPlaceReviewUploaded(SocialMediaSource source, Place place) {
    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Message> query = cb.createQuery(Message.class);
    Root<Message> message = query.from(Message.class);
    Join<Message, SocialMediaSource> socialMediaSourceJoin = message.join(Message_.SOURCE);
    final Join<Place, Message> placeMessageJoin = message.join(Message_.PLACE);
    List<Predicate> predicates = new ArrayList<>();
    predicates.add(cb.equal(socialMediaSourceJoin.get(SocialMediaSource_.ID), source.getId()));
    predicates.add(cb.equal(placeMessageJoin.get(Place_.ID), place.getId()));
    query
        .select(message)
        .where(predicates.toArray(new Predicate[0]))
        .orderBy(cb.desc(message.get(Message_.POST_DATE)));
    return entityManager.createQuery(query).setMaxResults(1).getResultList();
  }
}
