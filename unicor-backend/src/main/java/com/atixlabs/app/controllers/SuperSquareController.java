package com.atixlabs.app.controllers;

import com.atixlabs.app.services.SuperSquareService;
import com.atixlabs.app.services.dto.*;
import java.util.Optional;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(SuperSquareController.URL_MAPPING)
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET})
public class SuperSquareController {

  public static final String URL_MAPPING = "/super-squares";

  private SuperSquareService superSquareService;

  @Autowired
  public SuperSquareController(SuperSquareService superSquareService) {
    this.superSquareService = superSquareService;
  }

  @GetMapping
  public ResponseEntity<?> getSuperSquares(
      @RequestParam(required = false) @Min(1) Optional<Long> categoryId,
      @RequestParam(required = false) @Min(1) Optional<Long> superSquareId) {

    return ResponseEntity.ok().body(superSquareService.getSuperSquares(categoryId, superSquareId));
  }

  @GetMapping("/list")
  public ResponseEntity<?> getSuperSquaresList() {

    return ResponseEntity.ok().body(superSquareService.getSuperSquaresList());
  }

  @GetMapping("/status")
  public ResponseEntity<?> getSuperSquaresStatus(
      @RequestParam(required = false) @Min(1) Optional<Long> categoryId,
      @RequestParam(required = false) @Min(1) Optional<Long> superSquareId) {

    return ResponseEntity.ok()
        .body(superSquareService.getSuperSquaresStatus(categoryId, superSquareId));
  }

  @GetMapping("/number-messages")
  public ResponseEntity<?> getNumberMessagesByState(
      @RequestParam(required = false) @Min(1) Optional<Long> categoryId,
      @RequestParam(required = false) @Min(1) Optional<Long> superSquareId) {

    return ResponseEntity.ok()
        .body(superSquareService.getNumberMessages(categoryId, superSquareId));
  }

  @GetMapping("/top-words")
  public ResponseEntity<?> getTopWords(
      @RequestParam(required = false) @Min(1) Optional<Long> categoryId,
      @RequestParam(required = false) @Min(1) Optional<Long> superSquareId) {

    return ResponseEntity.ok().body(superSquareService.getTopWords(categoryId, superSquareId));
  }
}
