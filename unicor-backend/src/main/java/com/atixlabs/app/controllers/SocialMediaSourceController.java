package com.atixlabs.app.controllers;

import com.atixlabs.app.services.SocialMediaSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(SocialMediaSourceController.URL_MAPPING)
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET})
public class SocialMediaSourceController {

  public static final String URL_MAPPING = "/social-media";

  private SocialMediaSourceService socialMediaSourceService;

  @Autowired
  public SocialMediaSourceController(SocialMediaSourceService socialMediaSourceService) {
    this.socialMediaSourceService = socialMediaSourceService;
  }

  @GetMapping
  public ResponseEntity<?> getAllSocialMediaSource() {

    return ResponseEntity.ok().body(socialMediaSourceService.getAllSocialMediaSource());
  }
}
