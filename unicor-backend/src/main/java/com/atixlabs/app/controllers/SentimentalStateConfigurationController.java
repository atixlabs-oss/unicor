package com.atixlabs.app.controllers;

import com.atixlabs.app.services.SentimentStateConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(SentimentalStateConfigurationController.URL_MAPPING)
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET})
public class SentimentalStateConfigurationController {

  public static final String URL_MAPPING = "/sentimental-state-configuration";

  private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Autowired
  public SentimentalStateConfigurationController(
      SentimentStateConfigurationService sentimentStateConfigurationService) {
    this.sentimentStateConfigurationService = sentimentStateConfigurationService;
  }

  @GetMapping(value = "/scale")
  public ResponseEntity<?> getScale() {
    return ResponseEntity.ok().body(sentimentStateConfigurationService.getRange());
  }

  @GetMapping
  public ResponseEntity<?> getSentimentals() {
    return ResponseEntity.ok().body(sentimentStateConfigurationService.getAllSentiments());
  }
}
