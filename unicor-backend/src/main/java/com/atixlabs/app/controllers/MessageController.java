package com.atixlabs.app.controllers;

import com.atixlabs.app.services.MessageService;
import com.atixlabs.app.services.dto.MessageDTO;
import java.util.List;
import java.util.Optional;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(MessageController.URL_MAPPING)
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET, RequestMethod.POST})
public class MessageController {

  public static final String URL_MAPPING = "/messages";

  private MessageService messageService;

  @Autowired
  public MessageController(MessageService messageService) {
    this.messageService = messageService;
  }

  @GetMapping
  public ResponseEntity<?> getMessagesFilteredAndPaginated(
      @RequestParam("page") @Min(0) Integer page,
      @RequestParam("size") @Min(1) Integer size,
      @RequestParam(value = "categoryId", required = false) Optional<Long> categoryId,
      @RequestParam(value = "superSquareId", required = false) Optional<Long> superSquareId,
      @RequestParam(value = "socialMediaSourceIds", required = false)
          Optional<List<Long>> socialMediaSourceIds,
      @RequestParam(value = "sentimentalIds", required = false)
          Optional<List<Long>> sentimentalIds) {

    return ResponseEntity.ok()
        .body(
            messageService.getMessagesFilteredAndPaginated(
                page, size, categoryId, superSquareId, socialMediaSourceIds, sentimentalIds));
  }

  @GetMapping(value = "/unprocessed")
  public ResponseEntity<?> getUnprocessedMessages() {

    return ResponseEntity.ok().body(messageService.getUnprocessedMessages());
  }

  @PostMapping(value = "/processed")
  public ResponseEntity<?> updateProcessedMessages(
      @RequestBody List<MessageDTO> processedMessages) {

    return ResponseEntity.ok()
        .body(messageService.createOrUpdateProcessedMessages(processedMessages));
  }
}
