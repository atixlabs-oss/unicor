package com.atixlabs.app.controllers;

import com.atixlabs.app.services.CategoryService;
import java.util.Optional;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(CategoryController.URL_MAPPING)
@CrossOrigin(
    origins = "*",
    methods = {RequestMethod.GET})
public class CategoryController {

  public static final String URL_MAPPING = "/categories";

  private CategoryService categoryService;

  @Autowired
  public CategoryController(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  @GetMapping
  public ResponseEntity<?> getAllCategories() {
    return ResponseEntity.ok().body(categoryService.getAllCategories());
  }

  @GetMapping("/top")
  public ResponseEntity<?> getTopCategories(
      @RequestParam(required = false) @Min(1) Optional<Long> superSquareId,
      @RequestParam(required = false) @Min(1) Optional<Integer> limit) {
    return ResponseEntity.ok().body(categoryService.getTop(superSquareId, limit));
  }
}
