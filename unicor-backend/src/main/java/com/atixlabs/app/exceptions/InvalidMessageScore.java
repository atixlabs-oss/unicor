package com.atixlabs.app.exceptions;

import com.atixlabs.app.services.auxiliary.ConfiguredScoreRange;
import java.math.BigDecimal;

public class InvalidMessageScore extends RuntimeException {
  public InvalidMessageScore(BigDecimal score, ConfiguredScoreRange configuredScoreRange) {
    super(
        String.format(
            "The message score %.1f is not within the range %.2f and %.2f.",
            score, configuredScoreRange.getMinimumValue(), configuredScoreRange.getMaximumValue()));
  }
}
