package com.atixlabs.app.exceptions;

public class PlaceDoesntExistException extends Exception {
  public PlaceDoesntExistException(String message) {
    super(message);
  }
}
