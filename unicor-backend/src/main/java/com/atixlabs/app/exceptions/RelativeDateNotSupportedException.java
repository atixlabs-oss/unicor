package com.atixlabs.app.exceptions;

public class RelativeDateNotSupportedException extends Exception {
  public RelativeDateNotSupportedException(String message) {
    super(message);
  }
}
