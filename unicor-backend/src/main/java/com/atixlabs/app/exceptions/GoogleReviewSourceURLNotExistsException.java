package com.atixlabs.app.exceptions;

public class GoogleReviewSourceURLNotExistsException extends Exception {
  public GoogleReviewSourceURLNotExistsException(String message) {
    super(message);
  }
}
