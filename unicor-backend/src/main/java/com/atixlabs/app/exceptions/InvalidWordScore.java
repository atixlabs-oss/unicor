package com.atixlabs.app.exceptions;

import com.atixlabs.app.services.auxiliary.ConfiguredScoreRange;
import com.atixlabs.app.services.dto.WordDTO;

public class InvalidWordScore extends RuntimeException {
  public InvalidWordScore(WordDTO word, ConfiguredScoreRange configuredScoreRange) {
    super(
        String.format(
            "The %.1f score for the word '%s' is not within the range %.2f and %.2f.",
            word.getScore(),
            word.getName(),
            configuredScoreRange.getMinimumValue(),
            configuredScoreRange.getMaximumValue()));
  }
}
