package com.atixlabs.app.exceptions;

public class SourceMessageIdAlreadyExistsException extends RuntimeException {

  public SourceMessageIdAlreadyExistsException(String sourceId) {
    super(String.format("The source message id %s already exists.", sourceId));
  }
}
