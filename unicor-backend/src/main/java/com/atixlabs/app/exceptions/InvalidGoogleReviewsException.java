package com.atixlabs.app.exceptions;

public class InvalidGoogleReviewsException extends Exception {
  public InvalidGoogleReviewsException(String message) {
    super(message);
  }
}
