package com.atixlabs.app.exceptions;

import com.atixlabs.app.model.enums.SocialMediaSourceCode;

public class SocialMediaSourceNotFoundException extends RuntimeException {
  public SocialMediaSourceNotFoundException(SocialMediaSourceCode sourceCode) {
    super(String.format("Could not find social media source with code %s.", sourceCode));
  }
}
