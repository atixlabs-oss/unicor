package com.atixlabs.app.exceptions;

public class CouldntGenerateURLsFileException extends Exception {
  public CouldntGenerateURLsFileException(String message) {
    super(message);
  }
}
