package com.atixlabs.app.exceptions;

public class MessageNotFoundException extends RuntimeException {
  public MessageNotFoundException(Long id) {
    super(String.format("Could not find message with id %d.", id));
  }
}
