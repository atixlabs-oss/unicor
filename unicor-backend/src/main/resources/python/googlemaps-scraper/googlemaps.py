# -*- coding: utf-8 -*-
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from datetime import datetime, timedelta, timezone
import time
import re
import logging
import traceback
import json  

GM_WEBPAGE = 'https://www.google.com/maps/'
MAX_WAIT = 10
MAX_RETRY = 5
MAX_SCROLLS = 40

LOAD_REVIEWS_SLEEP = 0.5 #original 4
MORE_REVIEWS_SLEEP = 0.5 #original 1
EXPAND_REVIEWS_SLEEP = 1 #original 2
SORT_BY_SLEEP = 0.5 #original 3 
SORT_BY_SLEEP_LOAD = 0.5 #original 5
SHARE_LINKS_SLEEP = 0.4 #original 1
SHARE_LINK_RETRY_SLEEP = 0.4 #original 1

class GoogleMapsScraper:

    def __init__(self, until, debug=False):
        self.debug = debug
        self.driver = self.__get_driver()
        self.logger = self.__get_logger()
        self.until = until
        self.share_like_links_index = 0

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        if exc_type is not None:
            traceback.print_exception(exc_type, exc_value, tb)

        self.driver.close()
        self.driver.quit()

        return True

    def sort_by(self, url, ind):
        self.driver.get(url)
        wait = WebDriverWait(self.driver, MAX_WAIT)

        # open dropdown menu
        clicked = False
        tries = 0
        while not clicked and tries < MAX_RETRY:
            try:
                menu_bt = wait.until(EC.element_to_be_clickable((By.XPATH, '//button[@data-value=\'Sort\']')))
                menu_bt.click()
                clicked = True
                time.sleep(SORT_BY_SLEEP)
            except Exception as e:
                tries += 1
                self.logger.warn('Failed to click recent button')

            # failed to open the dropdown
            if tries == MAX_RETRY:
                return -1

        #  element of the list specified according to ind
        recent_rating_bt = self.driver.find_elements_by_xpath('//li[@role=\'menuitemradio\']')[ind]
        recent_rating_bt.click()

        # wait to load review (ajax call)
        time.sleep(SORT_BY_SLEEP_LOAD)

        return 0

    def get_reviews(self, offset, limit):
        # scroll to load reviews

        # wait for other reviews to load (ajax)
        time.sleep(LOAD_REVIEWS_SLEEP)

        self.__scroll()

        # expand review text
        self.__expand_reviews()
        self.__expand_reviews()
        #If a comment is empty, no share link will be found!
        share_links = self.__get_share_links()
        # parse reviews
        response = BeautifulSoup(self.driver.page_source, 'html.parser')
        rblock = response.find_all('div', class_='section-review-content')
        parsed_reviews = []

        i = 0
        read_reviews = 0
        for index, review in enumerate(rblock):
            if index >= offset and index < limit:
                parsed_review = self.__parse(review)
                if (self.__continue(parsed_review['retrieval_date'], parsed_review['relative_date'])):
                    read_reviews += 1
                    parsed_reviews.append(parsed_review)
                    if (parsed_review['caption'] is not None and parsed_review['caption'] != ""):
                        try:
                            parsed_review['url_review'] = share_links[i]
                        except:
                            parsed_review['url_review'] = ""
                        i += 1
                        print(json.dumps(parsed_review))
                else:
                    return parsed_reviews, read_reviews

        return parsed_reviews, read_reviews

    def __parse(self, review):
        item = {}

        id_review = review.find('button', class_='section-review-action-menu')['data-review-id']
        username = review.find('div', class_='section-review-title').find('span').text
        user_image_url = review.find('img', class_='section-review-reviewer-image').get('src')
        try:
            review_text = self.__filter_string(review.find('span', class_='section-review-text').text)
        except Exception as e:
            review_text = None

        rating = float(review.find('span', class_='section-review-stars')['aria-label'].split(' ')[1])
        relative_date = review.find('span', class_='section-review-publish-date').text
        user_url = review.find('a')['href']

        item['id_review'] = id_review
        item['caption'] = review_text

        # depends on language, which depends on geolocation defined by Google Maps
        # custom mapping to transform into date shuold be implemented
        item['relative_date'] = relative_date

        # store datetime of scraping and apply further processing to calculate
        # correct date as retrieval_date - time(relative_date)
        item['retrieval_date'] = datetime.now(timezone.utc).isoformat()
        item['rating'] = rating
        item['username'] = username
        item['url_user'] = user_url
        item['url_image'] = user_image_url
        return item

    # expand review description
    def __expand_reviews(self):
        # use XPath to load complete reviews
        links = self.driver.find_elements_by_xpath('//button[@class=\'section-expand-review mapsConsumerUiCommonButton__blue-link\']')
        for l in links:
            l.click()
        time.sleep(EXPAND_REVIEWS_SLEEP)

    def __get_share_links(self):
        all_buttons = self.driver.find_elements_by_xpath('//button[@class=\'section-review-interaction-button\']')
        all_buttons = all_buttons[self.share_like_links_index:]
        self.share_like_links_index += len(all_buttons)
        share_buttons = list()
        for b in all_buttons:
            if (b.text == 'Share'):
                share_buttons.append(b)

        #keep only the lattest loaded share buttons
        share_links = list()
        for b in share_buttons:
            b.click()
            time.sleep(SHARE_LINKS_SLEEP)
            retry = 0
            while(retry < MAX_RETRY):
                try:
                    share_link = self.driver.find_element_by_xpath('//input[@class=\'section-copy-link-input\']')
                    share_links.append(share_link.get_attribute('value'))
                    close_button = self.driver.find_element_by_xpath('//button[@class=\'mapsConsumerUiCommonClosebutton__close-button mapsConsumerUiCommonClosebutton__close-button-white-circle\']')
                    close_button.click()
                    break
                except Exception as e:
                    retry += 1
                    if (retry == MAX_RETRY):
                        share_links.append("")
                    time.sleep(SHARE_LINK_RETRY_SLEEP)


        return share_links

    # load more reviews
    def more_reviews(self):
        # use XPath to load complete reviews
        links = self.driver.find_elements_by_xpath('//button[@jsaction=\'pane.reviewChart.moreReviews\']')
        for l in links:
            l.click()
        time.sleep(MORE_REVIEWS_SLEEP)

    def __scroll(self):
        scrollable_div = self.driver.find_element_by_css_selector('div.section-layout.section-scrollbox.scrollable-y.scrollable-show')
        self.driver.execute_script('arguments[0].scrollTop = arguments[0].scrollHeight', scrollable_div)
      
    def __get_logger(self):
        # create logger
        logger = logging.getLogger('googlemaps-scraper')
        logger.setLevel(logging.DEBUG)

        # create console handler and set level to debug
        fh = logging.FileHandler('gm-scraper.log')
        fh.setLevel(logging.DEBUG)

        # create formatter
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

        # add formatter to ch
        fh.setFormatter(formatter)

        # add ch to logger
        logger.addHandler(fh)
        return logger

    def __get_driver(self, debug=False):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-notifications")
        chrome_options.add_argument("--lang=en-GB")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--disable-gpu")
        input_driver = webdriver.Chrome("/usr/local/bin/chromedriver",chrome_options=chrome_options)
        return input_driver

    # util function to clean special characters
    def __filter_string(self, str):
        strOut = str.replace('\r', ' ').replace('\n', ' ').replace('\t', ' ')
        return strOut

    def __parse_relative_date(self, string_date):
        curr_date = datetime.now()
        split_date = string_date.split(' ')
        n = split_date[0]
        delta = split_date[1]

        if delta == 'year':
            return curr_date - timedelta(days=365)
        elif delta == 'years':
            return curr_date - timedelta(days=365 * int(n))
        elif delta == 'month':
            return curr_date - timedelta(days=30)
        elif delta == 'months':
            return curr_date - timedelta(days=30 * int(n))
        elif delta == 'week':
            return curr_date - timedelta(weeks=1)
        elif delta == 'weeks':
            return curr_date - timedelta(weeks=int(n))
        elif delta == 'day':
            return curr_date - timedelta(days=1)
        elif delta == 'days':
            return curr_date - timedelta(days=int(n))
        elif delta == 'hour':
            return curr_date - timedelta(hours=1)
        elif delta == 'hours':
            return curr_date - timedelta(hours=int(n))
        elif delta == 'minute':
            return curr_date - timedelta(minutes=1)
        elif delta == 'minutes':
            return curr_date - timedelta(minutes=int(n))
        elif delta == 'moments':
            return curr_date

    def __continue(self, retrieval_date, relative_date):
        parsed_date = self.__parse_relative_date(relative_date)
        if (parsed_date <= self.until):
            return False
            
        return True
