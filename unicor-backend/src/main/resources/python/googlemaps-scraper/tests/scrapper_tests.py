import os, sys
CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(CURRENT_DIR))
from googlemaps import GoogleMapsScraper
import unittest
from datetime import datetime, timedelta

class TestGoogleMapsScraper(unittest.TestCase):



    def setUp(self):
        self._relativeDates = ["a minute ago", "minutes ago", "hours ago", "an hour ago",
                     "days ago", "a day ago", "weeks ago", "a week ago", "months ago",
                     "a month ago", "years ago", "a year ago"] 

    def _relativeDateIsCorrect(self, relativeDate):
        result = False
        for rd in self._relativeDates:
            result = result or (rd in relativeDate)
        return result

    def testScrappingDataProcess(self):
        url = "https://www.google.com/maps/place/Plaza+San+Mart%C3%ADn/@-31.4168318,-64.1857908,17z/data=!4m7!3m6!1s0x0:0x4382bb478dc6e982!8m2!3d-31.4168318!4d-64.1836021!9m1!1b1"
        scraper = GoogleMapsScraper(datetime(2009, 10, 5, 18, 00), debug=False)
        self.assertEqual(scraper.sort_by(url, 1), 0)
        reviews, read_reviews = scraper.get_reviews(0, 10)
        self.assertGreater(read_reviews, 0)
        for review in reviews:
            self.assertGreater(len(review), 0)
            self.assertGreaterEqual(len(review['caption']), 0)
            self.assertTrue(self._relativeDateIsCorrect(review['relative_date']))
            self.assertIsNotNone(review['retrieval_date'])
            self.assertGreaterEqual(review['rating'], 0.0)
            self.assertLessEqual(review['rating'], 5.0)
            self.assertGreater(len(review['username']), 0)
            self.assertGreater(len(review['url_user']), 0)
            self.assertGreater(len(review['url_image']), 0)
    
    def _parseRelativeDate(self, curr_date, string_date):
        split_date = string_date.split(' ')

        n = split_date[0]
        delta = split_date[1]

        if delta == 'year':
            return curr_date - timedelta(days=365)
        elif delta == 'years':
            return curr_date - timedelta(days=365 * int(n))
        elif delta == 'month':
            return curr_date - timedelta(days=30)
        elif delta == 'months':
            return curr_date - timedelta(days=30 * int(n))
        elif delta == 'week':
            return curr_date - timedelta(weeks=1)
        elif delta == 'weeks':
            return curr_date - timedelta(weeks=int(n))
        elif delta == 'day':
            return curr_date - timedelta(days=1)
        elif delta == 'days':
            return curr_date - timedelta(days=int(n))
        elif delta == 'hour':
            return curr_date - timedelta(hours=1)
        elif delta == 'hours':
            return curr_date - timedelta(hours=int(n))
        elif delta == 'minute':
            return curr_date - timedelta(minutes=1)
        elif delta == 'minutes':
            return curr_date - timedelta(minutes=int(n))
        elif delta == 'moments':
            return curr_date
    
    def testParseRelativeDate(self):
        now = datetime.now()
        date1 = self._parseRelativeDate(now, "a year ago")
        self.assertEqual(date1.year, now.year - 1)
        date2 = self._parseRelativeDate(now, "a month ago")
        self.assertEqual(date2.month, now.month - 1)
        date3 = self._parseRelativeDate(now, "a day ago")
        self.assertEqual(date3.day, now.day - 1)
        date4 = self._parseRelativeDate(now, "5 hours ago")
        self.assertEqual(date4.hour, now.hour - 5)
        date5 = self._parseRelativeDate(now, "17 minutes ago")
        self.assertEqual(date5.minute, now.minute - 17)

if __name__ == '__main__':
    unittest.main()
