# -*- coding: utf-8 -*-
from googlemaps import GoogleMapsScraper
from datetime import datetime, timedelta
import argparse
import csv
from termcolor import colored
import time
import dateutil.parser

ind = {'most_relevant' : 0 , 'newest' : 1, 'highest_rating' : 2, 'lowest_rating' : 3 }
HEADER = ['id_review', 'caption', 'relative_date', 'retrieval_date', 'rating', 'username', 'n_review_user', 'n_photo_user', 'url_user', 'url_image']
HEADER_W_SOURCE = ['id_review', 'caption', 'relative_date','retrieval_date', 'rating', 'username', 'n_review_user', 'n_photo_user', 'url_user', 'url_image', 'url_source']
MAX_RETRY_READ_REVIEWS = 4

def csv_writer(source_field, ind_sort_by, path='src/main/resources/python/googlemaps-scraper/data/'):
    outfile= ind_sort_by + '_gm_reviews.csv'
    targetfile = open(path + outfile, mode='w', encoding='utf-8', newline='\n')
    writer = csv.writer(targetfile, quoting=csv.QUOTE_MINIMAL)

    if source_field:
        h = HEADER_W_SOURCE
    else:
        h = HEADER
    writer.writerow(h)

    return writer


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Google Maps reviews scraper.')
    parser.add_argument('--N', type=int, default=100, help='Number of reviews to scrape')
    #parser.add_argument('--i', type=str, default='src/main/resources/python/googlemaps-scraper/urls.txt', help='target URLs file')
    parser.add_argument('--sort_by', type=str, default='newest', help='sort by most_relevant, newest, highest_rating or lowest_rating')
    parser.add_argument('--debug', dest='debug', action='store_true', help='Run scraper using browser graphical interface')
    parser.add_argument('--source', dest='source', action='store_true', help='Add source url to CSV file (for multiple urls in a single file)')
    parser.add_argument('--results_file', dest='results_file', action='store_true', help='Dont write results to file')
    parser.add_argument('--url', type=str, help='Target google reviews url to scrap data')
    parser.add_argument('--until', type=str, help='Timestamp until to process comments')
    parser.set_defaults(place=False, debug=False, source=False, results_file=False)
    args = parser.parse_args()
    # store reviews in CSV file
    if args.results_file:
        writer = csv_writer(args.source, args.sort_by)

    with GoogleMapsScraper(dateutil.parser.parse(args.until), debug=args.debug) as scraper:

        error = scraper.sort_by(args.url, ind[args.sort_by])
        if error == 0:
            n = 0
            if ind[args.sort_by] == 0:
                scraper.more_reviews()
            tries = 0    
            while n < args.N and tries < MAX_RETRY_READ_REVIEWS:
                reviews, read_reviews = scraper.get_reviews(n, args.N)
                if args.results_file:
                    for r in reviews:
                        row_data = list(r.values())
                        if args.source:
                            row_data.append(url[:-1])

                        writer.writerow(row_data)
                n += read_reviews
                #No se leyeron mas reviews porque se llego a la fecha limite
                if (read_reviews == 0):
                    tries += 1
