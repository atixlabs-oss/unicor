-- liquibase formatted sql

-- changeset tomas:20210413-02
INSERT INTO place (id, id_super_square, name) VALUES (13, 1, 'Museo de la Memoria de Córdoba');
ALTER TABLE place ALTER COLUMN id RESTART WITH 14;
INSERT INTO place_social_media_source (id_social_media_source, id_place, user_key) VALUES (2, 13, 'Museo+de+la+Memoria+Córdoba/@-31.416601,-64.1863456,17.52z/data=!4m10!1m2!2m1!1sMuseo+de+la+Memoria+de+Córdoba!3m6!1s0x9432a28346f5e1a3:0xeeee8343dd550641!8m2!3d-31.4164186!4d-64.1847306!9m1!1b1');
