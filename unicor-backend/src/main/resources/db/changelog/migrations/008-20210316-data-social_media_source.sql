-- liquibase formatted sql

-- changeset federico:20210316-01
INSERT INTO social_media_source (id, code, name) VALUES (1, 'TWITTER', 'Twitter');
INSERT INTO social_media_source (id, code, name) VALUES (2, 'GOOGLE_REVIEW', 'Google');
ALTER TABLE social_media_source ALTER COLUMN id RESTART WITH 3;