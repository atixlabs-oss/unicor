-- liquibase formatted sql

-- changeset federico:20210505-01
CREATE INDEX idx_message_on_id_place ON message (id_place);
CREATE INDEX idx_message_on_id_social_media_source ON message (id_social_media_source);

-- changeset federico:20210505-02
CREATE INDEX idx_place_on_id_super_square ON place (id_super_square);

-- changeset federico:20210505-03
ALTER TABLE message_category ADD CONSTRAINT message_category_pkey PRIMARY KEY (id_message, id_category);

