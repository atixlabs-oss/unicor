-- liquibase formatted sql

-- changeset federico:20210304-01
INSERT INTO category (id, name, main) VALUES (1, 'Medioambiente', true);
INSERT INTO category (id, name, main) VALUES (2, 'Servicio', true);
INSERT INTO category (id, name, main) VALUES (3, 'Infraestructura', true);
ALTER TABLE category ALTER COLUMN id RESTART WITH 4;