-- liquibase formatted sql

-- changeset federico:20210304-01
INSERT INTO sentiment_state_configuration (id, state, minimum_value, maximum_value, created, updated) VALUES (1, 'GOOD', 0.25, 1.00, '2021-03-08 15:30:30.567836', '2021-03-08 15:30:30.567836');
INSERT INTO sentiment_state_configuration (id, state, minimum_value, maximum_value, created, updated) VALUES (2, 'NEUTRAL', -0.24, 0.24,'2021-03-08 15:30:30.567836', '2021-03-08 15:30:30.567836');
INSERT INTO sentiment_state_configuration (id, state, minimum_value, maximum_value, created, updated) VALUES (3, 'BAD', -1.00, -0.25, '2021-03-08 15:30:30.567836', '2021-03-08 15:30:30.567836');
ALTER TABLE sentiment_state_configuration ALTER COLUMN id RESTART WITH 4;