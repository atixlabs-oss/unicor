-- liquibase formatted sql

-- changeset federico:20210304-01
INSERT INTO city (id, name) VALUES (1, 'Ciudad de Córdoba');
ALTER TABLE city ALTER COLUMN id RESTART WITH 2;

-- changeset federico:20210304-02
INSERT INTO super_square (id, name, id_city) VALUES (1, 'Super Manzana 1', 1);
INSERT INTO super_square (id, name, id_city) VALUES (2, 'Super Manzana 2', 1);
INSERT INTO super_square (id, name, id_city) VALUES (3, 'Super Manzana 3', 1);
ALTER TABLE super_square ALTER COLUMN id RESTART WITH 4;

-- changeset federico:20210304-03
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (1, -31.415229350600743, -64.18671790594954, 1);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (2, -31.417495778256807, -64.1876914340867, 1);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (5, -31.414770079357268, -64.19258988508062, 2);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (3, -31.418651305006403, -64.1836599864587, 1);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (6, -31.417180643675998, -64.19348729197077, 2);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (7, -31.41808810975812, -64.19023561608786, 2);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (4, -31.4164142510647, -64.18271166248164, 1);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (8, -31.41519256983108, -64.19122123069029, 2);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (9, -31.410097453203083, -64.18156307592754, 3);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (10, -31.41046522728367, -64.18023166918073, 3);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (11, -31.409480598604166, -64.1797380430976, 3);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (12, -31.409029263218198, -64.18109140751531, 3);
ALTER TABLE vertex ALTER COLUMN id RESTART WITH 13;

-- changeset federico:20210305-04
INSERT INTO place (id, id_super_square, name) VALUES (1, 1, 'Plaza San Martín');
INSERT INTO place (id, id_super_square, name) VALUES (2, 1, 'Catedral de Córdoba');
INSERT INTO place (id, id_super_square, name) VALUES (3, 1, 'Plazoleta Jerónimo Luis de Cabrera – Plazoleta Del Fundador');
INSERT INTO place (id, id_super_square, name) VALUES (4, 1, 'Cabildo de Córdoba');
INSERT INTO place (id, id_super_square, name) VALUES (5, 1, 'Archivo Provincial de la Memoria de Córdoba – Museo de la Memoria de Córdoba');
INSERT INTO place (id, id_super_square, name) VALUES (6, 2, 'Palacio Municipal – Municipalidad de Córdoba');
INSERT INTO place (id, id_super_square, name) VALUES (7, 2, 'Paseo Marqués de Sobremonte');
INSERT INTO place (id, id_super_square, name) VALUES (8, 2, 'Plaza de la Intendencia');
INSERT INTO place (id, id_super_square, name) VALUES (9, 2, 'Tribunales I, Poder Judicial de Córdoba');
INSERT INTO place (id, id_super_square, name) VALUES (10, 3, 'Mercado Norte');
ALTER TABLE place ALTER COLUMN id RESTART WITH 11;