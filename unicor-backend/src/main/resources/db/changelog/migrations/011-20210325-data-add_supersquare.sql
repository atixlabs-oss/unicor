-- liquibase formatted sql

-- changeset federico:20210325-01
INSERT INTO super_square (id, name, id_city) VALUES (4, 'Super Manzana 4', 1);
ALTER TABLE super_square ALTER COLUMN id RESTART WITH 5;

-- changeset federico:20210325-02
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (13, -31.42202726658051, -64.18658329346515, 4);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (14, -31.42356883056892, -64.18721011644244, 4);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (15, -31.42468136810465, -64.1866987521468, 4);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (16, -31.425198447085112, -64.18489302823424, 4);
INSERT INTO vertex (id, latitude, longitude, id_super_square) VALUES (17, -31.422883064980862, -64.18391041274191, 4);
ALTER TABLE vertex ALTER COLUMN id RESTART WITH 18;

-- changeset federico:20210325-03
INSERT INTO place (id, id_super_square, name) VALUES (11, 4, 'Paseo del Buen Pastor');
INSERT INTO place (id, id_super_square, name) VALUES (12, 4, 'Iglesia del Sagrado Corazón de Jesús (Iglesia de los Capuchinos)');
ALTER TABLE place ALTER COLUMN id RESTART WITH 13;

-- changeset federico:20210325-04
INSERT INTO place_social_media_source (id_social_media_source, id_place, user_key) VALUES (2, 11, 'Paseo+del+Buen+Pastor/@-31.4239068,-64.1864382,15z/data=!4m7!3m6!1s0x0:0xca71e10e4641086b!8m2!3d-31.4239068!4d-64.1864382!9m1!1b1');
INSERT INTO place_social_media_source (id_social_media_source, id_place, user_key) VALUES (2, 12, 'Iglesia+del+Sagrado+Corazón+de+Jesús+(Iglesia+de+los+Capuchinos)/@-31.4242804,-64.1856599,17z/data=!4m7!3m6!1s0x9432a28f9b82eb4f:0x5fa116c0073e51e2!8m2!3d-31.4244558!4d-64.185774!9m1!1b1');