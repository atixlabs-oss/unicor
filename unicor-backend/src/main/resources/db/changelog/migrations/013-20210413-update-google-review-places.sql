-- liquibase formatted sql

-- changeset tomas:20210413-01
update place set name = 'Plazoleta Jerónimo Luis de Cabrera' where name = 'Plazoleta Jerónimo Luis de Cabrera – Plazoleta Del Fundador';
update place_social_media_source set user_key = 'Plazoleta+Jer%C3%B3nimo+Luis+de+Cabrera/@-31.4111743,-64.1966157,13.74z/data=!4m10!1m2!2m1!1sPlazoleta+Jer%C3%B3nimo+Luis+de+Cabrera!3m6!1s0x0:0x2732b4af90345c3c!8m2!3d-31.4166062!4d-64.1855076!9m1!1b1' where user_key = 'Plazoleta+del+Fundador/@-31.4166084,-64.1855972,20.15z/data=!4m10!1m2!2m1!1sPlazoleta+Jer%C3%B3nimo+Luis+de+Cabrera+%E2%80%93+Plazoleta+Del+Fundador!3m6!1s0x0:0xf0fcf5869b4c4a6e!8m2!3d-31.4166364!4d-64.1852758!9m1!1b1';
update place set name = 'Palacio 6 de Julio, Municipalidad de Córdoba' where name = 'Palacio Municipal – Municipalidad de Córdoba';
update place set name = 'Archivo Provincial de la Memoria de Córdoba' where name = 'Archivo Provincial de la Memoria de Córdoba – Museo de la Memoria de Córdoba';

