-- liquibase formatted sql

-- changeset atix:1611861060664-1
INSERT INTO permission (code, created, updated, description) VALUES ('VIEW_PROFILE', '2021-01-28 16:10:34.06611', '2021-01-28 16:10:34.066168', 'Permiso ver perfil');
INSERT INTO permission (code, created, updated, description) VALUES ('CREATE_USER', '2021-01-28 16:10:34.073088', '2021-01-28 16:10:34.073114', 'Permiso crear usuario');
INSERT INTO permission (code, created, updated, description) VALUES ('LIST_USERS', '2021-01-28 16:10:34.076457', '2021-01-28 16:10:34.076483', 'Permiso Listar usuarios');
INSERT INTO permission (code, created, updated, description) VALUES ('MODIFY_USERS', '2021-01-28 16:10:34.079667', '2021-01-28 16:10:34.079689', 'Permiso Modificar usuarios');

-- changeset atix:1611861060664-2
INSERT INTO role (id, code, description, created, updated) VALUES (1, 'ROLE_ADMIN', 'Administrator', '2021-01-28 16:10:34.088021', '2021-01-28 16:10:34.088045');
INSERT INTO role (id, code, description, created, updated) VALUES (2, 'ROLE_OPERATOR', 'Operator', '2021-01-28 16:10:34.104593', '2021-01-28 16:10:34.104624');
ALTER TABLE role ALTER COLUMN id RESTART WITH 3;

-- changeset atix:1611861060664-3
INSERT INTO role_permission (id_role, id_permission) VALUES (1, 'LIST_USERS');
INSERT INTO role_permission (id_role, id_permission) VALUES (1, 'MODIFY_USERS');
INSERT INTO role_permission (id_role, id_permission) VALUES (1, 'CREATE_USER');
INSERT INTO role_permission (id_role, id_permission) VALUES (1, 'VIEW_PROFILE');
INSERT INTO role_permission (id_role, id_permission) VALUES (2, 'VIEW_PROFILE');
INSERT INTO role_permission (id_role, id_permission) VALUES (2, 'MODIFY_USERS');

-- changeset atix:1611861060664-4
INSERT INTO menu (id, code, icon, menu_type, name, item_order, request_type, uri, id_parent) VALUES (1, 'USERS', 'user', 'PRIMARY', 'My account', 1, NULL, NULL, NULL);
INSERT INTO menu (id, code, icon, menu_type, name, item_order, request_type, uri, id_parent) VALUES (2, 'VIEW_PROFILE', 'user', 'PRIMARY', 'View Profile', 0, 'GET', '/user-information', 1);
INSERT INTO menu (id, code, icon, menu_type, name, item_order, request_type, uri, id_parent) VALUES (3, 'LOGOUT', 'logout', 'PRIMARY','Logout', 1, 'GET', '/login', 1);
INSERT INTO menu (id, code, icon, menu_type, name, item_order, request_type, uri, id_parent) VALUES (4, 'MANAGE_USERS', 'setting', 'SECONDARY','Manage Users', 2, 'GET', '/administration', NULL);

ALTER TABLE menu ALTER COLUMN id RESTART WITH 5;

-- changeset atix:1611861060664-5
INSERT INTO role_menu (id_role, id_menu) VALUES (1, 2);
INSERT INTO role_menu (id_role, id_menu) VALUES (1, 4);
INSERT INTO role_menu (id_role, id_menu) VALUES (1, 3);
INSERT INTO role_menu (id_role, id_menu) VALUES (1, 1);
INSERT INTO role_menu (id_role, id_menu) VALUES (2, 2);
INSERT INTO role_menu (id_role, id_menu) VALUES (2, 3);
INSERT INTO role_menu (id_role, id_menu) VALUES (2, 1);

-- changeset atix:1611861060664-6
INSERT INTO users (id, created, updated, active, email, password, id_role) VALUES (1, '2021-01-28 16:10:34.305205', '2021-01-28 16:10:34.305239', TRUE, 'admin@atixlabs.com', '$2a$10$DfinIlWrlO5PP66sBgOb7O.GvRJMDN.isYJGIs8IecBEqelP7RCWq', 1);
INSERT INTO users (id, created, updated, active, email, password, id_role) VALUES (2, '2021-01-28 16:10:34.401136', '2021-01-28 16:10:34.401159', TRUE, 'operator@atixlabs.com','$2a$10$u3QJzu09FQ69fCHUWA4DxuS80W1qblhylez0GG42sOc91DxAVfos2', 2);
ALTER TABLE users ALTER COLUMN id RESTART WITH 3;

-- changeset martin:1609366630582-1
INSERT INTO action (id, code, description) VALUES (1, 'CREATE', 'Create');
INSERT INTO action (id, code, description) VALUES (2, 'UPDATE', 'Update');
INSERT INTO action (id, code, description) VALUES (3, 'DELETE', 'Delete');
INSERT INTO action (id, code, description) VALUES (4, 'VIEW', 'View');
ALTER TABLE action ALTER COLUMN id RESTART WITH 5;

-- changeset martin:1609366630582-11
INSERT INTO role_action (id_role, id_action) VALUES (1, 5);
INSERT INTO role_action (id_role, id_action) VALUES (1, 1);
INSERT INTO role_action (id_role, id_action) VALUES (1, 4);
INSERT INTO role_action (id_role, id_action) VALUES (1, 3);
INSERT INTO role_action (id_role, id_action) VALUES (1, 2);
INSERT INTO role_action (id_role, id_action) VALUES (2, 5);