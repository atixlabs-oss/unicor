package com.atixlabs.app.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.atixlabs.app.services.SuperSquareService;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@WithMockUser(authorities = "ROLE_ADMIN")
@SpringBootTest
public class SuperSquareControllerTest {

  @Autowired private MockMvc mockMvc;

  @InjectMocks private SuperSquareController superSquareController;

  @MockBean private SuperSquareService superSquareService;

  @Test
  public void getSuperSquares() throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(SuperSquareController.URL_MAPPING)
                .param("categoryId", "1")
                .param("superSquareId", "2"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(superSquareService, times(1)).getSuperSquares(any(Optional.class), any(Optional.class));
  }

  @Test
  public void getSuperSquaresList() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(SuperSquareController.URL_MAPPING + "/list"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(superSquareService, times(1)).getSuperSquaresList();
  }

  @Test
  public void getSuperSquaresStatus() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(SuperSquareController.URL_MAPPING + "/status"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(superSquareService, times(1))
        .getSuperSquaresStatus(any(Optional.class), any(Optional.class));
  }

  @Test
  public void getNumberMessagesByState() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(SuperSquareController.URL_MAPPING + "/number-messages"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(superSquareService, times(1))
        .getNumberMessages(any(Optional.class), any(Optional.class));
  }

  @Test
  public void getTopWords() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(SuperSquareController.URL_MAPPING + "/top-words"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }
}
