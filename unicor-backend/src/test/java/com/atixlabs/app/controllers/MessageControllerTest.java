package com.atixlabs.app.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.atixlabs.app.services.MessageService;
import com.atixlabs.app.services.dto.MessageDTO;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@WithMockUser(authorities = "ROLE_ADMIN")
@SpringBootTest
public class MessageControllerTest {

  @Autowired private MockMvc mockMvc;

  @InjectMocks private MessageController messageController;

  @MockBean private MessageService messageService;

  @Test
  public void getMessagesFilteredAndPaginated() throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(MessageController.URL_MAPPING)
                .param("page", "0")
                .param("size", "10"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(messageService, times(1))
        .getMessagesFilteredAndPaginated(
            anyInt(),
            anyInt(),
            any(Optional.class),
            any(Optional.class),
            any(Optional.class),
            any(Optional.class));
  }

  @Test
  public void getUnprocessedMessages() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(MessageController.URL_MAPPING + "/unprocessed"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(messageService, times(1)).getUnprocessedMessages();
  }

  @Test
  public void updateProcessedMessages() throws Exception {

    List<MessageDTO> messages = new ArrayList<>();
    Gson gson = new Gson();
    String jsonBody = gson.toJson(messages);

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(MessageController.URL_MAPPING + "/processed")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }
}
