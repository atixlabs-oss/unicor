package com.atixlabs.app.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.atixlabs.app.services.SocialMediaSourceService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@WithMockUser(authorities = "ROLE_ADMIN")
@SpringBootTest
public class SocialMediaSourceControllerTest {

  @Autowired private MockMvc mockMvc;

  @InjectMocks private SocialMediaSourceController socialMediaSourceController;

  @MockBean private SocialMediaSourceService socialMediaSourceService;

  @Test
  public void getAllSocialMediaSource() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(SocialMediaSourceController.URL_MAPPING))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(socialMediaSourceService, times(1)).getAllSocialMediaSource();
  }
}
