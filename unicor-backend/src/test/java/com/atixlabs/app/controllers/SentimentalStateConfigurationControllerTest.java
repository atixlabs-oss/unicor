package com.atixlabs.app.controllers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.atixlabs.app.services.SentimentStateConfigurationService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@WithMockUser(authorities = "ROLE_ADMIN")
@SpringBootTest
public class SentimentalStateConfigurationControllerTest {

  @Autowired private MockMvc mockMvc;

  @InjectMocks
  private SentimentalStateConfigurationController sentimentalStateConfigurationController;

  @MockBean private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Test
  public void getScale() throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(
                SentimentalStateConfigurationController.URL_MAPPING + "/scale"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(sentimentStateConfigurationService, times(1)).getRange();
  }

  @Test
  public void getAllSentimental() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(SentimentalStateConfigurationController.URL_MAPPING))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(sentimentStateConfigurationService, times(1)).getAllSentiments();
  }
}
