package com.atixlabs.app.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.atixlabs.app.services.CategoryService;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@WithMockUser(authorities = "ROLE_ADMIN")
@SpringBootTest
public class CategoryControllerTest {

  @Autowired private MockMvc mockMvc;

  @InjectMocks private CategoryController categoryController;

  @MockBean private CategoryService categoryService;

  @Test
  public void getAllCategories() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(CategoryController.URL_MAPPING))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(categoryService, times(1)).getAllCategories();
  }

  @Test
  public void getCategoriesTop() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get(CategoryController.URL_MAPPING + "/top"))
        .andExpect(MockMvcResultMatchers.status().isOk());

    verify(categoryService, times(1)).getTop(any(Optional.class), any(Optional.class));
  }
}
