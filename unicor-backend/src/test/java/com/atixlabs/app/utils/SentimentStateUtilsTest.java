package com.atixlabs.app.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.services.SentimentStateConfigurationService;
import com.atixlabs.security.utils.SentimentStateUtils;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SentimentStateUtilsTest {

  @Autowired private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Test
  public void getState() {
    List<SentimentStateConfiguration> configuredParameters =
        sentimentStateConfigurationService.getConfiguredParameters();

    assertEquals(
        SentimentState.GOOD,
        SentimentStateUtils.getState(new BigDecimal("1.0"), configuredParameters));
    assertEquals(
        SentimentState.GOOD,
        SentimentStateUtils.getState(new BigDecimal("0.3"), configuredParameters));

    assertEquals(
        SentimentState.NEUTRAL,
        SentimentStateUtils.getState(new BigDecimal("0.2"), configuredParameters));
    assertEquals(
        SentimentState.NEUTRAL,
        SentimentStateUtils.getState(new BigDecimal("-0.2"), configuredParameters));

    assertEquals(
        SentimentState.BAD,
        SentimentStateUtils.getState(new BigDecimal("-0.3"), configuredParameters));
    assertEquals(
        SentimentState.BAD,
        SentimentStateUtils.getState(new BigDecimal("-1.0"), configuredParameters));
  }
}
