package com.atixlabs.app.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

@SuppressWarnings("checkstyle:Indentation")
public class GoogleReviewDTO {

  @Test
  public void googleReviewNotValidIfAllFieldsAreNull() {
    com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO googleReviewDTO =
        new com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO();
    Assert.assertFalse(googleReviewDTO.isValid());
  }

  @Test
  public void googleReviewNotValidIfOneFieldsIsNull() {
    com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO googleReviewDTO =
        new com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO(
            "1", "asd", "test", "a year ago", "2021", 5D, null, "urluser", "urlImage");
    Assert.assertFalse(googleReviewDTO.isValid());
  }

  @Test
  public void googleReviewIsValidIfOnlyUrlUserIsNull() {
    com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO googleReviewDTO =
        new com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO(
            "1", "asd", "test", "a year ago", "2021", 5D, "username", null, "urlImage");
    Assert.assertTrue(googleReviewDTO.isValid());
  }

  @Test
  public void googleReviewIsValidIfAllFieldsAreNotNull() {
    com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO googleReviewDTO =
        new com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO(
            "1", "test", "asd", "a year ago", "2021", 5D, "username", "urluser", "urlImage");
    Assert.assertTrue(googleReviewDTO.isValid());
  }
}
