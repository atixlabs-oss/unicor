package com.atixlabs.app.configuration;

import static org.junit.jupiter.api.Assertions.*;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.repositories.SentimentStateConfigurationRepository;
import java.math.BigDecimal;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SentimentStateConfigurationTest {

  @Autowired private SentimentStateConfigurationRepository sentimentStateConfigurationRepository;

  @Test
  public void sentimentStateConfiguration() {
    Optional<SentimentStateConfiguration> sentimentStateGoodConfiguration =
        sentimentStateConfigurationRepository.findByState(SentimentState.GOOD);
    assertFalse(sentimentStateGoodConfiguration.isEmpty());
    assertTrue(
        sentimentStateGoodConfiguration.get().getMinimumValue().equals(new BigDecimal("0.25")));
    assertTrue(
        sentimentStateGoodConfiguration.get().getMaximumValue().equals(new BigDecimal("1.00")));

    Optional<SentimentStateConfiguration> sentimentStateNeutralConfiguration =
        sentimentStateConfigurationRepository.findByState(SentimentState.NEUTRAL);
    assertFalse(sentimentStateNeutralConfiguration.isEmpty());
    assertTrue(
        sentimentStateNeutralConfiguration.get().getMinimumValue().equals(new BigDecimal("-0.24")));
    assertTrue(
        sentimentStateNeutralConfiguration.get().getMaximumValue().equals(new BigDecimal("0.24")));

    Optional<SentimentStateConfiguration> sentimentStateBadConfiguration =
        sentimentStateConfigurationRepository.findByState(SentimentState.BAD);
    assertFalse(sentimentStateBadConfiguration.isEmpty());
    assertTrue(
        sentimentStateBadConfiguration.get().getMinimumValue().equals(new BigDecimal("-1.00")));
    assertTrue(
        sentimentStateBadConfiguration.get().getMaximumValue().equals(new BigDecimal("-0.25")));
  }
}
