package com.atixlabs.app.services.auxiliary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.SuperSquare;
import com.atixlabs.datafactory.CategoryMother;
import com.atixlabs.datafactory.SuperSquareMother;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

public class SuperSquareStatusTest {

  @Test
  public void superSquareStatus_Constructor1() {
    SuperSquare superSquare = SuperSquareMother.complete().build();
    Category category = CategoryMother.complete().build();
    SuperSquareStatus superSquareStatus = new SuperSquareStatus(superSquare, category, 0.8);

    assertEquals(superSquare, superSquareStatus.getEntity());
    assertEquals(category, superSquareStatus.getCategory());
    assertEquals(new BigDecimal("0.8"), superSquareStatus.getAverageScore());
  }
}
