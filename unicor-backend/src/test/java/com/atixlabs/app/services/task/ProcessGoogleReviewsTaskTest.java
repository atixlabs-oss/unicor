package com.atixlabs.app.services.task;

import com.atixlabs.app.model.Place;
import com.atixlabs.app.model.PlaceSocialMediaSource;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.embedables.PlaceSocialMediaSourceKey;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.services.MessageService;
import com.atixlabs.app.services.SocialMediaSourceService;
import com.atixlabs.app.socialmedia.googlereviews.service.ReviewImporterService;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Month;
import java.time.ZoneId;
import java.util.*;
import org.apache.commons.exec.DefaultExecutor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.support.CronTrigger;

public class ProcessGoogleReviewsTaskTest {

  @InjectMocks private ProcessGoogleReviewsTask processGoogleReviewsTask;

  @Mock ReviewImporterService reviewImporterService;

  @Mock DefaultExecutor defaultExecutor;

  @Mock MessageService messageService;

  @Mock SocialMediaSourceService socialMediaSourceService;

  @BeforeEach
  @SuppressWarnings("deprecation")
  void init() {
    MockitoAnnotations.initMocks(this);
  }

  List<PlaceSocialMediaSource> getGoogleReviewURLs() {
    List<PlaceSocialMediaSource> googleReviewURLS = new ArrayList<>();
    SocialMediaSource src =
        new SocialMediaSource(null, SocialMediaSourceCode.GOOGLE_REVIEW, "test");
    googleReviewURLS.add(
        (new PlaceSocialMediaSource(new PlaceSocialMediaSourceKey(), new Place(), src, "test")));
    return googleReviewURLS;
  }

  /* @Test
  public void processGoogleReviewsPythonExecutionFailsReviewImporterServiceShouldNotBeCalled()
      throws IOException {
    when(reviewImporterService.getAllGoogleMapsUrls()).thenReturn(getGoogleReviewURLs());
    when(socialMediaSourceService.findSocialMediaSourceByCode(SocialMediaSourceCode.GOOGLE_REVIEW))
        .thenReturn(new SocialMediaSource());
    when(messageService.getLastTimeUpdated(any(), any()))
        .thenReturn(Optional.of(LocalDateTime.now()));
    when(defaultExecutor.execute(any())).thenReturn(1);
    processGoogleReviewsTask.scrapGoogleReviews();
    Mockito.verify(reviewImporterService, Mockito.times(1)).getAllGoogleMapsUrls();
  }*/

  private static final String cronExpression = "0 0 0 * * *";

  @Test
  public void ifCurrentTimeIs23HoursThenProcessShouldBeCalledInOneHour() {
    final org.springframework.scheduling.support.CronTrigger trigger =
        new CronTrigger(cronExpression); // CRON expression used to call
    Calendar today = Calendar.getInstance();
    today.set(Calendar.YEAR, 2020);
    today.set(Calendar.DAY_OF_YEAR, 366);
    today.set(Calendar.HOUR_OF_DAY, 23);

    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss EEEE");
    final Date yesterday = today.getTime();
    System.out.println("Yesterday was : " + df.format(yesterday));
    Date nextExecutionTime =
        trigger.nextExecutionTime(
            new TriggerContext() {
              @Override
              public Date lastScheduledExecutionTime() {
                return yesterday;
              }

              @Override
              public Date lastActualExecutionTime() {
                return yesterday;
              }

              @Override
              public Date lastCompletionTime() {
                return yesterday;
              }
            });
    Assertions.assertEquals(
        nextExecutionTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear(), 2021);
    Assertions.assertEquals(
        nextExecutionTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getMonth(),
        Month.JANUARY);
    Assertions.assertEquals(
        nextExecutionTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getDayOfWeek(),
        DayOfWeek.FRIDAY);
    Assertions.assertEquals(
        nextExecutionTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().getMinute(),
        0);
    Assertions.assertEquals(
        nextExecutionTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().getHour(),
        0);
    Assertions.assertEquals(
        nextExecutionTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().getSecond(),
        0);

    String message = "Next Execution date: " + df.format(nextExecutionTime);
    System.out.println(message);
  }
}
