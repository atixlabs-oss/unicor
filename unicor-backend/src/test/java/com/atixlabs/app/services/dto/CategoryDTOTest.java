package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.Category;
import com.atixlabs.datafactory.CategoryMother;
import org.junit.jupiter.api.Test;

public class CategoryDTOTest {

  @Test
  public void categoryDTO_Constructor() {
    Category category = CategoryMother.complete().build();
    CategoryDTO categoryDTO = new CategoryDTO(category);

    assertEquals(category.getId(), categoryDTO.getId());
    assertEquals(category.getName(), categoryDTO.getName());
  }
}
