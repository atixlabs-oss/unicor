package com.atixlabs.app.services.auxiliary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.Place;
import com.atixlabs.datafactory.PlaceMother;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

public class PlaceGeneralStatusTest {

  @Test
  public void placeStatus_Constructor() {
    Place place = PlaceMother.complete().build();
    Long numberMessages = 10L;
    PlaceGeneralStatus placeStatus = new PlaceGeneralStatus(place, numberMessages, -0.5);

    assertEquals(place, placeStatus.getEntity());
    assertEquals(numberMessages, placeStatus.getNumberMessages());
    assertEquals(new BigDecimal("-0.5"), placeStatus.getAverageScore());
  }
}
