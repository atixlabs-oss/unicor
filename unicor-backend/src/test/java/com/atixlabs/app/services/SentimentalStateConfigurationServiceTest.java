package com.atixlabs.app.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.repositories.SentimentStateConfigurationRepository;
import com.atixlabs.app.services.dto.SentimentalStateScaleConfigurationDTO;
import java.math.BigDecimal;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class SentimentalStateConfigurationServiceTest {

  @InjectMocks private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Mock private SentimentStateConfigurationRepository sentimentStateConfigurationRepository;

  @BeforeEach
  @SuppressWarnings("deprecation")
  public void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getScaleConfiguration() {
    BigDecimal max1 = new BigDecimal(1.0);
    BigDecimal min1 = new BigDecimal(0.25);
    BigDecimal max2 = new BigDecimal(-0.25);
    BigDecimal min2 = new BigDecimal(-1);
    when(sentimentStateConfigurationRepository.findByState(SentimentState.GOOD))
        .thenReturn(
            Optional.of(new SentimentStateConfiguration(1L, SentimentState.GOOD, max1, min1)));
    when(sentimentStateConfigurationRepository.findByState(SentimentState.BAD))
        .thenReturn(
            Optional.of(new SentimentStateConfiguration(2L, SentimentState.BAD, max2, min2)));
    SentimentalStateScaleConfigurationDTO sentimentalStateScaleConfigurationDTO =
        sentimentStateConfigurationService.getRange();
    assertEquals(max1, sentimentalStateScaleConfigurationDTO.getMax());
    assertEquals(min2, sentimentalStateScaleConfigurationDTO.getMin());
  }
}
