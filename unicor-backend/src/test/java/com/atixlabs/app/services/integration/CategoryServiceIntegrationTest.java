package com.atixlabs.app.services.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.Place;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.repositories.*;
import com.atixlabs.app.services.CategoryService;
import com.atixlabs.app.services.dto.CategoryDTO;
import com.atixlabs.app.services.dto.TopCategoryDTO;
import com.atixlabs.datafactory.MessageMother;
import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class CategoryServiceIntegrationTest {

  @Autowired private CategoryService categoryService;

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private PlaceRepository placeRepository;

  @Autowired private SocialMediaSourceRepository socialMediaSourceRepository;

  @Autowired private MessageRepository messageRepository;

  @BeforeEach
  public void beforeEach() {
    messageRepository.deleteAll();
    messageRepository.flush();
  }

  @Test
  public void whenGetAllCategories_expectAllCategories() {
    List<CategoryDTO> categories = categoryService.getAllCategories();

    assertFalse(categories.isEmpty());
    assertEquals(3, categories.size());
  }

  @Test
  @Transactional
  public void whenGetTop1Categories_expectOneCategory() {
    this.generateBasicMessagesSet();

    List<TopCategoryDTO> categories = categoryService.getTop(Optional.empty(), Optional.of(1));

    assertFalse(categories.isEmpty());
    assertEquals(1, categories.size());

    TopCategoryDTO topCategoryDTO = categories.get(0);

    assertEquals(topCategoryDTO.getAverage(), new BigDecimal("0.06"));
    assertEquals(topCategoryDTO.getCount(), Long.valueOf(5));
    assertEquals(topCategoryDTO.getName(), "Servicio");
    assertEquals(topCategoryDTO.getPercentage(), new BigDecimal("53.00"));
    assertEquals(topCategoryDTO.getState(), SentimentState.NEUTRAL.name());
  }

  @Test
  @Transactional
  public void whenGetTop10Categories_expectAtMost10Categories() {
    this.generateBasicMessagesSet();

    List<TopCategoryDTO> categories = categoryService.getTop(Optional.empty(), Optional.of(10));

    assertFalse(categories.isEmpty());
    assertEquals(3, categories.size());
  }

  public void generateBasicMessagesSet() {

    SocialMediaSource source =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.GOOGLE_REVIEW).get();
    Place placeSuperSquare1 = placeRepository.findById(1L).get();
    Place placeSuperSquare2 = placeRepository.findById(6L).get();
    Category category1 = categoryRepository.findById(1L).get();
    Category category2 = categoryRepository.findById(2L).get();
    Category category3 = categoryRepository.findById(3L).get();

    List<Category> categories12 = Lists.newArrayList(category1, category2);
    List<Category> categories23 = Lists.newArrayList(category2, category3);

    Message message1 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare1)
            .categories(categories12)
            .score(new BigDecimal("0.9"))
            .sourceMessageId("1")
            .build();
    Message message2 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare1)
            .categories(categories12)
            .score(new BigDecimal("-0.3"))
            .sourceMessageId("2")
            .build();
    Message message3 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare1)
            .categories(categories23)
            .score(new BigDecimal("-0.9"))
            .sourceMessageId("3")
            .build();
    Message message4 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare2)
            .categories(categories23)
            .score(new BigDecimal("0.9"))
            .sourceMessageId("4")
            .build();
    Message message5 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare2)
            .categories(categories23)
            .score(new BigDecimal("-0.3"))
            .sourceMessageId("5")
            .build();

    messageRepository.save(message1);
    messageRepository.save(message2);
    messageRepository.save(message3);
    messageRepository.save(message4);
    messageRepository.save(message5);
  }
}
