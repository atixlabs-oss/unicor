package com.atixlabs.app.services.integration;

import static org.junit.jupiter.api.Assertions.*;

import com.atixlabs.app.exceptions.InvalidMessageScore;
import com.atixlabs.app.exceptions.InvalidWordScore;
import com.atixlabs.app.exceptions.SourceMessageIdAlreadyExistsException;
import com.atixlabs.app.model.*;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.repositories.*;
import com.atixlabs.app.services.MessageService;
import com.atixlabs.app.services.auxiliary.ConfiguredScoreRange;
import com.atixlabs.app.services.dto.*;
import com.atixlabs.datafactory.MessageMother;
import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@SpringBootTest
public class MessageServiceIntegrationTest {

  @Autowired private MessageService messageService;

  @Autowired private MessageRepository messageRepository;

  @Autowired private SocialMediaSourceRepository socialMediaSourceRepository;

  @Autowired private PlaceRepository placeRepository;

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private WordRepository wordRepository;

  @Autowired private SentimentStateConfigurationRepository sentimentStateConfigurationRepository;

  @BeforeEach
  public void beforeEach() {
    messageRepository.deleteAll();
    messageRepository.flush();

    wordRepository.deleteAll();
    wordRepository.flush();
  }

  @Test
  @Transactional
  public void getMessagesFilteredAndPaginated_whenFilterIsEmpty_expectAllMessages() {
    this.generateBasicMessagesSet();
    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0, 10, Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(5, messages.size());

    assertEquals(SocialMediaSourceCode.GOOGLE_REVIEW, messages.get(0).getSource().getCode());
    assertEquals(SocialMediaSourceCode.TWITTER, messages.get(1).getSource().getCode());
    assertEquals(SocialMediaSourceCode.FACEBOOK, messages.get(2).getSource().getCode());
    assertEquals(SocialMediaSourceCode.INSTAGRAM, messages.get(3).getSource().getCode());
    assertEquals(SocialMediaSourceCode.GOOGLE_REVIEW, messages.get(4).getSource().getCode());
  }

  @Test
  @Transactional
  public void getMessagesFilteredAndPaginated_whenFilterByTwitter_expectOneMessage() {
    this.generateBasicMessagesSet();

    Long twitterId =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.TWITTER).get().getId();
    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0,
                10,
                Optional.empty(),
                Optional.empty(),
                Optional.of(Lists.newArrayList(twitterId)),
                Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(1, messages.size());

    assertEquals(SocialMediaSourceCode.TWITTER, messages.get(0).getSource().getCode());
  }

  @Test
  @Transactional
  public void getMessagesFilteredAndPaginated_whenFilterByTwitterAndGoogle_expectThreeMessages() {
    this.generateBasicMessagesSet();

    Long twitterId =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.TWITTER).get().getId();
    Long googleId =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.GOOGLE_REVIEW).get().getId();
    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0,
                10,
                Optional.empty(),
                Optional.empty(),
                Optional.of(Lists.newArrayList(twitterId, googleId)),
                Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(3, messages.size());

    assertEquals(SocialMediaSourceCode.GOOGLE_REVIEW, messages.get(0).getSource().getCode());
    assertEquals(SocialMediaSourceCode.TWITTER, messages.get(1).getSource().getCode());
    assertEquals(SocialMediaSourceCode.GOOGLE_REVIEW, messages.get(2).getSource().getCode());
  }

  @Test
  @Transactional
  public void getMessagesFilteredAndPaginated_whenFilterByCategoryId1_expectTwoMessages() {
    this.generateBasicMessagesSet();

    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0, 10, Optional.of(1L), Optional.empty(), Optional.empty(), Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(2, messages.size());
  }

  @Test
  @Transactional
  public void
      getMessagesFilteredAndPaginated_whenFilterByCategoryId2AndSuperSquareId1_expectThreeMessages() {
    this.generateBasicMessagesSet();

    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0, 10, Optional.of(2L), Optional.of(1L), Optional.empty(), Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(3, messages.size());
  }

  @Test
  @Transactional
  public void
      getMessagesFilteredAndPaginated_whenFilterByCategoryId2AndSuperSquareId1AndGoogleReviews_expectOneMessage() {
    this.generateBasicMessagesSet();

    Long googleId =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.GOOGLE_REVIEW).get().getId();

    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0,
                10,
                Optional.of(2L),
                Optional.of(1L),
                Optional.of(Lists.newArrayList(googleId)),
                Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(1, messages.size());
    assertEquals(googleId, messages.get(0).getSource().getId());
  }

  @Test
  @Transactional
  public void getMessagesFilteredAndPaginated_whenFilterByCategoryId1AndTwitter_expectOneMessage() {
    this.generateBasicMessagesSet();

    Long twitterId =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.TWITTER).get().getId();

    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0,
                10,
                Optional.of(1L),
                Optional.empty(),
                Optional.of(Lists.newArrayList(twitterId)),
                Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(1, messages.size());
  }

  @Test
  @Transactional
  public void getMessagesFilteredAndPaginated_whenFilterBySuperSquareId1_expectThreeMessages() {
    this.generateBasicMessagesSet();

    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0, 10, Optional.empty(), Optional.of(1L), Optional.empty(), Optional.empty())
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(3, messages.size());
  }

  @Test
  @Transactional
  public void getMessagesFilteredAndPaginated_whenFilterBySentimentId1_expectTwoMessages() {
    this.generateBasicMessagesSet();

    SentimentStateConfiguration sentimentStateConfiguration =
        sentimentStateConfigurationRepository.findAll().get(0);

    List<MessageStatusDTO> messages =
        messageService
            .getMessagesFilteredAndPaginated(
                0,
                10,
                Optional.empty(),
                Optional.empty(),
                Optional.empty(),
                Optional.of(Lists.newArrayList(sentimentStateConfiguration.getId())))
            .getContent();

    assertFalse(messages.isEmpty());
    assertEquals(sentimentStateConfiguration.getState(), SentimentState.GOOD);
    assertEquals(2, messages.size());
  }

  @Test
  @Transactional
  public void getUnprocessedMessages() {
    this.generateBasicMessagesSet();
    List<MessageDTO> unprocessedMessages = messageService.getUnprocessedMessages();

    assertFalse(unprocessedMessages.isEmpty());
    assertEquals(1, unprocessedMessages.size());
  }

  @Test
  @Transactional
  public void createOrUpdateProcessedMessages_updateProcessedMessage() {

    SocialMediaSource googleSource =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.GOOGLE_REVIEW).get();
    Place placeSuperSquare2 = placeRepository.findById(6L).get();

    Message message =
        Message.builder()
            .content("Test")
            .sourceMessageId("ChdDSUhNMG9nS0VJQ0FnSUR5djV6YzlnRRAB")
            .url("Link test")
            .postDate(LocalDateTime.now().minusDays(4))
            .source(googleSource)
            .place(placeSuperSquare2)
            .score(null)
            .magnitude(null)
            .sentiment(null)
            .categories(new ArrayList<Category>())
            .words(new ArrayList<MessageWord>())
            .build();

    message = messageRepository.save(message);

    wordRepository.save(new Word("Plaza"));

    final List<String> categories = Lists.newArrayList("medioAmbIente", "Salud");
    final List<WordDTO> words =
        Lists.newArrayList(
            new WordDTO(
                "plaza", new BigDecimal("0.5"), new BigDecimal("0.5"), new BigDecimal("0.5")),
            new WordDTO(
                "Verde", new BigDecimal("0.3"), new BigDecimal("0.3"), new BigDecimal("0.3")));

    List<Category> existingCategories = categoryRepository.findAll();
    List<Word> existingWords = wordRepository.findAll();

    assertFalse(existingCategories.isEmpty());
    assertEquals(3, existingCategories.size());

    assertFalse(existingWords.isEmpty());
    assertEquals(1, existingWords.size());

    final MessageDTO messageDTO =
        MessageDTO.builder()
            .id(message.getId())
            .score(new BigDecimal(0.5))
            .magnitude(new BigDecimal(0.3))
            .sentiment(new BigDecimal(0.4))
            .categories(categories)
            .words(words)
            .build();

    Hibernate.initialize(message.getCategories());
    Hibernate.initialize(message.getWords());

    assertNull(message.getScore());
    assertTrue(message.getCategories().isEmpty());
    assertTrue(message.getWords().isEmpty());

    ProcessMessagesResult result =
        messageService.createOrUpdateProcessedMessages(Lists.newArrayList(messageDTO));

    assertEquals(1, result.getTotalMessages().intValue());
    assertEquals(1, result.getTotalValidMessages().intValue());
    assertEquals(0, result.getTotalErrorMessages().intValue());
    assertTrue(result.getErrorMessages().isEmpty());

    existingCategories = categoryRepository.findAll();
    assertEquals(4, existingCategories.size());
    assertEquals("Salud", existingCategories.get(3).getName());

    existingWords = wordRepository.findAll();
    assertEquals(2, existingWords.size());
    assertEquals("Verde", existingWords.get(1).getName());

    Message messageFinal = messageRepository.findById(message.getId()).get();

    assertNotNull(messageFinal.getScore());
    assertEquals(new BigDecimal(0.5), messageFinal.getScore());
    assertNotNull(messageFinal.getSentiment());
    assertEquals(new BigDecimal(0.4), messageFinal.getSentiment());
    assertNotNull(messageFinal.getMagnitude());
    assertEquals(new BigDecimal(0.3), messageFinal.getMagnitude());

    assertFalse(messageFinal.getCategories().isEmpty());
    assertEquals(2, messageFinal.getCategories().size());
    List<String> categoryList =
        message.getCategories().stream().map(Category::getName).collect(Collectors.toList());
    assertTrue(categoryList.contains("Medioambiente"));
    assertTrue(categoryList.contains("Salud"));

    assertFalse(message.getWords().isEmpty());
    assertEquals(2, messageFinal.getWords().size());
    List<String> wordList =
        message.getWords().stream()
            .map(messageWord -> messageWord.getWord().getName())
            .collect(Collectors.toList());
    assertTrue(wordList.contains("Plaza"));
    assertTrue(wordList.contains("Verde"));
  }

  @Test
  @Transactional
  public void createOrUpdateProcessedMessages_createProcessedMessage() {

    wordRepository.save(new Word("Plaza"));

    final List<String> categories = Lists.newArrayList("medioAmbIente", "Salud");
    final List<WordDTO> words =
        Lists.newArrayList(
            new WordDTO(
                "plaza", new BigDecimal("0.5"), new BigDecimal("0.5"), new BigDecimal("0.5")),
            new WordDTO(
                "Verde", new BigDecimal("0.3"), new BigDecimal("0.3"), new BigDecimal("0.3")));

    List<Category> existingCategories = categoryRepository.findAll();
    List<Word> existingWords = wordRepository.findAll();

    assertFalse(existingCategories.isEmpty());
    assertEquals(3, existingCategories.size());

    assertFalse(existingWords.isEmpty());
    assertEquals(1, existingWords.size());

    assertTrue(messageRepository.findAll().isEmpty());

    SocialMediaSourceDTO twitterSourceDTO =
        new SocialMediaSourceDTO(
            socialMediaSourceRepository.findByCode(SocialMediaSourceCode.TWITTER).get());

    final MessageDTO messageDTO =
        MessageDTO.builder()
            .url("Url Test")
            .sourceMessageId("ChdDSUhNMG9nS0VJQ0FnSUR5djV6YzlnRRAB")
            .source(twitterSourceDTO)
            .content("Message test")
            .score(new BigDecimal(0.7))
            .sentiment(new BigDecimal(0.6))
            .magnitude(new BigDecimal(0.5))
            .categories(categories)
            .words(words)
            .categorizable(Boolean.FALSE)
            .build();

    ProcessMessagesResult result =
        messageService.createOrUpdateProcessedMessages(Lists.newArrayList(messageDTO));

    assertEquals(1, result.getTotalMessages().intValue());
    assertEquals(1, result.getTotalValidMessages().intValue());
    assertEquals(0, result.getTotalErrorMessages().intValue());
    assertTrue(result.getErrorMessages().isEmpty());

    existingCategories = categoryRepository.findAll();
    assertEquals(4, existingCategories.size());
    assertEquals("Salud", existingCategories.get(3).getName());

    existingWords = wordRepository.findAll();
    assertEquals(2, existingWords.size());
    assertEquals("Verde", existingWords.get(1).getName());

    assertEquals(1, messageRepository.findAll().size());
    Message messageFinal = messageRepository.findAll().stream().findFirst().get();

    assertNotNull(messageFinal.getScore());
    assertEquals(new BigDecimal(0.7), messageFinal.getScore());
    assertNotNull(messageFinal.getSentiment());
    assertEquals(new BigDecimal(0.6), messageFinal.getSentiment());
    assertNotNull(messageFinal.getMagnitude());
    assertEquals(new BigDecimal(0.5), messageFinal.getMagnitude());

    assertFalse(messageFinal.getCategories().isEmpty());
    assertEquals(2, messageFinal.getCategories().size());
    List<String> categoryList =
        messageFinal.getCategories().stream().map(Category::getName).collect(Collectors.toList());
    assertTrue(categoryList.contains("Medioambiente"));
    assertTrue(categoryList.contains("Salud"));

    assertFalse(messageFinal.getWords().isEmpty());
    assertEquals(2, messageFinal.getWords().size());
    List<String> wordList =
        messageFinal.getWords().stream()
            .map(messageWord -> messageWord.getWord().getName())
            .collect(Collectors.toList());
    assertTrue(wordList.contains("Plaza"));
    assertTrue(wordList.contains("Verde"));
  }

  @Test
  public void createOrUpdateProcessedMessages_whenMessageDoesNotExist_expectThrowException() {
    MessageDTO messageDTO = MessageDTO.builder().id(50L).score(new BigDecimal(0.5)).build();

    ProcessMessagesResult result =
        messageService.createOrUpdateProcessedMessages(Lists.newArrayList(messageDTO));

    assertEquals(1, result.getTotalMessages().intValue());
    assertEquals(0, result.getTotalValidMessages().intValue());
    assertEquals(1, result.getTotalErrorMessages().intValue());

    assertFalse(result.getErrorMessages().isEmpty());
    assertEquals(1, result.getErrorMessages().size());
    assertEquals(50L, result.getErrorMessages().get(0).getMessageId().longValue());
    assertEquals("Could not find message with id 50.", result.getErrorMessages().get(0).getError());
  }

  @Test
  @Transactional
  public void createOrUpdateProcessedMessages_whenMessageScoreIsNotValid_expectFail() {
    this.generateBasicMessagesSet();
    Message message = messageRepository.findAll().stream().findFirst().get();

    MessageDTO messageDTO =
        MessageDTO.builder().id(message.getId()).score(new BigDecimal(1.5)).build();

    ProcessMessagesResult result =
        messageService.createOrUpdateProcessedMessages(Lists.newArrayList(messageDTO));

    assertEquals(1, result.getTotalMessages().intValue());
    assertEquals(0, result.getTotalValidMessages().intValue());
    assertEquals(1, result.getTotalErrorMessages().intValue());

    assertFalse(result.getErrorMessages().isEmpty());
    assertEquals(1, result.getErrorMessages().size());
    assertEquals(message.getId(), result.getErrorMessages().get(0).getMessageId());

    ConfiguredScoreRange configuredScoreRange =
        sentimentStateConfigurationRepository.getConfiguredScoreRange();
    assertEquals(
        new InvalidMessageScore(new BigDecimal(1.5), configuredScoreRange).getMessage(),
        result.getErrorMessages().get(0).getError());
  }

  @Test
  @Transactional
  public void createOrUpdateProcessedMessages_whenWordScoreIsNotValid_expectFail() {
    this.generateBasicMessagesSet();
    Message message = messageRepository.findAll().stream().findFirst().get();

    WordDTO wordDTO =
        new WordDTO("Limpio", new BigDecimal("0.5"), new BigDecimal("0.5"), new BigDecimal("-2.5"));

    MessageDTO messageDTO =
        MessageDTO.builder()
            .id(message.getId())
            .score(new BigDecimal(0.5))
            .words(Lists.newArrayList(wordDTO))
            .build();

    ProcessMessagesResult result =
        messageService.createOrUpdateProcessedMessages(Lists.newArrayList(messageDTO));

    assertEquals(1, result.getTotalMessages().intValue());
    assertEquals(0, result.getTotalValidMessages().intValue());
    assertEquals(1, result.getTotalErrorMessages().intValue());

    assertFalse(result.getErrorMessages().isEmpty());
    assertEquals(1, result.getErrorMessages().size());
    assertEquals(message.getId(), result.getErrorMessages().get(0).getMessageId());

    ConfiguredScoreRange configuredScoreRange =
        sentimentStateConfigurationRepository.getConfiguredScoreRange();
    assertEquals(
        new InvalidWordScore(wordDTO, configuredScoreRange).getMessage(),
        result.getErrorMessages().get(0).getError());
  }

  @Test
  @Transactional
  public void
      createOrUpdateProcessedMessages_whenSourceMessageIdAlreadyExists_expectThrowException() {

    SocialMediaSourceDTO twitterSourceDTO =
        new SocialMediaSourceDTO(
            socialMediaSourceRepository.findByCode(SocialMediaSourceCode.TWITTER).get());

    String sourceMessageId = "ChdDSUhNMG9nS0VJQ0FnSUR5djV6YzlnRRAB";
    final MessageDTO messageDTO =
        MessageDTO.builder()
            .url("Url Test")
            .sourceMessageId(sourceMessageId)
            .source(twitterSourceDTO)
            .content("Message test")
            .score(new BigDecimal(0.5))
            .categories(new ArrayList<>())
            .words(new ArrayList<>())
            .categorizable(Boolean.FALSE)
            .build();

    ProcessMessagesResult result =
        messageService.createOrUpdateProcessedMessages(Lists.newArrayList(messageDTO));

    assertEquals(1, result.getTotalMessages().intValue());
    assertEquals(1, result.getTotalValidMessages().intValue());
    assertEquals(0, result.getTotalErrorMessages().intValue());
    assertTrue(result.getErrorMessages().isEmpty());

    ProcessMessagesResult resultSecondTime =
        messageService.createOrUpdateProcessedMessages(Lists.newArrayList(messageDTO));

    assertEquals(1, resultSecondTime.getTotalMessages().intValue());
    assertEquals(0, resultSecondTime.getTotalValidMessages().intValue());
    assertEquals(1, resultSecondTime.getTotalErrorMessages().intValue());
    assertEquals(sourceMessageId, resultSecondTime.getErrorMessages().get(0).getSourceMessageId());
    assertEquals(
        new SourceMessageIdAlreadyExistsException(sourceMessageId).getMessage(),
        resultSecondTime.getErrorMessages().get(0).getError());
  }

  public void generateBasicMessagesSet() {

    SocialMediaSource googleSource =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.GOOGLE_REVIEW).get();
    SocialMediaSource twitterSource =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.TWITTER).get();
    SocialMediaSource facebookSource =
        socialMediaSourceRepository.save(
            SocialMediaSource.builder()
                .code(SocialMediaSourceCode.FACEBOOK)
                .name("Facebook")
                .build());
    SocialMediaSource instagramSource =
        socialMediaSourceRepository.save(
            SocialMediaSource.builder()
                .code(SocialMediaSourceCode.INSTAGRAM)
                .name("Instagram")
                .build());

    Place placeSuperSquare1 = placeRepository.findById(1L).get();
    Place placeSuperSquare2 = placeRepository.findById(6L).get();
    Category category1 = categoryRepository.findById(1L).get();
    Category category2 = categoryRepository.findById(2L).get();
    Category category3 = categoryRepository.findById(3L).get();

    List<Category> categories12 = Lists.newArrayList(category1, category2);
    List<Category> categories23 = Lists.newArrayList(category2, category3);

    Message message1 =
        MessageMother.complete()
            .postDate(LocalDateTime.now())
            .source(googleSource)
            .place(placeSuperSquare1)
            .categories(categories12)
            .score(new BigDecimal("0.9"))
            .sourceMessageId("1")
            .build();
    Message message2 =
        MessageMother.complete()
            .postDate(LocalDateTime.now().minusDays(1))
            .source(twitterSource)
            .place(placeSuperSquare1)
            .categories(categories12)
            .score(new BigDecimal("-0.3"))
            .sourceMessageId("2")
            .build();
    Message message3 =
        MessageMother.complete()
            .postDate(LocalDateTime.now().minusDays(2))
            .source(facebookSource)
            .place(placeSuperSquare1)
            .categories(categories23)
            .score(new BigDecimal("-0.9"))
            .sourceMessageId("3")
            .build();
    Message message4 =
        MessageMother.complete()
            .postDate(LocalDateTime.now().minusDays(3))
            .source(instagramSource)
            .place(placeSuperSquare2)
            .categories(categories23)
            .score(new BigDecimal("0.9"))
            .sourceMessageId("4")
            .build();
    Message message5 =
        MessageMother.complete()
            .postDate(LocalDateTime.now().minusDays(4))
            .source(googleSource)
            .place(placeSuperSquare2)
            .categories(categories23)
            .score(new BigDecimal("-0.3"))
            .sourceMessageId("5")
            .build();

    Message message6 =
        MessageMother.complete()
            .postDate(LocalDateTime.now().minusDays(4))
            .source(googleSource)
            .place(placeSuperSquare2)
            .categories(categories23)
            .score(null)
            .sourceMessageId("6")
            .build();

    messageRepository.save(message1);
    messageRepository.save(message2);
    messageRepository.save(message3);
    messageRepository.save(message4);
    messageRepository.save(message5);
    messageRepository.save(message6);
  }
}
