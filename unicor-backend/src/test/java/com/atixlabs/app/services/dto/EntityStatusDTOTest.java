package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.SuperSquare;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.datafactory.CategoryMother;
import com.atixlabs.datafactory.SuperSquareMother;
import com.google.common.collect.Lists;
import java.util.List;
import org.junit.jupiter.api.Test;

public class EntityStatusDTOTest {

  @Test
  public void entityStatusDTO_Constructor() {
    SuperSquare superSquare = SuperSquareMother.complete().build();
    List<CategoryStatusDTO> categories =
        Lists.newArrayList(
            new CategoryStatusDTO(CategoryMother.complete().build(), SentimentState.GOOD));
    EntityStatusDTO entityStatusDTO = new EntityStatusDTO(superSquare, categories);

    assertEquals(superSquare.getId(), entityStatusDTO.getId());
    assertEquals(superSquare.getName(), entityStatusDTO.getName());
    assertEquals(categories, entityStatusDTO.getCategories());
  }
}
