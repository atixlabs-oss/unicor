package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.embedables.Coordinate;
import com.atixlabs.datafactory.CoordinateMother;
import org.junit.jupiter.api.Test;

public class CoordinateDTOTest {

  @Test
  public void coordinateDTO_Constructor() {
    Coordinate coordinate = CoordinateMother.complete().build();
    CoordinateDTO coordinateDTO = new CoordinateDTO(coordinate);

    assertEquals(coordinate.getLatitude(), coordinateDTO.getLatitude());
    assertEquals(coordinate.getLongitude(), coordinateDTO.getLongitude());
  }
}
