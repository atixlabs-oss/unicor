package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.services.auxiliary.SuperSquareStatus;
import com.atixlabs.datafactory.CategoryMother;
import com.atixlabs.datafactory.SuperSquareMother;
import org.junit.jupiter.api.Test;

public class CategoryStatusDTOTest {

  @Test
  public void superSquareStatus_Constructor1() {
    Category category = CategoryMother.complete().build();
    SuperSquareStatus superSquareStatus =
        new SuperSquareStatus(SuperSquareMother.complete().build(), category, 0.3);
    SentimentState state = SentimentState.GOOD;

    CategoryStatusDTO categoryStatusDTO = new CategoryStatusDTO(superSquareStatus, state);

    assertEquals(category.getId(), categoryStatusDTO.getId());
    assertEquals(category.getName(), categoryStatusDTO.getName());
    assertEquals(state, categoryStatusDTO.getState());
  }

  @Test
  public void superSquareStatus_Constructor2() {
    Category category = CategoryMother.complete().build();
    SentimentState state = SentimentState.BAD;

    CategoryStatusDTO categoryStatusDTO = new CategoryStatusDTO(category, state);

    assertEquals(category.getId(), categoryStatusDTO.getId());
    assertEquals(category.getName(), categoryStatusDTO.getName());
    assertEquals(state, categoryStatusDTO.getState());
  }
}
