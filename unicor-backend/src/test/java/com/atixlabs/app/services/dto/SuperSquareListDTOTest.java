package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.SuperSquare;
import com.atixlabs.datafactory.SuperSquareMother;
import org.junit.jupiter.api.Test;

public class SuperSquareListDTOTest {

  @Test
  public void superSquareListDTO_Constructor() {
    SuperSquare superSquare = SuperSquareMother.complete().build();
    SuperSquareListDTO superSquareListDTO = new SuperSquareListDTO(superSquare);

    assertEquals(superSquare.getId(), superSquareListDTO.getId());
    assertEquals(superSquare.getName(), superSquareListDTO.getName());
  }
}
