package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.datafactory.MessageMother;
import com.atixlabs.datafactory.SocialMediaSourceMother;
import org.junit.jupiter.api.Test;

public class MessageDTOTest {

  @Test
  public void messageDTO_constructor() {
    SocialMediaSource socialMediaSource = SocialMediaSourceMother.complete().build();
    SocialMediaSourceDTO socialMediaSourceDTO = new SocialMediaSourceDTO(socialMediaSource);

    Message message = MessageMother.complete().build();
    MessageDTO messageDTO = new MessageDTO(message, socialMediaSourceDTO);

    assertEquals(message.getId(), messageDTO.getId());
    assertEquals(message.getSourceMessageId(), messageDTO.getSourceMessageId());
    assertEquals(message.getUrl(), messageDTO.getUrl());
    assertEquals(message.getAvatar(), messageDTO.getAvatar());
    assertEquals(message.getContent(), messageDTO.getContent());
    assertEquals(message.getPostDate(), messageDTO.getPostDate());
    assertEquals(message.getUsername(), messageDTO.getUsername());
    assertEquals(socialMediaSourceDTO, messageDTO.getSource());
    assertEquals(SentimentState.NO_STATE, messageDTO.getState());
    assertFalse(messageDTO.getCategorizable());
  }
}
