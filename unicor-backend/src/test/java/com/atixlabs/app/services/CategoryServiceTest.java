package com.atixlabs.app.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.repositories.CategoryRepository;
import com.atixlabs.app.services.dto.CategoryDTO;
import com.atixlabs.datafactory.CategoryMother;
import com.google.common.collect.Lists;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CategoryServiceTest {

  @InjectMocks private CategoryService categoryService;

  @Mock private CategoryRepository categoryRepository;

  @BeforeEach
  @SuppressWarnings("deprecation")
  void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getAllCategories() {

    when(categoryRepository.findAll())
        .thenReturn(
            Lists.newArrayList(
                CategoryMother.complete().build(),
                Category.builder().id(2L).name("Servicio").build()));

    List<CategoryDTO> categories = categoryService.getAllCategories();

    assertFalse(categories.isEmpty());
    assertEquals(2, categories.size());
  }
}
