package com.atixlabs.app.services.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.repositories.SocialMediaSourceRepository;
import com.atixlabs.app.services.SocialMediaSourceService;
import com.atixlabs.app.services.dto.SocialMediaSourceDTO;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SocialMediaSourceServiceIntegrationTest {

  @Autowired private SocialMediaSourceService socialMediaSourceService;

  @Autowired private SocialMediaSourceRepository socialMediaSourceRepository;

  @Test
  public void getAllSocialMediaSource() {

    List<SocialMediaSource> socialMediaSourceList = socialMediaSourceRepository.findAll();
    List<SocialMediaSourceDTO> socialMediaSourceDTOList =
        socialMediaSourceService.getAllSocialMediaSource();

    assertEquals(socialMediaSourceList.size(), socialMediaSourceDTOList.size());

    assertEquals(socialMediaSourceList.get(0).getId(), socialMediaSourceDTOList.get(0).getId());
    assertEquals(socialMediaSourceList.get(0).getCode(), socialMediaSourceDTOList.get(0).getCode());
    assertEquals(socialMediaSourceList.get(0).getName(), socialMediaSourceDTOList.get(0).getName());

    assertEquals(socialMediaSourceList.get(1).getId(), socialMediaSourceDTOList.get(1).getId());
    assertEquals(socialMediaSourceList.get(1).getCode(), socialMediaSourceDTOList.get(1).getCode());
    assertEquals(socialMediaSourceList.get(1).getName(), socialMediaSourceDTOList.get(1).getName());
  }
}
