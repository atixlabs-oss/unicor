package com.atixlabs.app.services.auxiliary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.SuperSquare;
import com.atixlabs.datafactory.SuperSquareMother;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

public class SuperSquareGeneralStatusTest {

  @Test
  public void superSquareStatus_Constructor() {
    SuperSquare superSquare = SuperSquareMother.complete().build();
    Long numberMessages = 20L;
    SuperSquareGeneralStatus placeStatus =
        new SuperSquareGeneralStatus(superSquare, numberMessages, -0.2);

    assertEquals(superSquare, placeStatus.getEntity());
    assertEquals(numberMessages, placeStatus.getNumberMessages());
    assertEquals(new BigDecimal("-0.2"), placeStatus.getAverageScore());
  }
}
