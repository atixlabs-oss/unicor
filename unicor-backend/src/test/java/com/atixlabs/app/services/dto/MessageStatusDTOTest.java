package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.datafactory.MessageMother;
import com.atixlabs.datafactory.SocialMediaSourceMother;
import org.junit.jupiter.api.Test;

public class MessageStatusDTOTest {

  @Test
  public void messageStatusDTO_constructor() {
    SocialMediaSource socialMediaSource = SocialMediaSourceMother.complete().build();
    SocialMediaSourceDTO socialMediaSourceDTO = new SocialMediaSourceDTO(socialMediaSource);

    Message message = MessageMother.complete().build();
    SentimentState state = SentimentState.GOOD;
    MessageStatusDTO messageDTO = new MessageStatusDTO(message, socialMediaSourceDTO, state);

    assertEquals(message.getId(), messageDTO.getId());
    assertEquals(message.getAvatar(), messageDTO.getAvatar());
    assertEquals(message.getContent(), messageDTO.getContent());
    assertEquals(message.getPostDate(), messageDTO.getPostDate());
    assertEquals(message.getUsername(), messageDTO.getUsername());
    assertEquals(socialMediaSourceDTO, messageDTO.getSource());
    assertEquals(state, messageDTO.getState());
  }
}
