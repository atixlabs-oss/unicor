package com.atixlabs.app.services.auxiliary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class NumberMessageCountTest {
  @Test
  public void superSquareStatus_Constructor1() {
    NumberMessagesCount numberMessagesCount = new NumberMessagesCount(1L, 10D);

    assertEquals(numberMessagesCount.getAverage(), Double.valueOf(10));
    assertEquals(numberMessagesCount.getCount(), Long.valueOf(1));
  }
}
