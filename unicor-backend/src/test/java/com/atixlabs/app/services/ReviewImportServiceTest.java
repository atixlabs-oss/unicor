package com.atixlabs.app.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.atixlabs.app.exceptions.GoogleReviewSourceURLNotExistsException;
import com.atixlabs.app.exceptions.RelativeDateNotSupportedException;
import com.atixlabs.app.model.Message;
import com.atixlabs.app.model.Place;
import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.socialmedia.googlereviews.dto.GoogleReviewDTO;
import com.atixlabs.app.socialmedia.googlereviews.service.ReviewImporterService;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ReviewImportServiceTest {

  @InjectMocks private ReviewImporterService reviewImporterService;

  @Mock MessageService messageService;
  @Mock SocialMediaSourceService socialMediaSourceService;

  @BeforeEach
  @SuppressWarnings("deprecation")
  void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void
      reviewImporterServiceGetLocalDateTimeInvalidRelativeDateThrowsRelativeDateNotSupportedException() {

    assertThrows(
        RelativeDateNotSupportedException.class,
        () ->
            reviewImporterService.getLocalDateTime(
                "an invalid retreval date", "2021-03-04 14:00:00.000000+00:00"));
  }

  @Test
  public void reviewImporterServiceGetLocalDateTimeRelativeDateYesterday()
      throws RelativeDateNotSupportedException {
    LocalDateTime date =
        reviewImporterService.getLocalDateTime("a day ago", "2021-03-04T14:00:00.000000+00:00");
    Assert.assertTrue(date.getYear() == 2021);
    Assert.assertTrue(date.getMonth() == Month.MARCH);
    Assert.assertTrue(date.getDayOfMonth() == 3);
    Assert.assertTrue(date.getHour() == 11);
    Assert.assertTrue(date.getMinute() == 0);
  }

  @Test
  public void reviewImporterServiceGetLocalDateTimeRelativeDateAYearAgo()
      throws RelativeDateNotSupportedException {
    LocalDateTime date =
        reviewImporterService.getLocalDateTime("a year ago", "2021-03-04T14:00:00.000000+00:00");
    Assert.assertTrue(date.getYear() == 2020);
    Assert.assertTrue(date.getMonth() == Month.MARCH);
    Assert.assertTrue(date.getDayOfMonth() == 4);
    Assert.assertTrue(date.getHour() == 11);
    Assert.assertTrue(date.getMinute() == 0);
  }

  private String createMessageOfSize(int size) {
    String message = "";
    while (message.trim().split(" ").length != size || message == "") {
      message += "a ";
    }
    return message;
  }

  @Test
  public void reviewImporterServiceProcessGoogleReviewDTOsIgnoresEmptyMessage()
      throws RelativeDateNotSupportedException, GoogleReviewSourceURLNotExistsException {
    List<GoogleReviewDTO> googleReviewDTOS =
        Arrays.asList(
            new GoogleReviewDTO(
                "",
                "",
                "",
                "a day ago",
                "2021-04-07T00:00:00.000000+00:00",
                5.1,
                "tomas",
                "url1",
                "url2"));

    Place place = new Place(1L, "place", null);
    List<Message> messages = reviewImporterService.processReviews(googleReviewDTOS, place);
    when(socialMediaSourceService.findSocialMediaSourceByCode(any()))
        .thenReturn(new SocialMediaSource(1L, SocialMediaSourceCode.GOOGLE_REVIEW, "Google"));
    when(messageService.existsMessage(any())).thenReturn(false);

    Assert.assertTrue(messages.size() == 0);
  }

  @Test
  public void
      reviewImporterServiceProcessGoogleReviewDTOsDoesntMarkMessagesWithLessThan20WordsAsCategorizable()
          throws RelativeDateNotSupportedException, GoogleReviewSourceURLNotExistsException {
    List<GoogleReviewDTO> googleReviewDTOS =
        Arrays.asList(
            new GoogleReviewDTO(
                "",
                "",
                createMessageOfSize(1),
                "a day ago",
                "2021-04-07T00:00:00.000000+00:00",
                5.1,
                "tomas",
                "url1",
                "url2"),
            new GoogleReviewDTO(
                "",
                "",
                createMessageOfSize(19),
                "a day ago",
                "2021-04-07T00:00:00.000000+00:00",
                5.1,
                "tomas",
                "url1",
                "url2"));

    Place place = new Place(1L, "place", null);
    List<Message> messages = reviewImporterService.processReviews(googleReviewDTOS, place);
    when(socialMediaSourceService.findSocialMediaSourceByCode(any()))
        .thenReturn(new SocialMediaSource(1L, SocialMediaSourceCode.GOOGLE_REVIEW, "Google"));
    when(messageService.existsMessage(any())).thenReturn(false);

    Assert.assertTrue(messages.size() == 2);
    Assert.assertTrue(!messages.get(0).getCategorizable());
    Assert.assertTrue(!messages.get(1).getCategorizable());
  }

  @Test
  public void
      reviewImporterServiceProcessGoogleReviewDTOsMarksMessagesWithSizeMoreOrEqualThan20WordsAsCategorizable()
          throws RelativeDateNotSupportedException, GoogleReviewSourceURLNotExistsException {
    List<GoogleReviewDTO> googleReviewDTOS =
        Arrays.asList(
            new GoogleReviewDTO(
                "",
                "",
                createMessageOfSize(20),
                "a day ago",
                "2021-04-07T00:00:00.000000+00:00",
                5.1,
                "tomas",
                "url1",
                "url2"),
            new GoogleReviewDTO(
                "",
                "",
                createMessageOfSize(21),
                "a day ago",
                "2021-04-07T00:00:00.000000+00:00",
                5.1,
                "tomas",
                "url1",
                "url2"));

    Place place = new Place(1L, "place", null);
    List<Message> messages = reviewImporterService.processReviews(googleReviewDTOS, place);
    when(socialMediaSourceService.findSocialMediaSourceByCode(any()))
        .thenReturn(new SocialMediaSource(1L, SocialMediaSourceCode.GOOGLE_REVIEW, "Google"));
    when(messageService.existsMessage(any())).thenReturn(false);

    Assert.assertTrue(messages.size() == 2);
    Assert.assertTrue(messages.get(0).getCategorizable());
    Assert.assertTrue(messages.get(1).getCategorizable());
  }
}
