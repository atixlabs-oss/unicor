package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.enums.SentimentState;
import org.junit.jupiter.api.Test;

public class NumberMessageDTOTest {
  @Test
  public void number_message_dto_Constructor1() {
    NumberMessagesDTO numberMessagesDTO = new NumberMessagesDTO(2L, 10D, SentimentState.BAD.name());

    assertEquals(numberMessagesDTO.getNumberMessages(), Long.valueOf(2));
    assertEquals(numberMessagesDTO.getState(), SentimentState.BAD.toString());
    assertEquals(numberMessagesDTO.getAvarage(), Double.valueOf(10));
  }
}
