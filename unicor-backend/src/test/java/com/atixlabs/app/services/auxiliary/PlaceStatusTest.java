package com.atixlabs.app.services.auxiliary;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.Place;
import com.atixlabs.datafactory.CategoryMother;
import com.atixlabs.datafactory.PlaceMother;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

public class PlaceStatusTest {

  @Test
  public void placeStatus_Constructor() {
    Place place = PlaceMother.complete().build();
    Category category = CategoryMother.complete().build();
    PlaceStatus placeStatus = new PlaceStatus(place, category, 0.7);

    assertEquals(place, placeStatus.getEntity());
    assertEquals(category, placeStatus.getCategory());
    assertEquals(new BigDecimal("0.7"), placeStatus.getAverageScore());
  }
}
