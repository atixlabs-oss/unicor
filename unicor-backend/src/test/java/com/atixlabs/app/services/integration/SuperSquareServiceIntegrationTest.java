package com.atixlabs.app.services.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.atixlabs.app.model.*;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;
import com.atixlabs.app.repositories.*;
import com.atixlabs.app.services.SentimentStateConfigurationService;
import com.atixlabs.app.services.SuperSquareService;
import com.atixlabs.app.services.dto.*;
import com.atixlabs.datafactory.MessageMother;
import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
public class SuperSquareServiceIntegrationTest {

  @Autowired private SuperSquareService superSquareService;

  @Autowired private SentimentStateConfigurationService sentimentStateConfigurationService;

  @Autowired private SuperSquareRepository superSquareRepository;

  @Autowired private MessageRepository messageRepository;

  @Autowired private CategoryRepository categoryRepository;

  @Autowired private PlaceRepository placeRepository;

  @Autowired private SocialMediaSourceRepository socialMediaSourceRepository;

  @Autowired private WordRepository wordRepository;

  @BeforeEach
  public void beforeEach() {
    messageRepository.deleteAll();
    messageRepository.flush();
  }

  @Test
  public void getSuperSquares_whenSuperSquareIdIsEmpty_expectAllSuperSquares() {
    List<SuperSquare> allSuperSquares = superSquareRepository.findAll();
    List<SuperSquareDTO> superSquares =
        superSquareService.getSuperSquares(Optional.empty(), Optional.empty());

    assertFalse(superSquares.isEmpty());
    assertEquals(allSuperSquares.size(), superSquares.size());
  }

  @Test
  public void getSuperSquares_whenSuperSquareIdIsNotEmpty_expectOneSuperSquare() {
    SuperSquare superSquare = superSquareRepository.findAll().stream().findFirst().get();
    List<SuperSquareDTO> superSquares =
        superSquareService.getSuperSquares(Optional.empty(), Optional.of(superSquare.getId()));

    assertFalse(superSquares.isEmpty());
    assertEquals(1, superSquares.size());
    assertEquals(superSquare.getId(), superSquares.get(0).getId());
    assertEquals(superSquare.getName(), superSquares.get(0).getName());
  }

  @Test
  public void whenGetSuperSquaresList_expectAllSuperSquares() {
    List<SuperSquareListDTO> superSquares = superSquareService.getSuperSquaresList();

    assertFalse(superSquares.isEmpty());
    assertEquals(4, superSquares.size());
  }

  @Test
  @Transactional
  public void getSuperSquares_whenCategoryIdAndSuperSquareIdAreEmpty_expectAllSuperSquares() {
    this.generateBasicMessagesSet();
    List<SuperSquareDTO> superSquares =
        superSquareService.getSuperSquares(Optional.empty(), Optional.empty());

    assertFalse(superSquares.isEmpty());
    assertEquals(4, superSquares.size());

    assertEquals(SentimentState.NEUTRAL, superSquares.get(0).getStatus().getState());
    assertEquals(new BigDecimal("-0.1"), superSquares.get(0).getStatus().getScore());

    assertEquals(SentimentState.GOOD, superSquares.get(1).getStatus().getState());
    assertEquals(new BigDecimal("0.3"), superSquares.get(1).getStatus().getScore());

    assertEquals(SentimentState.NO_STATE, superSquares.get(2).getStatus().getState());
    assertEquals(new BigDecimal("0.0"), superSquares.get(2).getStatus().getScore());
  }

  @Test
  @Transactional
  public void getSuperSquares_whenSuperSquareIdIsEmpty_expectAllSuperSquaresButGroupedByCategory() {
    this.generateBasicMessagesSet();
    List<SuperSquareDTO> superSquares =
        superSquareService.getSuperSquares(Optional.of(1L), Optional.empty());

    assertFalse(superSquares.isEmpty());
    assertEquals(4, superSquares.size());

    assertEquals(SentimentState.GOOD, superSquares.get(0).getStatus().getState());
    assertEquals(new BigDecimal("0.3"), superSquares.get(0).getStatus().getScore());

    assertEquals(SentimentState.NO_STATE, superSquares.get(1).getStatus().getState());
    assertEquals(new BigDecimal("0.0"), superSquares.get(1).getStatus().getScore());

    assertEquals(SentimentState.NO_STATE, superSquares.get(2).getStatus().getState());
    assertEquals(new BigDecimal("0.0"), superSquares.get(2).getStatus().getScore());
  }

  @Test
  @Transactional
  public void
      getSuperSquares_whenSuperSquareIdAndCategoryIdAreNotEmpty_expectSuperSquareFilteredAndGroupedByCategory() {
    this.generateBasicMessagesSet();
    List<SuperSquareDTO> superSquares =
        superSquareService.getSuperSquares(Optional.of(2L), Optional.of(2L));

    assertFalse(superSquares.isEmpty());
    assertEquals(1, superSquares.size());

    assertEquals(SentimentState.GOOD, superSquares.get(0).getStatus().getState());
    assertEquals(new BigDecimal("0.3"), superSquares.get(0).getStatus().getScore());
  }

  @Test
  @Transactional
  public void getSuperSquares_whenCategoryIdIsEmpty_expectSuperSquareFiltered() {
    this.generateBasicMessagesSet();
    List<SuperSquareDTO> superSquares =
        superSquareService.getSuperSquares(Optional.empty(), Optional.of(1L));

    assertFalse(superSquares.isEmpty());
    assertEquals(1, superSquares.size());

    assertEquals(SentimentState.NEUTRAL, superSquares.get(0).getStatus().getState());
    assertEquals(new BigDecimal("-0.1"), superSquares.get(0).getStatus().getScore());
  }

  @Test
  @Transactional
  public void
      getSuperSquaresStatus_whenSuperSquareIdAndCategoryIdAreEmpty_expectSuperSquaresStatus() {
    this.generateBasicMessagesSet();
    List<EntityStatusDTO> superSquaresStatus =
        superSquareService.getSuperSquaresStatus(Optional.empty(), Optional.empty());

    // SuperSquare 1
    SuperSquare superSquare1 = superSquareRepository.findById(1L).get();

    EntityStatusDTO superSquare1Status = superSquaresStatus.get(0);
    assertEquals(superSquare1.getName(), superSquare1Status.getName());
    assertEquals(3L, superSquare1Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, superSquare1Status.getState());

    List<CategoryStatusDTO> categoryStatus1 = superSquare1Status.getCategories();
    categoryStatus1.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus1.size());
    assertEquals(SentimentState.GOOD, categoryStatus1.get(0).getState());
    assertEquals(SentimentState.NEUTRAL, categoryStatus1.get(1).getState());
    assertEquals(SentimentState.BAD, categoryStatus1.get(2).getState());

    // SuperSquare 2
    SuperSquare superSquare2 = superSquareRepository.findById(2L).get();

    EntityStatusDTO superSquare2Status = superSquaresStatus.get(1);
    assertEquals(superSquare2.getName(), superSquare2Status.getName());
    assertEquals(2L, superSquare2Status.getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, superSquare2Status.getState());

    List<CategoryStatusDTO> categoryStatus2 = superSquare2Status.getCategories();
    categoryStatus2.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus2.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(0).getState());
    assertEquals(SentimentState.GOOD, categoryStatus2.get(1).getState());
    assertEquals(SentimentState.GOOD, categoryStatus2.get(2).getState());

    // SuperSquare 3
    SuperSquare superSquare3 = superSquareRepository.findById(3L).get();

    EntityStatusDTO superSquare3Status = superSquaresStatus.get(2);
    assertEquals(superSquare3.getName(), superSquare3Status.getName());
    assertEquals(0L, superSquare3Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, superSquare3Status.getState());

    List<CategoryStatusDTO> categoryStatus3 = superSquare3Status.getCategories();
    categoryStatus3.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus3.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(2).getState());
  }

  @Test
  @Transactional
  public void
      getSuperSquaresStatus_whenSuperSquareIdIsEmpty_expectSuperSquaresStatusButOnlyGeneralAndCategoryFiltered() {
    this.generateBasicMessagesSet();
    List<EntityStatusDTO> superSquaresStatus =
        superSquareService.getSuperSquaresStatus(Optional.of(2L), Optional.empty());

    // SuperSquare 1
    SuperSquare superSquare1 = superSquareRepository.findById(1L).get();

    EntityStatusDTO superSquare1Status = superSquaresStatus.get(0);
    assertEquals(superSquare1.getName(), superSquare1Status.getName());
    assertEquals(3L, superSquare1Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, superSquare1Status.getState());

    List<CategoryStatusDTO> categoryStatus1 = superSquare1Status.getCategories();
    categoryStatus1.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus1.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus1.get(0).getState());
    assertEquals(SentimentState.NEUTRAL, categoryStatus1.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus1.get(2).getState());

    // SuperSquare 2
    SuperSquare superSquare2 = superSquareRepository.findById(2L).get();

    EntityStatusDTO superSquare2Status = superSquaresStatus.get(1);
    assertEquals(superSquare2.getName(), superSquare2Status.getName());
    assertEquals(2L, superSquare2Status.getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, superSquare2Status.getState());

    List<CategoryStatusDTO> categoryStatus2 = superSquare2Status.getCategories();
    categoryStatus2.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus2.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(0).getState());
    assertEquals(SentimentState.GOOD, categoryStatus2.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(2).getState());

    // SuperSquare 3
    SuperSquare superSquare3 = superSquareRepository.findById(3L).get();

    EntityStatusDTO superSquare3Status = superSquaresStatus.get(2);
    assertEquals(superSquare3.getName(), superSquare3Status.getName());
    assertEquals(0L, superSquare3Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, superSquare3Status.getState());

    List<CategoryStatusDTO> categoryStatus3 = superSquare3Status.getCategories();
    categoryStatus3.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus3.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(2).getState());
  }

  @Test
  @Transactional
  public void getSuperSquaresStatus_whenSuperSquareIdIsNotEmpty_expectSuperSquareStatusByPlace() {
    this.generateBasicMessagesSet();
    List<EntityStatusDTO> superSquaresStatus =
        superSquareService.getSuperSquaresStatus(Optional.empty(), Optional.of(1L));

    // SuperSquare 1
    // Place 1
    List<Place> placesSuperSquare1 = placeRepository.findBySuperSquareId(1L);
    placesSuperSquare1.sort(Comparator.comparing(Place::getId));

    Place place1 = placesSuperSquare1.get(0);
    EntityStatusDTO place1Status = superSquaresStatus.get(0);
    assertEquals(place1.getName(), place1Status.getName());
    assertEquals(3L, place1Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, place1Status.getState());

    List<CategoryStatusDTO> categoryStatus1 = place1Status.getCategories();
    categoryStatus1.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus1.size());
    assertEquals(SentimentState.GOOD, categoryStatus1.get(0).getState());
    assertEquals(SentimentState.NEUTRAL, categoryStatus1.get(1).getState());
    assertEquals(SentimentState.BAD, categoryStatus1.get(2).getState());

    // Place 2
    Place place2 = placesSuperSquare1.get(1);
    EntityStatusDTO place2Status = superSquaresStatus.get(1);
    assertEquals(place2.getName(), place2Status.getName());
    assertEquals(0L, place2Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place2Status.getState());

    List<CategoryStatusDTO> categoryStatus2 = place2Status.getCategories();
    categoryStatus2.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus2.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(2).getState());

    // Place 3
    Place place3 = placesSuperSquare1.get(2);
    EntityStatusDTO place3Status = superSquaresStatus.get(2);
    assertEquals(place3.getName(), place3Status.getName());
    assertEquals(0L, place3Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place3Status.getState());

    List<CategoryStatusDTO> categoryStatus3 = place3Status.getCategories();
    categoryStatus3.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus3.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(2).getState());

    // Place 4
    Place place4 = placesSuperSquare1.get(3);
    EntityStatusDTO place4Status = superSquaresStatus.get(3);
    assertEquals(place4.getName(), place4Status.getName());
    assertEquals(0L, place4Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place4Status.getState());

    List<CategoryStatusDTO> categoryStatus4 = place4Status.getCategories();
    categoryStatus4.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus4.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus4.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus4.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus4.get(2).getState());

    // Place 5
    Place place5 = placesSuperSquare1.get(4);
    EntityStatusDTO place5Status = superSquaresStatus.get(4);
    assertEquals(place5.getName(), place5Status.getName());
    assertEquals(0L, place5Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place5Status.getState());

    List<CategoryStatusDTO> categoryStatus5 = place5Status.getCategories();
    categoryStatus5.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus5.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus5.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus5.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus5.get(2).getState());
  }

  @Test
  @Transactional
  public void
      getSuperSquaresStatus_whenSuperSquareIdAndCategoryIdAreNotEmpty_expectSuperSquareStatusByPlace() {
    this.generateBasicMessagesSet();
    List<EntityStatusDTO> superSquaresStatus =
        superSquareService.getSuperSquaresStatus(Optional.of(1L), Optional.of(1L));

    // SuperSquare 1
    // Place 1
    List<Place> placesSuperSquare1 = placeRepository.findBySuperSquareId(1L);
    placesSuperSquare1.sort(Comparator.comparing(Place::getId));

    Place place1 = placesSuperSquare1.get(0);
    EntityStatusDTO place1Status = superSquaresStatus.get(0);
    assertEquals(place1.getName(), place1Status.getName());
    assertEquals(3L, place1Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, place1Status.getState());

    List<CategoryStatusDTO> categoryStatus1 = place1Status.getCategories();
    categoryStatus1.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus1.size());
    assertEquals(SentimentState.GOOD, categoryStatus1.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus1.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus1.get(2).getState());

    // Place 2
    Place place2 = placesSuperSquare1.get(1);
    EntityStatusDTO place2Status = superSquaresStatus.get(1);
    assertEquals(place2.getName(), place2Status.getName());
    assertEquals(0L, place2Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place2Status.getState());

    List<CategoryStatusDTO> categoryStatus2 = place2Status.getCategories();
    categoryStatus2.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus2.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus2.get(2).getState());

    // Place 3
    Place place3 = placesSuperSquare1.get(2);
    EntityStatusDTO place3Status = superSquaresStatus.get(2);
    assertEquals(place3.getName(), place3Status.getName());
    assertEquals(0L, place3Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place3Status.getState());

    List<CategoryStatusDTO> categoryStatus3 = place3Status.getCategories();
    categoryStatus3.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus3.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus3.get(2).getState());

    // Place 4
    Place place4 = placesSuperSquare1.get(3);
    EntityStatusDTO place4Status = superSquaresStatus.get(3);
    assertEquals(place4.getName(), place4Status.getName());
    assertEquals(0L, place4Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place4Status.getState());

    List<CategoryStatusDTO> categoryStatus4 = place4Status.getCategories();
    categoryStatus4.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus4.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus4.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus4.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus4.get(2).getState());

    // Place 5
    Place place5 = placesSuperSquare1.get(4);
    EntityStatusDTO place5Status = superSquaresStatus.get(4);
    assertEquals(place5.getName(), place5Status.getName());
    assertEquals(0L, place5Status.getNumberMessages().longValue());
    assertEquals(SentimentState.NO_STATE, place5Status.getState());

    List<CategoryStatusDTO> categoryStatus5 = place5Status.getCategories();
    categoryStatus5.sort(Comparator.comparing(CategoryStatusDTO::getId));
    assertEquals(3, categoryStatus5.size());
    assertEquals(SentimentState.NO_STATE, categoryStatus5.get(0).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus5.get(1).getState());
    assertEquals(SentimentState.NO_STATE, categoryStatus5.get(2).getState());
  }

  @Test
  @Transactional
  public void
      getNumberMessages_whenSuperSquareIdAndCategoryIdAreNotEmpty_expectNumberMessagesFiltered() {
    this.generateBasicMessagesSet();

    List<NumberMessagesDTO> numberMessages =
        superSquareService.getNumberMessages(Optional.of(1L), Optional.of(1L));

    NumberMessagesDTO numberMessagesDTO1 = numberMessages.get(0);
    assertEquals(numberMessagesDTO1.getState(), SentimentState.GOOD.toString());
    assertEquals(numberMessagesDTO1.getNumberMessages(), Long.valueOf(1));

    NumberMessagesDTO numberMessagesDTO2 = numberMessages.get(1);
    assertEquals(numberMessagesDTO2.getState(), SentimentState.NEUTRAL.toString());
    assertEquals(numberMessagesDTO2.getNumberMessages(), Long.valueOf(0));

    NumberMessagesDTO numberMessagesDTO3 = numberMessages.get(2);
    assertEquals(numberMessagesDTO3.getState(), SentimentState.BAD.toString());
    assertEquals(numberMessagesDTO3.getNumberMessages(), Long.valueOf(1));
  }

  @Test
  @Transactional
  public void
      getNumberMessages_whenSuperSquareIdAndCategoryIdAreEmpty_expectGeneralNumberMessages() {
    this.generateBasicMessagesSet();

    List<NumberMessagesDTO> numberMessages =
        superSquareService.getNumberMessages(Optional.empty(), Optional.empty());

    NumberMessagesDTO numberMessagesDTO1 = numberMessages.get(0);
    assertEquals(numberMessagesDTO1.getState(), SentimentState.GOOD.toString());
    assertEquals(numberMessagesDTO1.getNumberMessages(), Long.valueOf(2));

    NumberMessagesDTO numberMessagesDTO2 = numberMessages.get(1);
    assertEquals(numberMessagesDTO2.getState(), SentimentState.NEUTRAL.toString());
    assertEquals(numberMessagesDTO2.getNumberMessages(), Long.valueOf(0));

    NumberMessagesDTO numberMessagesDTO3 = numberMessages.get(2);
    assertEquals(numberMessagesDTO3.getState(), SentimentState.BAD.toString());
    assertEquals(numberMessagesDTO3.getNumberMessages(), Long.valueOf(4));
  }

  @Test
  @Transactional
  public void getTopWords_whenCategoryIdAndSuperSquareIdAreEmpty_expectTop5WordsOfAllMessages() {
    this.generateBasicMessagesSet();
    List<WordStatusDTO> topWords =
        superSquareService.getTopWords(Optional.empty(), Optional.empty());

    assertFalse(topWords.isEmpty());
    assertEquals(5, topWords.size());

    assertEquals("Plaza", topWords.get(0).getWord());
    assertEquals(5L, topWords.get(0).getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, topWords.get(0).getState());

    assertEquals("Desastre", topWords.get(1).getWord());
    assertEquals(4L, topWords.get(1).getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, topWords.get(1).getState());

    assertEquals("Funcionarios", topWords.get(2).getWord());
    assertEquals(4L, topWords.get(2).getNumberMessages().longValue());
    assertEquals(SentimentState.BAD, topWords.get(2).getState());

    assertEquals("Trámites", topWords.get(3).getWord());
    assertEquals(2L, topWords.get(3).getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, topWords.get(3).getState());

    assertEquals("Pagos", topWords.get(4).getWord());
    assertEquals(1L, topWords.get(4).getNumberMessages().longValue());
    assertEquals(SentimentState.BAD, topWords.get(4).getState());
  }

  @Test
  @Transactional
  public void getTopWords_whenSuperSquareIdIs1AndCategoryIdIsEmpty_expectTopWordsSuperSquare1() {
    this.generateBasicMessagesSet();
    List<WordStatusDTO> topWords =
        superSquareService.getTopWords(Optional.empty(), Optional.of(1L));

    assertFalse(topWords.isEmpty());
    assertEquals(5, topWords.size());

    assertEquals("Desastre", topWords.get(0).getWord());
    assertEquals(3L, topWords.get(0).getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, topWords.get(0).getState());

    assertEquals("Funcionarios", topWords.get(1).getWord());
    assertEquals(3L, topWords.get(1).getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, topWords.get(1).getState());

    assertEquals("Plaza", topWords.get(2).getWord());
    assertEquals(3L, topWords.get(2).getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, topWords.get(2).getState());

    assertEquals("Trámites", topWords.get(3).getWord());
    assertEquals(2L, topWords.get(3).getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, topWords.get(3).getState());

    assertEquals("Verde", topWords.get(4).getWord());
    assertEquals(1L, topWords.get(4).getNumberMessages().longValue());
    assertEquals(SentimentState.BAD, topWords.get(4).getState());
  }

  @Test
  @Transactional
  public void getTopWords_whenSuperSquareIdIsEmptyAndCategoryIdIs1_expectTopWordsCategory1() {
    this.generateBasicMessagesSet();
    List<WordStatusDTO> topWords =
        superSquareService.getTopWords(Optional.of(1L), Optional.empty());

    assertFalse(topWords.isEmpty());
    assertEquals(5, topWords.size());

    assertEquals("Desastre", topWords.get(0).getWord());
    assertEquals(3L, topWords.get(0).getNumberMessages().longValue());
    assertEquals(SentimentState.NEUTRAL, topWords.get(0).getState());

    assertEquals("Funcionarios", topWords.get(1).getWord());
    assertEquals(2L, topWords.get(1).getNumberMessages().longValue());
    assertEquals(SentimentState.BAD, topWords.get(1).getState());

    assertEquals("Plaza", topWords.get(2).getWord());
    assertEquals(2L, topWords.get(2).getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, topWords.get(2).getState());

    assertEquals("Trámites", topWords.get(3).getWord());
    assertEquals(2L, topWords.get(3).getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, topWords.get(3).getState());

    assertEquals("Verde", topWords.get(4).getWord());
    assertEquals(1L, topWords.get(4).getNumberMessages().longValue());
    assertEquals(SentimentState.BAD, topWords.get(4).getState());
  }

  @Test
  @Transactional
  public void
      getTopWords_whenSuperSquareIdAndCategoryIdAre2_expectTopWordsOfSuperSquare2AndCategory2() {
    this.generateBasicMessagesSet();
    List<WordStatusDTO> topWords = superSquareService.getTopWords(Optional.of(2L), Optional.of(2L));

    assertFalse(topWords.isEmpty());
    assertEquals(3, topWords.size());

    assertEquals("Plaza", topWords.get(0).getWord());
    assertEquals(2L, topWords.get(0).getNumberMessages().longValue());
    assertEquals(SentimentState.GOOD, topWords.get(0).getState());

    assertEquals("Funcionarios", topWords.get(1).getWord());
    assertEquals(1L, topWords.get(1).getNumberMessages().longValue());
    assertEquals(SentimentState.BAD, topWords.get(1).getState());

    assertEquals("Pagos", topWords.get(2).getWord());
    assertEquals(1L, topWords.get(2).getNumberMessages().longValue());
    assertEquals(SentimentState.BAD, topWords.get(2).getState());
  }

  public void generateBasicMessagesSet() {

    SocialMediaSource source =
        socialMediaSourceRepository.findByCode(SocialMediaSourceCode.GOOGLE_REVIEW).get();
    Place placeSuperSquare1 = placeRepository.findById(1L).get();
    Place placeSuperSquare2 = placeRepository.findById(6L).get();
    Category category1 = categoryRepository.findById(1L).get();
    Category category2 = categoryRepository.findById(2L).get();
    Category category3 = categoryRepository.findById(3L).get();

    List<Category> categories12 = Lists.newArrayList(category1, category2);
    List<Category> categories23 = Lists.newArrayList(category2, category3);

    Message message1 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare1)
            .categories(categories12)
            .score(new BigDecimal("0.9"))
            .sourceMessageId("1")
            .build();
    Message message2 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare1)
            .categories(categories12)
            .score(new BigDecimal("-0.3"))
            .sourceMessageId("2")
            .build();
    Message message3 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare1)
            .categories(categories23)
            .score(new BigDecimal("-0.9"))
            .sourceMessageId("3")
            .build();
    Message message4 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare2)
            .categories(categories23)
            .score(new BigDecimal("0.9"))
            .sourceMessageId("4")
            .build();
    Message message5 =
        MessageMother.complete()
            .source(source)
            .place(placeSuperSquare2)
            .categories(categories23)
            .score(new BigDecimal("-0.3"))
            .sourceMessageId("5")
            .build();
    Message message6 =
        MessageMother.complete()
            .source(source)
            .categories(categories12)
            .score(new BigDecimal("-0.3"))
            .sourceMessageId("6")
            .build();

    final Word word1 = wordRepository.save(new Word(1L, "Plaza"));
    final Word word2 = wordRepository.save(new Word(2L, "Funcionarios"));
    final Word word3 = wordRepository.save(new Word(3L, "Desastre"));
    final Word word4 = wordRepository.save(new Word(4L, "Trámites"));
    final Word word5 = wordRepository.save(new Word(5L, "Verde"));
    final Word word6 = wordRepository.save(new Word(6L, "Pagos"));

    final Message messageId1 = messageRepository.save(message1);
    final Message messageId2 = messageRepository.save(message2);
    final Message messageId3 = messageRepository.save(message3);
    final Message messageId4 = messageRepository.save(message4);
    final Message messageId5 = messageRepository.save(message5);
    final Message messageId6 = messageRepository.save(message6);

    messageId1.setWords(
        Lists.newArrayList(
            MessageWord.builder()
                .word(word1)
                .message(messageId1)
                .score(new BigDecimal("0.5"))
                .build(),
            MessageWord.builder()
                .word(word2)
                .message(messageId1)
                .score(new BigDecimal("0.2"))
                .build(),
            MessageWord.builder()
                .word(word3)
                .message(messageId1)
                .score(new BigDecimal("-0.3"))
                .build(),
            MessageWord.builder()
                .word(word4)
                .message(messageId1)
                .score(new BigDecimal("0.9"))
                .build()));

    messageRepository.save(messageId1);

    messageId2.setWords(
        Lists.newArrayList(
            MessageWord.builder()
                .word(word1)
                .message(messageId2)
                .score(new BigDecimal("0.5"))
                .build(),
            MessageWord.builder()
                .word(word2)
                .message(message2)
                .score(new BigDecimal("-0.7"))
                .build(),
            MessageWord.builder()
                .word(word3)
                .message(messageId2)
                .score(new BigDecimal("-0.1"))
                .build(),
            MessageWord.builder()
                .word(word4)
                .message(messageId2)
                .score(new BigDecimal("0.4"))
                .build(),
            MessageWord.builder()
                .word(word5)
                .message(messageId2)
                .score(new BigDecimal("-0.6"))
                .build()));

    messageRepository.save(messageId2);

    messageId3.setWords(
        Lists.newArrayList(
            MessageWord.builder()
                .word(word1)
                .message(messageId3)
                .score(new BigDecimal("0.6"))
                .build(),
            MessageWord.builder()
                .word(word2)
                .message(messageId3)
                .score(new BigDecimal("0.1"))
                .build(),
            MessageWord.builder()
                .word(word3)
                .message(messageId3)
                .score(new BigDecimal("0.0"))
                .build()));

    messageRepository.save(messageId3);

    messageId4.setWords(
        Lists.newArrayList(
            MessageWord.builder()
                .word(word1)
                .message(messageId4)
                .score(new BigDecimal("0.3"))
                .build(),
            MessageWord.builder()
                .word(word2)
                .message(messageId4)
                .score(new BigDecimal("-0.7"))
                .build()));

    messageRepository.save(messageId4);

    messageId5.setWords(
        Lists.newArrayList(
            MessageWord.builder()
                .word(word1)
                .message(messageId5)
                .score(new BigDecimal("0.5"))
                .build(),
            MessageWord.builder()
                .word(word6)
                .message(messageId5)
                .score(new BigDecimal("-0.9"))
                .build()));

    messageRepository.save(messageId5);

    messageId6.setWords(
        Lists.newArrayList(
            MessageWord.builder()
                .word(word3)
                .message(messageId6)
                .score(new BigDecimal("0.5"))
                .build()));

    messageRepository.save(messageId6);
  }
}
