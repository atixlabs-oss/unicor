package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.enums.SentimentState;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

public class TopCategoryDTOTest {
  @Test
  public void topCategory_dto_Constructor() {
    TopCategoryDTO topCategoryDTO =
        new TopCategoryDTO("Servicio", 1L, 2D, 2D, SentimentState.BAD.name());

    assertEquals(topCategoryDTO.getAverage(), new BigDecimal("2.00"));
    assertEquals(topCategoryDTO.getCount(), Long.valueOf(1));
    assertEquals(topCategoryDTO.getName(), "Servicio");
    assertEquals(topCategoryDTO.getPercentage(), new BigDecimal("2.00"));
    assertEquals(topCategoryDTO.getState(), SentimentState.BAD.name());
  }
}
