package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.datafactory.SocialMediaSourceMother;
import org.junit.jupiter.api.Test;

public class SocialMediaSourceDTOTest {

  @Test
  public void socialMediaSourceDTO_constructor() {
    SocialMediaSource socialMediaSource = SocialMediaSourceMother.complete().build();
    SocialMediaSourceDTO socialMediaSourceDTO = new SocialMediaSourceDTO(socialMediaSource);

    assertEquals(socialMediaSource.getId(), socialMediaSourceDTO.getId());
    assertEquals(socialMediaSource.getCode(), socialMediaSourceDTO.getCode());
    assertEquals(socialMediaSource.getName(), socialMediaSourceDTO.getName());
  }
}
