package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.SuperSquare;
import com.atixlabs.app.model.embedables.Coordinate;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.datafactory.CoordinateMother;
import com.atixlabs.datafactory.SuperSquareMother;
import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Test;

public class SuperSquareDTOTest {

  @Test
  public void superSquareDTO_Constructor() {
    SuperSquare superSquare = SuperSquareMother.complete().build();
    Coordinate coordinate = CoordinateMother.complete().build();
    List<CoordinateDTO> polygon =
        Lists.newArrayList(new CoordinateDTO(coordinate), new CoordinateDTO(coordinate));
    StatusDTO status = new StatusDTO(SentimentState.GOOD, new BigDecimal(2.03));
    SuperSquareDTO superSquareDTO = new SuperSquareDTO(superSquare, polygon, status);

    assertEquals(superSquare.getId(), superSquareDTO.getId());
    assertEquals(superSquare.getName(), superSquareDTO.getName());
    assertEquals(polygon, superSquareDTO.getPolygon());
    assertEquals(status, superSquareDTO.getStatus());
  }
}
