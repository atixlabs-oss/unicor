package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

public class SentimentalStateScaleConfigurationDTOTest {
  @Test
  public void sentimentaLstate_scale_configuration_dto_Constructor() {
    BigDecimal max = new BigDecimal(1.0);
    BigDecimal min = new BigDecimal(-1.0);
    SentimentalStateScaleConfigurationDTO sentimentalStateScaleConfigurationDTO =
        new SentimentalStateScaleConfigurationDTO(max, min);
    assertEquals(sentimentalStateScaleConfigurationDTO.getMax(), max);
    assertEquals(sentimentalStateScaleConfigurationDTO.getMin(), min);
  }
}
