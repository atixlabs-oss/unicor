package com.atixlabs.app.services;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.atixlabs.app.model.Category;
import com.atixlabs.app.model.PlaceEntity;
import com.atixlabs.app.model.SuperSquare;
import com.atixlabs.app.model.configuration.SentimentStateConfiguration;
import com.atixlabs.app.model.enums.SentimentState;
import com.atixlabs.app.repositories.SuperSquareRepository;
import com.atixlabs.app.services.auxiliary.StatusEntity;
import com.atixlabs.app.services.auxiliary.SuperSquareGeneralStatus;
import com.atixlabs.app.services.auxiliary.SuperSquareStatus;
import com.atixlabs.app.services.dto.*;
import com.atixlabs.datafactory.CategoryMother;
import com.atixlabs.datafactory.SuperSquareMother;
import com.google.common.collect.Lists;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.jpa.domain.Specification;

public class SuperSquareServiceTest {

  @InjectMocks private SuperSquareService superSquareService;

  @Mock private SuperSquareRepository superSquareRepository;

  @Mock private CategoryService categoryService;

  @Mock private SentimentStateConfigurationService sentimentStateConfigurationService;

  @BeforeEach
  @SuppressWarnings("deprecation")
  void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  public void getSuperSquares() {

    SuperSquare superSquare = SuperSquareMother.complete().build();

    List<SuperSquare> superSquares =
        Lists.newArrayList(superSquare, SuperSquareMother.complete().id(2L).build());
    when(superSquareRepository.findAll(any(Specification.class))).thenReturn(superSquares);

    List<SuperSquareGeneralStatus> superSquaresStatus =
        Lists.newArrayList(new SuperSquareGeneralStatus(superSquare, 2L, -0.35));
    when(superSquareRepository.findSuperSquareStatusBy(any(Optional.class), any(Optional.class)))
        .thenReturn(superSquaresStatus);

    when(sentimentStateConfigurationService.getConfiguredParameters())
        .thenReturn(this.getSentimentStateConfigurationMock());

    List<SuperSquareDTO> superSquaresReturn =
        superSquareService.getSuperSquares(Optional.empty(), Optional.empty());

    assertFalse(superSquaresReturn.isEmpty());
    assertEquals(superSquares.size(), superSquaresReturn.size());

    assertEquals(SentimentState.BAD, superSquaresReturn.get(0).getStatus().getState());
    assertEquals(new BigDecimal("-0.4"), superSquaresReturn.get(0).getStatus().getScore());

    assertEquals(SentimentState.NO_STATE, superSquaresReturn.get(1).getStatus().getState());
    assertEquals(new BigDecimal("0.0"), superSquaresReturn.get(1).getStatus().getScore());
  }

  @Test
  public void getSuperSquaresList() {

    when(superSquareRepository.findAll())
        .thenReturn(
            Lists.newArrayList(
                SuperSquareMother.complete().build(),
                SuperSquareMother.complete().build(),
                SuperSquareMother.complete().build(),
                SuperSquareMother.complete().build()));

    List<SuperSquareListDTO> superSquaresList = superSquareService.getSuperSquaresList();

    assertFalse(superSquaresList.isEmpty());
    assertEquals(4, superSquaresList.size());
  }

  @Test
  public void processAndMapToEntityDTO() {

    List<Category> mainCategories =
        Lists.newArrayList(
            new Category(1L, "Medioambiente", Boolean.TRUE),
            new Category(2L, "Infraestructura", Boolean.TRUE),
            new Category(3L, "Servicio", Boolean.TRUE));

    List<SentimentStateConfiguration> configuredParameters =
        this.getSentimentStateConfigurationMock();

    SuperSquare superSquare = SuperSquareMother.complete().build();
    List<StatusEntity> entityStatusList =
        Lists.newArrayList(
            new SuperSquareStatus(superSquare, CategoryMother.complete().build(), 0.5));
    List<PlaceEntity> entityPlaceList =
        Lists.newArrayList(
            superSquare, SuperSquare.builder().id(2L).name("Super manzana 2").build());

    when(categoryService.findMainCategories()).thenReturn(mainCategories);
    List<EntityStatusDTO> status =
        superSquareService.processAndMapToEntityDTO(
            entityStatusList, entityPlaceList, configuredParameters);

    assertEquals(SentimentState.NO_STATE, status.get(0).getCategories().get(0).getState());
    assertEquals(SentimentState.GOOD, status.get(0).getCategories().get(1).getState());
    assertEquals(SentimentState.NO_STATE, status.get(0).getCategories().get(2).getState());

    assertEquals(SentimentState.NO_STATE, status.get(1).getCategories().get(0).getState());
    assertEquals(SentimentState.NO_STATE, status.get(1).getCategories().get(1).getState());
    assertEquals(SentimentState.NO_STATE, status.get(1).getCategories().get(2).getState());
  }

  @Test
  public void calculateMissingCategories() {
    List<Category> mainCategories =
        Lists.newArrayList(
            new Category(1L, "Medioambiente", Boolean.TRUE),
            new Category(2L, "Infraestructura", Boolean.TRUE),
            new Category(3L, "Servicio", Boolean.TRUE));
    List<CategoryStatusDTO> categoriesStatus =
        Lists.newArrayList(
            new CategoryStatusDTO(CategoryMother.complete().build(), SentimentState.GOOD));

    List<CategoryStatusDTO> missingCategories =
        superSquareService.calculateMissingCategories(mainCategories, categoriesStatus);
    assertEquals(2, missingCategories.size());
    assertEquals(SentimentState.NO_STATE, missingCategories.get(0).getState());
    assertEquals(SentimentState.NO_STATE, missingCategories.get(1).getState());
  }

  @Test
  public void setGeneralStateAndNumberMessages_whenFindGeneralStatus_expectFieldsAreSet() {
    List<SentimentStateConfiguration> configuredParameters =
        this.getSentimentStateConfigurationMock();
    EntityStatusDTO entityStatusDTO =
        new EntityStatusDTO(SuperSquareMother.complete().build(), null);
    List<SuperSquareGeneralStatus> generalStatus =
        Lists.newArrayList(
            new SuperSquareGeneralStatus(SuperSquareMother.complete().build(), 1000L, 0.3));

    assertNull(entityStatusDTO.getNumberMessages());
    assertNull(entityStatusDTO.getState());

    superSquareService.setGeneralStateAndNumberMessages(
        entityStatusDTO, generalStatus, configuredParameters);

    assertNotNull(entityStatusDTO.getNumberMessages());
    assertEquals(1000L, entityStatusDTO.getNumberMessages().longValue());

    assertNotNull(entityStatusDTO.getState());
    assertEquals(SentimentState.GOOD, entityStatusDTO.getState());
  }

  @Test
  public void
      setGeneralStateAndNumberMessages_whenCantFindGeneralStatus_expectNoStateAndNumberMessages0() {
    List<SentimentStateConfiguration> configuredParameters =
        this.getSentimentStateConfigurationMock();

    EntityStatusDTO entityStatusDTO =
        new EntityStatusDTO(SuperSquareMother.complete().build(), null);
    List<SuperSquareGeneralStatus> generalStatus =
        Lists.newArrayList(
            new SuperSquareGeneralStatus(SuperSquareMother.complete().id(2L).build(), 1000L, 0.3));

    assertNull(entityStatusDTO.getNumberMessages());
    assertNull(entityStatusDTO.getState());

    superSquareService.setGeneralStateAndNumberMessages(
        entityStatusDTO, generalStatus, configuredParameters);

    assertNotNull(entityStatusDTO.getNumberMessages());
    assertEquals(0L, entityStatusDTO.getNumberMessages().longValue());

    assertNotNull(entityStatusDTO.getState());
    assertEquals(SentimentState.NO_STATE, entityStatusDTO.getState());
  }

  @Test
  public void getStatus() {
    List<SentimentStateConfiguration> configuredParameters =
        this.getSentimentStateConfigurationMock();

    SuperSquare superSquare = SuperSquareMother.complete().build();
    List<SuperSquareGeneralStatus> superSquaresStatus =
        Lists.newArrayList(new SuperSquareGeneralStatus(superSquare, 2L, 0.35));
    StatusDTO status =
        superSquareService.getStatus(superSquare, superSquaresStatus, configuredParameters);

    assertEquals(SentimentState.GOOD, status.getState());
    assertEquals(new BigDecimal("0.4"), status.getScore());
  }

  private List<SentimentStateConfiguration> getSentimentStateConfigurationMock() {
    return Lists.newArrayList(
        new SentimentStateConfiguration(
            1L, SentimentState.GOOD, new BigDecimal("1.00"), new BigDecimal("0.25")),
        new SentimentStateConfiguration(
            2L, SentimentState.NEUTRAL, new BigDecimal("0.25"), new BigDecimal("-0.25")),
        new SentimentStateConfiguration(
            3L, SentimentState.BAD, new BigDecimal("-0.25"), new BigDecimal("-1.00")));
  }
}
