package com.atixlabs.app.services.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.atixlabs.app.model.enums.SentimentState;
import org.junit.jupiter.api.Test;

public class WordStatusDTOTest {

  @Test
  public void wordDTO_constructor() {
    WordStatus wordStatus = new WordStatus("Plaza", 50L, 0.3);
    WordStatusDTO wordStatusDTO = new WordStatusDTO(wordStatus, SentimentState.GOOD);

    assertEquals(wordStatus.getWord(), wordStatusDTO.getWord());
    assertEquals(wordStatus.getNumberMessages(), wordStatusDTO.getNumberMessages());
    assertEquals(SentimentState.GOOD, wordStatusDTO.getState());
  }
}
