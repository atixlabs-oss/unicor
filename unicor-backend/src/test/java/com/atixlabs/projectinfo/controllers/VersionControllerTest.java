package com.atixlabs.projectinfo.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(properties = {"project-info.buildVersion=1"})
public class VersionControllerTest {

  @Autowired private ProjectInfoController projectInfoController;

  @Test
  public void whenGetAppVersion_thenObtainBuildVersionOk() {
    ResponseEntity<?> version = projectInfoController.getVersion();
    assertEquals(version.getStatusCode(), HttpStatus.OK);
    assertEquals("1", version.getBody());
  }
}
