package com.atixlabs.security.services.integration;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.atixlabs.security.dto.AuthenticatedUserDto;
import com.atixlabs.security.exceptions.InactiveUserException;
import com.atixlabs.security.services.UserPermissionsService;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class UserPermissionsServiceIntegrationTest {

  @Autowired private UserPermissionsService userPermissionsService;

  @Test
  public void whenFindUserAuthenticatedAdmin_thenGetAuthenticatedUserDto()
      throws InactiveUserException {

    Optional<AuthenticatedUserDto> admin =
        this.userPermissionsService.findUserAuthenticated("admin@atixlabs.com", "admin");
    assertTrue(admin.isPresent());
  }
}
