package com.atixlabs.security.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import com.atixlabs.datafactory.RoleMother;
import com.atixlabs.datafactory.UserEditRequestMother;
import com.atixlabs.datafactory.UserMother;
import com.atixlabs.security.dto.UserEditRequest;
import com.atixlabs.security.dto.UserGridDTO;
import com.atixlabs.security.dto.UserProfileDTO;
import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.exceptions.ExistUserException;
import com.atixlabs.security.exceptions.InexistentUserException;
import com.atixlabs.security.exceptions.PasswordNotMatchException;
import com.atixlabs.security.exceptions.UsernameAlreadyExistsException;
import com.atixlabs.security.model.Role;
import com.atixlabs.security.model.User;
import com.atixlabs.security.repositories.RoleRepository;
import com.atixlabs.security.repositories.UserRepository;
import com.google.common.collect.Lists;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityNotFoundException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

class UserServiceImplTest {

  @InjectMocks private UserService service;

  @Mock private UserRepository repository;

  @Mock private RoleRepository roleRepository;

  @Mock private PasswordEncoder passwordEncoder;

  @BeforeEach
  @SuppressWarnings("deprecation")
  void init() {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  void findAll() {
    when(repository.findAll())
        .thenReturn(
            Stream.of(
                    new User(
                        376L,
                        "Eric",
                        "$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby",
                        Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build()),
                    new User(
                        375L,
                        "Juan",
                        "$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby",
                        Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build()))
                .collect(Collectors.toList()));
    Assertions.assertEquals(2, service.findAll().size());
  }

  // TODO: write all other methods to test.

  @Ignore
  void updateValidUser() {
    long userId = 10;
    User user =
        new User(10L, "oldName", "", Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build());
    when(repository.findById(userId)).thenReturn(Optional.of(user));
    User userExpected =
        new User(10L, "newName", "", Role.builder().code(RoleCode.ROLE_ADMIN).build());
    userExpected.setEmail("newUsername@atixlabs.com");
    Optional<User> retrievedUser = Optional.empty();

    Map<String, Object> updates = new HashMap<>();
    updates.put("selectedKey", "2");
    updates.put("lastName", "newSurname");
    updates.put("role", RoleCode.ROLE_ADMIN.role());
    updates.put("phone", "1140515445");
    updates.put("name", "newName");
    updates.put("password", "nPass12345");
    updates.put("confirmPassword", "nPass12345");
    updates.put("type", "ZONES");
    updates.put("email", "newUsername@atixlabs.com");
    updates.put("username", "newUsername@atixlabs.com");
    try {
      retrievedUser = service.updateUser(updates, userId);
    } catch (Exception ex) {
      Assert.fail("exception during update");
    }
    if (!retrievedUser.isPresent()) Assert.fail();
    assertTrue(userExpected.equals(retrievedUser.get()));
  }

  @Test
  void updateInexistentUserAndExpectInexistentUserException() {
    long userId = 10;
    when(repository.findById(userId)).thenReturn(Optional.empty());
    Map<String, Object> updates = new HashMap<>();
    Optional<User> retrievedUser;
    InexistentUserException ex =
        assertThrows(
            InexistentUserException.class,
            () -> service.updateUser(updates, userId),
            "Expected to throw inexistent user");
  }

  @Test
  void updateExistentUserWithExistentMailExpectToDoNothing() {
    String email = "estoppel@atixlabs.com";
    long idUser = 10L;
    User user = new User();
    user.setEmail(email);
    user.setId(11L);
    when(repository.findById(idUser)).thenReturn(Optional.of(user));
    when(repository.findByEmail(email)).thenReturn(Optional.of(user));
    when(passwordEncoder.matches(any(), any())).thenReturn(false);
    Map<String, Object> updates = new HashMap<>();
    updates.put("email", email);
    Optional<User> retrievedUser = Optional.empty();
    try {
      retrievedUser = service.updateUser(updates, idUser);
    } catch (Exception ex) {
      Assert.fail("exception during update");
    }

    Assertions.assertEquals(Optional.empty(), retrievedUser);
  }

  @Test
  void setDisableUser() {
    User user1 = new User();
    User user2 = new User();
    assertTrue(user1.isActive());
    assertTrue(user2.isActive());
    service.putDisableOnUsers(Lists.newArrayList(user1, user2));
    assertFalse(user1.isActive());
    assertFalse(user2.isActive());
  }

  @Test
  void createOrEditUser() throws ExistUserException, PasswordNotMatchException {
    UserEditRequest userDTO = UserEditRequestMother.complete().build();
    User user = UserMother.complete().build();

    when(repository.findByEmail(anyString())).thenReturn(Optional.empty());
    when(roleRepository.findByCode(any(RoleCode.class))).thenReturn(RoleMother.complete().build());
    when(repository.save(any(User.class))).thenReturn(user);
    User userResult = service.createOrEdit(userDTO);
    Assertions.assertEquals(user.getEmail(), userResult.getEmail());
  }

  @Test
  void createOrEditUser_whenCreateUserAndUsernameExists_thenExpectThrowException() {
    UserEditRequest userDTO = UserEditRequestMother.complete().id(1L).build();
    User user = UserMother.complete().id(2L).build();

    when(repository.findById(anyLong())).thenReturn(Optional.of(user));
    when(repository.findByEmail(anyString())).thenReturn(Optional.of(user));
    assertThrows(
        UsernameAlreadyExistsException.class,
        () -> {
          service.createOrEdit(userDTO);
        });
  }

  @Test
  void constructorUserGridDTO() {
    User user = UserMother.complete().id(20L).build();
    UserGridDTO userGridDTO = new UserGridDTO(user);
    assertEquals(user.getId(), userGridDTO.getId());
    assertEquals(user.getEmail(), userGridDTO.getEmail());
    assertEquals(user.isActive(), userGridDTO.isActive());
  }

  @Test
  void constructorUserProfileDTO() {
    User user = UserMother.complete().build();

    UserProfileDTO userProfileDTO = new UserProfileDTO(user);
    assertEquals(user.getId(), userProfileDTO.getId());
    assertEquals(user.getEmail(), userProfileDTO.getEmail());
    assertEquals(user.getEmail(), userProfileDTO.getEmail());
  }

  @Test
  public void enable() {
    User user = UserMother.complete().active(false).build();
    when(repository.findById(anyLong())).thenReturn(Optional.of(user));
    when(repository.save(any(User.class))).thenAnswer(i -> i.getArguments()[0]);

    assertFalse(user.isActive());

    User userResult = service.enable(1L);
    assertTrue(userResult.isActive());
  }

  @Test
  public void setActive_whenUserDoesNoExist_expectFail() {
    when(repository.findById(anyLong())).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, () -> service.setActive(1L, Boolean.TRUE));
  }

  @Test
  public void disable() {
    User user = UserMother.complete().active(true).build();
    when(repository.findById(anyLong())).thenReturn(Optional.of(user));
    when(repository.save(any(User.class))).thenAnswer(i -> i.getArguments()[0]);

    assertTrue(user.isActive());

    User userResult = service.disable(1L);
    assertFalse(userResult.isActive());
  }

  @Test
  public void getProfile() {
    User user = UserMother.complete().id(1L).build();
    when(repository.findById(anyLong())).thenReturn(Optional.of(user));

    UserProfileDTO userProfile = service.getProfile(1L);
    assertEquals(1L, userProfile.getId().longValue());
  }
}
