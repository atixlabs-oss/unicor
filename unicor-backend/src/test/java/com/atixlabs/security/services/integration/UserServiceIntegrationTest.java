package com.atixlabs.security.services.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.atixlabs.datafactory.UserMother;
import com.atixlabs.security.dto.FilterUserDto;
import com.atixlabs.security.dto.UserGridDTO;
import com.atixlabs.security.model.User;
import com.atixlabs.security.repositories.UserRepository;
import com.atixlabs.security.services.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class UserServiceIntegrationTest {

  @Autowired private UserService userService;

  @Autowired private UserRepository userRepository;

  @Test
  public void findAllWithFilter_whenSearchMartin_thenReturnMartinUser() {

    this.generateBasicUsersSet();

    Pageable pageRequest = PageRequest.of(0, 20);
    FilterUserDto filter = FilterUserDto.builder().search("Martin").build();
    Page<UserGridDTO> users = userService.findUsersFilteredAndPaginated(filter, pageRequest);
    assertFalse(users.isEmpty());
    assertEquals(1, users.getTotalElements());
    assertEquals("Martin", users.getContent().get(0).getEmail());
  }

  private void generateBasicUsersSet() {

    User user1 = UserMother.complete().id(1L).password("fedeadmin1").build();
    User user2 = UserMother.complete().id(2L).email("Martin").password("martinadmin1").build();
    User user3 = UserMother.complete().id(3L).email("German").password("germanadmin1").build();
    User user4 = UserMother.complete().id(4L).email("Paula").password("paulaadmin1").build();

    userRepository.save(user1);
    userRepository.save(user2);
    userRepository.save(user3);
    userRepository.save(user4);
  }
}
