package com.atixlabs.security.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.when;

import com.atixlabs.datafactory.UserMother;
import com.atixlabs.security.dto.FilterUserDto;
import com.atixlabs.security.dto.UserGridDTO;
import com.atixlabs.security.dto.UserProfileDTO;
import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.exceptions.UserNotFoundException;
import com.atixlabs.security.model.Role;
import com.atixlabs.security.model.User;
import com.atixlabs.security.repositories.RoleRepository;
import com.atixlabs.security.repositories.UserRepository;
import com.atixlabs.security.services.UserService;
import com.google.common.collect.Lists;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@SpringBootTest
class UsersControllerTest {

  private String user =
      "{\n"
          + "\t\"id\":\"2\",\n"
          + "\t\"username\":\"erics@atixlabs.com\",\n"
          + "\t\"email\":\"erics@atsixlabs.com\",\n"
          + "\t\"name\":\"Eric\",\n"
          + "\t\"lastName\":\"stoppel\",\n"
          + "\t\"password\":\"aA1234566\",\n"
          + "\t\"confirmPassword\":\"aA1234566\",\n"
          + "\t\"phone\":\"123456789\",\n"
          + "\t\"role\":\"ROLE_ADMIN\",\n"
          + "\t\"type\":\"GLOBAL\"\n"
          + "}";

  @InjectMocks private UsersController usersController;

  private MockMvc mockMvc;

  @Mock private UserService userService;

  @Mock private RoleRepository roleRepository;

  @Mock private UserRepository userRepository;

  @BeforeEach
  void setUp() {
    mockMvc = MockMvcBuilders.standaloneSetup(usersController).build();
  }

  @Test
  void findAll() throws Exception {
    when(roleRepository.findByCode(RoleCode.ROLE_ADMIN))
        .thenReturn(Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build());
    when(userRepository.findAll())
        .thenReturn(
            Stream.of(
                    new User(
                        376L,
                        "Eric@test.com",
                        "$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby",
                        Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build()),
                    new User(
                        375L,
                        "Juan@test.com",
                        "$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby",
                        Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build()))
                .collect(Collectors.toList()));

    MvcResult result =
        mockMvc
            .perform(MockMvcRequestBuilders.get("/users").accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();
  }

  @Test
  void create() throws Exception {
    when(userRepository.findAll())
        .thenReturn(
            Stream.of(
                    new User(
                        376L,
                        "Eric@test.com",
                        "$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby",
                        Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build()),
                    new User(
                        375L,
                        "Juan@test.com",
                        "$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby",
                        Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build()))
                .collect(Collectors.toList()));

    mockMvc
        .perform(MockMvcRequestBuilders.get("/users").accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void updateNonExistentUser_andExpectToFail() throws Exception {
    when(userService.findById(any(Long.class))).thenThrow(UsernameNotFoundException.class);

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(UsersController.URL_MAPPING_USERS + "/1")
                .content(user)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  void updateExistentUser_andExpectAUser() throws Exception {

    when(userService.findById(any(Long.class))).thenReturn(new User());
    when(userRepository.save(any(User.class))).thenReturn(new User());

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(UsersController.URL_MAPPING_USERS + "/2")
                .content(user)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void createUserWithInvalidPassword_andExpectToFail() throws Exception {
    String user =
        "{\n"
            + "\t\"email\":\"erics@atsixlabs.com\",\n"
            + "\t\"password\":\"a\",\n"
            + "\t\"confirmPassword\":\"a\",\n"
            + "\t\"role\":\"ROLE_ADMIN\",\n"
            + "\t\"type\":\"GLOBAL\"\n"
            + "}";

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(UsersController.URL_MAPPING_USERS)
                .content(user)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isBadRequest());
  }

  @Test
  void createValidUser() throws Exception {
    String user =
        "{\n"
            + "\t\"email\":\"erics@atsixlabs.com\",\n"
            + "\t\"password\":\"Aa123456\",\n"
            + "\t\"confirmPassword\":\"Aa123456\",\n"
            + "\t\"role\":\"ROLE_ADMIN\",\n"
            + "\t\"type\":\"GLOBAL\"\n"
            + "}";

    mockMvc
        .perform(
            MockMvcRequestBuilders.post(UsersController.URL_MAPPING_USERS)
                .content(user)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isCreated());
  }

  @Test
  void findById() throws Exception {
    when(userService.findById(any(Long.class))).thenReturn(this.getUserMock());

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(UsersController.URL_MAPPING_USERS + "/1")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void findByIdNonExistentUser_andExpectToFail() throws Exception {
    when(userService.findById(any(Long.class))).thenThrow(UserNotFoundException.class);

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(UsersController.URL_MAPPING_USERS + "/1")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  void whenActivateUser_thenSetUserActive() throws Exception {
    when(userService.setActive(376L, true)).thenReturn(this.getUserMock());

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(UsersController.URL_MAPPING_USERS + "/376/active/true")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());

    Mockito.verify(userService, Mockito.times(1)).setActive(376L, true);
  }

  @Test
  void whenActivateUserThatNotExists_thenReturnNotFound() throws Exception {
    when(userService.setActive(any(Long.class), any(Boolean.class)))
        .thenThrow(new EntityNotFoundException());

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(UsersController.URL_MAPPING_USERS + "/376/active/true")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isNotFound());

    Mockito.verify(userService, Mockito.times(1)).setActive(376L, true);
  }

  @Test
  void findAllWithFilters() throws Exception {
    Pageable pageRequest = PageRequest.of(0, 20);
    User user = UserMother.complete().build();
    List<UserGridDTO> users = Lists.newArrayList(new UserGridDTO(user));
    Page<UserGridDTO> pageUsersGridDTO = new PageImpl<>(users, pageRequest, users.size());
    when(userService.findUsersFilteredAndPaginated(any(FilterUserDto.class), any(Pageable.class)))
        .thenReturn(pageUsersGridDTO);

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(UsersController.URL_MAPPING_USERS + "/filters")
                .param("page", "0")
                .param("size", "20")
                .param("searchCriteria", "GoodYear")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void getProfile() throws Exception {
    when(userService.getProfile(anyLong())).thenReturn(new UserProfileDTO());

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(UsersController.URL_MAPPING_USERS + "/profile")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void getProfile_whenUserDoesNotExist_expectNotFound() throws Exception {
    when(userService.getProfile(any())).thenThrow(new UserNotFoundException(1L));

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(UsersController.URL_MAPPING_USERS + "/profile")
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  private User getUserMock() {
    return new User(
        376L,
        "Eric@test.com",
        "$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby",
        Role.builder().code(RoleCode.ROLE_ADMIN).id(1).build());
  }
}
