package com.atixlabs.security.controllers;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import com.atixlabs.datafactory.RoleMother;
import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.exceptions.ExistUserException;
import com.atixlabs.security.exceptions.PasswordNotMatchException;
import com.atixlabs.security.model.Role;
import com.atixlabs.security.model.User;
import com.atixlabs.security.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@Slf4j
public abstract class ContextRestController {

  @LocalServerPort protected int port;

  @Autowired UserRepository repository;

  @BeforeEach
  public void beforeLoggin() throws ExistUserException, PasswordNotMatchException {

    repository.deleteAll();
    repository.flush();
    User user = new User();
    user.setEmail("admin@atixlabs.com");
    user.setPassword("admin1");
    user.setRole(RoleMother.complete().build());
    repository.save(user);

    user = new User();
    user.setEmail("operator@atixlabs.com");
    user.setPassword("operator1");
    user.setRole(Role.builder().id(2).description("VIEWER").code(RoleCode.ROLE_OPERATOR).build());
    repository.save(user);
    repository.flush();
  }

  @AfterEach
  public void after() {
    repository.deleteAll();
  }
}
