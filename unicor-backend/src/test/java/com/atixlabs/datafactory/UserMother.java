package com.atixlabs.datafactory;

import com.atixlabs.security.model.User;

public class UserMother {

  public static User.UserBuilder complete() {
    return User.builder()
        .id(1L)
        .password("$2y$12$IAQ4rJsx0Zu5cCphS3jrju7WE4QcUrkF/ZkwZukjMTQiTuMQu0tby")
        .email("fortmann@atixlabs.com")
        .role(RoleMother.complete().build())
        .active(true);
  }
}
