package com.atixlabs.datafactory;

import com.atixlabs.app.model.Vertex;

public class VertexMother {

  public static Vertex.VertexBuilder complete() {
    return Vertex.builder().id(1L).coordinate(CoordinateMother.complete().build());
  }
}
