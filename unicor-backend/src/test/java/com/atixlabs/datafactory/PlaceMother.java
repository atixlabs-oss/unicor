package com.atixlabs.datafactory;

import com.atixlabs.app.model.Place;

public class PlaceMother {

  public static Place.PlaceBuilder complete() {
    return Place.builder()
        .id(1L)
        .name("Plaza San Martín")
        .superSquare(SuperSquareMother.complete().build());
  }
}
