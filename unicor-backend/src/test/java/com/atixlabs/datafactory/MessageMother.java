package com.atixlabs.datafactory;

import com.atixlabs.app.model.Message;
import java.math.BigDecimal;
import java.time.LocalDateTime;

public class MessageMother {

  public static Message.MessageBuilder complete() {
    return Message.builder()
        .postDate(LocalDateTime.now())
        .sourceMessageId("ChdDSUhNMG9nS0VJQ0FnSUR5djV6YzlnRRAB")
        .source(SocialMediaSourceMother.complete().build())
        .content("Test")
        .score(new BigDecimal("0.1"))
        .sentiment(new BigDecimal("0.2"))
        .magnitude(new BigDecimal("0.3"))
        .url("Test link")
        .username("Pablo Perez")
        .categorizable(Boolean.FALSE);
  }
}
