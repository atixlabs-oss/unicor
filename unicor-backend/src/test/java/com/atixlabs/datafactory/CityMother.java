package com.atixlabs.datafactory;

import com.atixlabs.app.model.City;

public class CityMother {

  public static City.CityBuilder complete() {
    return City.builder().id(1L).name("Ciudad de Córdoba");
  }
}
