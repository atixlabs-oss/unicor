package com.atixlabs.datafactory;

import com.atixlabs.app.model.Category;

public class CategoryMother {

  public static Category.CategoryBuilder complete() {
    return Category.builder().id(1L).name("Medioambiente");
  }
}
