package com.atixlabs.datafactory;

import com.atixlabs.app.model.SocialMediaSource;
import com.atixlabs.app.model.enums.SocialMediaSourceCode;

public class SocialMediaSourceMother {

  public static SocialMediaSource.SocialMediaSourceBuilder complete() {
    return SocialMediaSource.builder()
        .id(1L)
        .code(SocialMediaSourceCode.GOOGLE_REVIEW)
        .name("Google Review");
  }
}
