package com.atixlabs.datafactory;

import com.atixlabs.app.model.embedables.Coordinate;
import java.math.BigDecimal;

public class CoordinateMother {

  public static Coordinate.CoordinateBuilder complete() {
    return Coordinate.builder()
        .latitude(new BigDecimal(-31.416192891095786))
        .longitude(new BigDecimal(-64.18722681596564));
  }
}
