package com.atixlabs.datafactory;

import com.atixlabs.app.model.SuperSquare;
import com.google.common.collect.Lists;

public class SuperSquareMother {

  public static SuperSquare.SuperSquareBuilder complete() {

    return SuperSquare.builder()
        .id(1L)
        .name("Super Manzana 1")
        .vertices(
            Lists.newArrayList(VertexMother.complete().build(), VertexMother.complete().build()))
        .city(CityMother.complete().build());
  }
}
