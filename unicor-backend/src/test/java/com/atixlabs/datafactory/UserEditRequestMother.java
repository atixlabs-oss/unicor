package com.atixlabs.datafactory;

import com.atixlabs.security.dto.UserEditRequest;

public class UserEditRequestMother {
  public static UserEditRequest.UserEditRequestBuilder complete() {
    return UserEditRequest.builder().email("asd@email.com").role("ROLE_ADMIN");
  }
}
