package com.atixlabs.datafactory;

import com.atixlabs.security.enums.RoleCode;
import com.atixlabs.security.model.Role;

public class RoleMother {
  public static Role.RoleBuilder complete() {
    return Role.builder().id(1).description("ADMIN").code(RoleCode.ROLE_ADMIN);
  }
}
