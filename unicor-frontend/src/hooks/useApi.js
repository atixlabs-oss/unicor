import { useEffect, useState } from 'react';

export const useGetApi = (next, defaultData = {}, defaultLoading = false) => {
  const [response, setResponse] = useState({
    error: '',
    loading: defaultLoading,
    data: defaultData
  });

  const getData = async (...values) => {
    setResponse(response_ => ({ ...response_, loading: true }));
    try {
      const data = await next(...values);
      setResponse(response_ => ({ ...response_, data }));
    } catch (error) {
      setResponse(response_ => ({ ...response_, error: error.response.data.error }));
    }
    setResponse(response_ => ({ ...response_, loading: false }));
  };

  return { ...response, getData };
};

export const useEffectFilterApi = (next, filter, defaultData) => {
  const { data, getData, error, loading } = useGetApi(next, defaultData);
  const [firstLoading, setFirstLoading] = useState(true);

  const exNext = async () => {
    await getData(filter);
    setFirstLoading(false);
  };

  useEffect(() => {
    exNext();
  }, [filter]);

  return { data, error, loading, firstLoading };
};
