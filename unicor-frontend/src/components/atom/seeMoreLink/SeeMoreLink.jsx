import React from 'react';
import PropTypes from 'prop-types';

import './see-more-link.scss';

const SeeMoreLink = ({ onClickHandler, text }) => (
  <span className="showmore-link" onClick={onClickHandler}>
    {text}
  </span>
);

export default SeeMoreLink;

SeeMoreLink.propTypes = {
  onClickHandler: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired
};
