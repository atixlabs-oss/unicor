import { Select } from 'antd';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { useGetApi } from '../../hooks/useApi';

const { Option } = Select;

const SelectSearchAtom = ({ fetchData, placeholder, onChange, defaultValue }) => {
  const { data: options, loading, getData } = useGetApi(fetchData, []);

  useEffect(() => {
    getData();
  }, []);

  const handleChange = id_ => onChange(options.find(({ id }) => id === id_));

  return (
    <Select
      showSearch
      loading={loading}
      style={{ width: 200 }}
      placeholder={placeholder}
      className="MapFilter"
      size="large"
      onChange={handleChange}
      value={defaultValue}
      allowClear
      optionFilterProp="children"
      filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
    >
      {options.map(({ id, name }) => (
        <Option key={id} value={id}>
          {name}
        </Option>
      ))}
    </Select>
  );
};

export default SelectSearchAtom;

SelectSearchAtom.propTypes = {
  defaultValue: PropTypes.number,
  fetchData: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired
};
