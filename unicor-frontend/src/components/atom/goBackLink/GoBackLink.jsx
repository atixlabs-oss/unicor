import React from 'react';
import PropTypes from 'prop-types';
import ArrowLeftIcon from '../icons/ArrowLeftIcon'

import './go-back-link.scss';

const GoBackLink = ({ onClickHandler, text }) => (
  <div className="goback-container" onClick={onClickHandler}>
    <ArrowLeftIcon style="goBack-icon" />
    <p className="goBack-text">{text}</p>
  </div>
);

export default GoBackLink;

GoBackLink.propTypes = {
  onClickHandler: PropTypes.func.isRequired,
  text: PropTypes.string,
};
