import React from 'react';
import PropTypes from 'prop-types';
import UserIcon from '../icons/UserIcon'

import './login-button.scss';

const LoginButton = ({ onClickHandler }) => (
  <div className="login-btn--container" onClick={onClickHandler}>
    <UserIcon style="login-btn--icon" />
    <p className="login-btn--text">Iniciar sesión</p>
  </div>
);

export default LoginButton;

LoginButton.propTypes = {
  onClickHandler: PropTypes.func.isRequired,
};
