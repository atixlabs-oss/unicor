import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import * as d3 from 'd3';

// (Los estilos de los textos se pueden definir directamente en las lineas 124 y 137. Para el circulo, linea 107)
// Default del estilo del valor que se muestra en el circulo
const VALUE_FONT = {
  family: 'Work Sans',
  size: 10,
  color: '#515E6B',
  weight: '400'
};

// Default del estilo del texto/titulo que se muestra en el circulo
const LABEL_FONT = {
  family: 'Work Sans',
  size: 14,
  color: '#515E6B',
  weight: 'bold'
};

// TODO: Logica para calcular el tamaño de los Bubble
const calculateSize = ({ value }) => {
  if (value >= 1000) {
    return value * 0.8;
  }
  return value;
};

export default class BubbleChart extends Component {
  constructor(properties) {
    super(properties);

    this.renderChart = this.renderChart.bind(this);
    this.renderBubbles = this.renderBubbles.bind(this);
  }

  componentDidMount() {
    this.svg = ReactDOM.findDOMNode(this);
    this.renderChart();
  }

  componentDidUpdate() {
    const { width, height } = this.props;
    if (width !== 0 && height !== 0) {
      this.renderChart();
    }
  }

  render() {
    const { width, height } = this.props;
    return <svg width={width} height={height} />;
  }

  renderChart() {
    const { overflow, graph, data, width, padding } = this.props;
    this.svg.innerHTML = '';
    if (overflow) this.svg.style.overflow = 'visible';

    const bubblesWidth = width;
    const color = d3.scaleOrdinal(d3.schemeCategory20c);

    const pack = d3
      .pack()
      .size([bubblesWidth * graph.zoom, bubblesWidth * graph.zoom])
      .padding(padding);

    // Se calcula los tamaños a partir de los valores;
    const root = d3
      .hierarchy({ children: data })
      .sum(calculateSize)
      .sort((a, b) => b.value - a.value)
      .each(d => {
        if (d.data.label) {
          d.label = d.data.label;
          d.id = d.data.label.toLowerCase().replace(/ |\//g, '-');
        }
      });

    // Se calcula la distribucion
    const nodes = pack(root).leaves();

    // Se llama a la funcion para dibujar los Bubbles
    this.renderBubbles(bubblesWidth, nodes, color);
  }

  renderBubbles(width, nodes, color) {
    const { graph, valueFont, labelFont } = this.props;

    // Grafico contenedor
    const bubbleChart = d3
      .select(this.svg)
      .append('g')
      .attr('class', 'bubble-chart')
      .attr('transform', d => `translate(${width * graph.offsetX},${width * graph.offsetY})`);

    const node = bubbleChart
      .selectAll('.node')
      .data(nodes)
      .enter()
      .append('g')
      .attr('class', 'node')
      .attr('transform', d => `translate(${d.x},${d.y})`);

    // Estilo del circulo
    node
      .append('circle')
      .attr('id', d => d.id)
      .attr('r', d => d.r - d.r * 0.04)
      .style('fill', () => 'transparent')
      .style('stroke', d => (d.data.color ? d.data.color : color(nodes.indexOf(d))))
      .style('stroke-width', () => 8)
      .style('z-index', 1);

    node
      .append('clipPath')
      .attr('id', d => `clip-${d.id}`)
      .append('use')
      .attr('xlink:href', d => `#${d.id}`);

    // Estilo del texto
    node
      .append('text')
      .attr('class', 'label-text')
      .style('font-size', `${labelFont.size}px`)
      .attr('clip-path', d => `url(#clip-${d.id})`)
      .style('font-weight', () => (labelFont.weight ? labelFont.weight : 600))
      .style('font-family', labelFont.family)
      .style('fill', () => (labelFont.color ? labelFont.color : '#000'))
      .style('stroke', () => (labelFont.lineColor ? labelFont.lineColor : '#000'))
      .style('stroke-width', () => (labelFont.lineWeight ? labelFont.lineWeight : 0))
      .text(d => d.label);

    // Estilo del texto para el valor del circulo
    node
      .append('text')
      .attr('class', 'value-text')
      .style('font-size', `${valueFont.size}px`)
      .attr('clip-path', d => `url(#clip-${d.id})`)
      .style('font-weight', () => (valueFont.weight ? valueFont.weight : 600))
      .style('font-family', valueFont.family)
      .style('fill', () => (valueFont.color ? valueFont.color : '#000'))
      .style('stroke', () => (valueFont.lineColor ? valueFont.lineColor : '#000'))
      .style('stroke-width', () => (valueFont.lineWeight ? valueFont.lineWeight : 0))
      .text(d => d.data.value);

    // Centra el valor en el circulo
    d3.selectAll('.value-text')
      .attr('x', function(d) {
        const self = d3.select(this);
        const width = self.node().getBBox().width;
        return -(width / 2);
      })
      .style('opacity', function(d) {
        const self = d3.select(this);
        const width = self.node().getBBox().width;
        d.hideLabel = width * 1.05 > d.r * 2;
        return d.hideLabel ? 0 : 1;
      })
      .attr('y', () => labelFont.size / 2);

    // Centra el texto en el circulo
    d3.selectAll('.label-text')
      .attr('x', function() {
        const self = d3.select(this);
        const width = self.node().getBBox().width;
        return -(width / 2);
      })
      .attr('y', d => {
        if (d.hideLabel) {
          return valueFont.size / 3;
        }
        return -valueFont.size * 0.5;
      });

    node.append('title').text(d => d.label);
  }
}

BubbleChart.propTypes = {
  data: PropTypes.array,
  graph: PropTypes.shape({
    zoom: PropTypes.number,
    offsetX: PropTypes.number,
    offsetY: PropTypes.number
  }),
  height: PropTypes.number,
  labelFont: PropTypes.shape({
    family: PropTypes.string,
    size: PropTypes.number,
    color: PropTypes.string,
    weight: PropTypes.string,
    lineWeight: PropTypes.string,
    lineColor: PropTypes.string
  }),
  overflow: PropTypes.bool,
  padding: PropTypes.number,
  valueFont: PropTypes.shape({
    family: PropTypes.string,
    size: PropTypes.number,
    color: PropTypes.string,
    weight: PropTypes.string,
    lineWeight: PropTypes.string,
    lineColor: PropTypes.string
  }),
  width: PropTypes.number
};

BubbleChart.defaultProps = {
  overflow: false,
  graph: {
    zoom: 1,
    offsetX: 0,
    offsetY: -0.01
  },
  width: 700,
  height: 700,
  padding: 0,
  valueFont: VALUE_FONT,
  labelFont: LABEL_FONT
};
