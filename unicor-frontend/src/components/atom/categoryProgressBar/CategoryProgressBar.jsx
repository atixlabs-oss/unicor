import React from 'react';
import PropTypes from 'prop-types';
import { Popover, Progress } from 'antd';

import './category-progress-bar.scss';
import { VALUATION_STYLE } from '../../../utils/constants';
import CategoryTooltip from '../categoryTooltip/category-tooltip';

const CategoryProgressBar = ({ categoryName, state, percentBar, index, average }) => (
  <div className="CategoryProgressBar">
    <div className="LeftSide">
      <h2>
        {index}. {categoryName}
      </h2>
    </div>
    <div className="RightSide">
      <Popover content={<CategoryTooltip status={state} average={average} />}>
        <Progress
          percent={percentBar <= 1 ? 1 : percentBar}
          status="active"
          strokeColor={VALUATION_STYLE[state].strokeColor}
          showInfo={false}
          strokeLinecap="square"
          strokeWidth={12}
        />
      </Popover>
    </div>
  </div>
);

export default CategoryProgressBar;

CategoryProgressBar.propTypes = {
  average: PropTypes.number.isRequired,
  categoryName: PropTypes.string.isRequired,
  index: PropTypes.string.isRequired,
  percentBar: PropTypes.number.isRequired,
  state: PropTypes.object.isRequired
};
