import React from 'react';
import PropTypes from 'prop-types';
import CryIcon from '../icons/CryIcon.jsx';
import SadIcon from '../icons/SadIcon.jsx';
import NeutralIcon from '../icons/NeutralIcon.jsx';
import SmileIcon from '../icons/SmileIcon.jsx';
import HappyIcon from '../icons/HappyIcon.jsx';

import './review-score-box.scss';

import {classifyScore} from '../../../utils/func-helper';

const ReviewScoreBox = ({ score }) => {

    const getBoxOpacity = (number) => {
        return (classifyScore(score) !== number) ? "opacity" : ""
    }
    return (
        <div className="score_box">
            <div className="score_box__review_container">
                <div className={"score_box__review cry-bgcolor " + getBoxOpacity(1)}><CryIcon /></div>
                <div className={"score_box__review sad-bgcolor " + getBoxOpacity(2)}><SadIcon /></div>
                <div className={"score_box__review neutral-bgcolor " + getBoxOpacity(3)}><NeutralIcon /></div>
                <div className={"score_box__review smile-bgcolor " + getBoxOpacity(4)}><SmileIcon /></div>
                <div className={"score_box__review happy-bgcolor " + getBoxOpacity(5)}><HappyIcon/></div>
            </div>
            <div className="score_box__numbers_container">
                <span>-1</span>
                <span>0</span>
                <span>1</span>
            </div>
        </div>
    )
};

export default ReviewScoreBox;

ReviewScoreBox.propTypes = {
  score: PropTypes.number.isRequired
};
