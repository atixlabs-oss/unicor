import React from 'react';
import PropTypes from 'prop-types';

const InputLabel = ({ text }) => (
  <label className="input-label">
    {text} <span>*</span>
  </label>
);

export default InputLabel;

InputLabel.propTypes = {
  text: PropTypes.string.isRequired
};
