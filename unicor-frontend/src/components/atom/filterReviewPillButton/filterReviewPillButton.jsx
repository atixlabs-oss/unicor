import React from 'react';
import PropTypes from 'prop-types';

import './filter-review-pill-button.scss';

const FilterReviewPillButton = ({ sourceIcon, theme, handle, id }) => (
  <button className={`filter-review-button ${theme}`} onClick={() => handle(id)}>
    <div className="filter-review-icon">{sourceIcon}</div>
  </button>
);

export default FilterReviewPillButton;

FilterReviewPillButton.propTypes = {
  handle: PropTypes.func.isRequired,
  id: PropTypes.number,
  sourceIcon: PropTypes.object,
  theme: PropTypes.string
};
