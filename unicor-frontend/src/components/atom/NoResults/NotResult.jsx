import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const NoResults = ({ title, subtitle }) => (
  <div className="no-results-container">
    <div className="no-results-image-box">
      <img src="../../../no-results-img.jpg" alt="No hay resultados para esta búsqueda"></img>
    </div>
    <h3>{title}</h3>
    <p>{subtitle}</p>
  </div>
);

NoResults.propTypes = {
  subtitle: PropTypes.string,
  title: PropTypes.string
};

NoResults.defaultProps = {
  title: 'No se encontraron resultados para esta búsqueda',
  subtitle: 'Cambie los filtros para obtener información'
};

export default NoResults;
