import React from 'react';
import PropTypes from 'prop-types';

import './reviews-cards.scss';

const ReviewsCard = ({ reviewIcon, reviewAmount }) => (
  <div className="ReviwesCard">
    <img src={reviewIcon} className="ReviewIcon" alt="Review Smile" />
    <h2>{reviewAmount}</h2>
    <p>Reviews</p>
  </div>
);

export default ReviewsCard;

ReviewsCard.propTypes = {
  reviewAmount: PropTypes.number.isRequired,
  reviewIcon: PropTypes.string.isRequired
};
