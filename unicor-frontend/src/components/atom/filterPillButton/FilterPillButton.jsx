import React from 'react';
import PropTypes from 'prop-types';

import './filter-pill-button.scss';

const FilterPillButton = ({ text, sourceIcon, theme, handle, id }) => (
  <button className={`FilterPillButton ${theme}`} onClick={() => handle(id)}>
    <p className="MediaIcon">{sourceIcon}</p>
    <p>{text}</p>
  </button>
);

export default FilterPillButton;

FilterPillButton.propTypes = {
  handle: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  sourceIcon: PropTypes.object,
  text: PropTypes.string.isRequired,
  theme: PropTypes.string
};
