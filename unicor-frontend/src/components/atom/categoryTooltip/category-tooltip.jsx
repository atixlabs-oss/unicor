import React from 'react';
import PropTypes from 'prop-types';
import { VALUATION_STYLE } from '../../../utils/constants';

const CategoryTooltip = ({ status, average }) => (
  <div className="CategoryTooltip">
    <img src={VALUATION_STYLE[status].icon} className="ReviewIcon" alt="Review Smile" />
    <p>{average}</p>
  </div>
);

export default CategoryTooltip;

CategoryTooltip.propTypes = {
  average: PropTypes.number.isRequired,
  status: PropTypes.string.isRequired
};
