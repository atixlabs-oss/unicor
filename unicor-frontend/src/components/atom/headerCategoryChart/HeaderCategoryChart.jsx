import React from 'react';
import PropTypes from 'prop-types';

import './header-category-chart.scss';

const HeaderCategoryChart = ({ range: { min, max } }) => (
  <div className="HeaderCategoryChart">
    <div className="LeftSide">
      <p>Sentimientos</p>
    </div>
    <div className="RightSide">
      <p>{min}</p>
      <p>0</p>
      <p>{max}</p>
    </div>
  </div>
);

export default HeaderCategoryChart;

HeaderCategoryChart.propTypes = {
  range: PropTypes.object.isRequired
};
