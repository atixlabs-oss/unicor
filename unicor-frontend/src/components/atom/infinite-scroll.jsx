import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

const InfiniteScroll = ({ children, hasMore, next, loading, refresh }) => {
  const [page, setPage] = useState(0);
  const loader = useRef(null);

  // here we handle what happens when user scrolls to Load More div
  // in this case we just update page variable
  const handleObserver = entities => {
    const target = entities[0];
    if (target.isIntersecting && hasMore && !loading) {
      setPage(page_ => page_ + 1);
    }
  };

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: '20px',
      threshold: 1.0
    };

    const observer = new IntersectionObserver(handleObserver, options);
    if (loader.current) {
      observer.observe(loader.current);
    }
  }, [hasMore]);

  useEffect(() => {
    if (page !== 0) next(page);
  }, [page]);

  useEffect(() => {
    if (refresh) {
      next(0);
      setPage(0);
    }
  }, [refresh]);

  return (
    <>
      {children}
      {hasMore && <div ref={loader} style={{ display: 'flex', justifyContent: 'center' }} />}
    </>
  );
};

export default InfiniteScroll;

InfiniteScroll.propTypes = {
  children: PropTypes.node.isRequired,
  hasMore: PropTypes.bool.isRequired,
  loading: PropTypes.bool,
  next: PropTypes.func.isRequired,
  refresh: PropTypes.bool.isRequired
};
