import React from 'react';
import PropTypes from 'prop-types';

import './titlePage.scss';

const TitlePage = ({ text }) => (
  <div className="TitlePage">
    <h1>{text}</h1>
  </div>
);

export default TitlePage;

TitlePage.propTypes = {
  text: PropTypes.string.isRequired
};
