import React, { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { UserContext } from '../../services/providers/user-context';
import { LOGIN_URL } from '../../utils/constants';
import PropTypes from 'prop-types';

const AdminRoute = ({ component, requireAuthentication }) => {
  const { user } = useContext(UserContext);

  if (!requireAuthentication) return component;

  return user.role === 'Administrator' && user.accessToken ? (
    component
  ) : (
    <Redirect to={LOGIN_URL} />
  );
};

export default AdminRoute;

AdminRoute.propTypes = {
  component: PropTypes.func.isRequired,
  requireAuthentication: PropTypes.bool.isRequired
};
