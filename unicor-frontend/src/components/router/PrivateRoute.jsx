import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import AdminRoute from './AdminRoute';
import MainLayout from '../layout/MainLayout';

const PrivateRoute = ({ component: Component, requireAuthentication, isHeader, ...rest }) => {
  if (!isHeader) return <Route {...rest} render={properties => <Component {...properties} />} />;

  return (
    <MainLayout>
      <Route
        {...rest}
        render={properties => (
          <AdminRoute
            requireAuthentication={requireAuthentication}
            component={<Component {...properties} />}
          />
        )}
      />
    </MainLayout>
  );
};

export default PrivateRoute;

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  exact: PropTypes.bool,
  isHeader: PropTypes.bool,
  path: PropTypes.string,
  requireAuthentication: PropTypes.bool.isRequired
};

PrivateRoute.defaultProps = {
  exact: false,
  isHeader: true
};
