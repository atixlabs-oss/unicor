import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import {
  CREATE_USER_URL,
  EDIT_USER_URL,
  HOME_URL,
  LOGIN_URL,
  USER_LIST_URL
} from '../../utils/constants';
import Home from '../pages/home/Home';
import LoginContainer from '../pages/login/LoginContainer';
import CreateUser from '../pages/userList/createUser/create-user';
import EditUser from '../pages/userList/editUser/edit-user';
import UserList from '../pages/userList/user-list';
import PrivateRoute from './PrivateRoute';

const routesConfig = [
  {
    path: LOGIN_URL,
    component: LoginContainer,
    requireAuthentication: false,
    exact: false,
    isHeader: false
  },
  { path: HOME_URL, component: Home, requireAuthentication: false, exact: true },
  {
    path: USER_LIST_URL,
    component: UserList,
    requireAuthentication: true,
    exact: true
  },
  {
    path: CREATE_USER_URL,
    component: CreateUser,
    requireAuthentication: true,
    exact: true
  },
  {
    path: EDIT_USER_URL,
    component: EditUser,
    requireAuthentication: true,
    exact: true
  }
];

export default function BasicExample() {
  const routes = routesConfig.map((route, i) => <PrivateRoute key={i} {...route} />);
  return (
    <Router>
      <Switch>{routes}</Switch>
    </Router>
  );
}
