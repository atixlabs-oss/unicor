import React, { useState, useEffect } from 'react';
import { Button, message, Table } from 'antd';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { DEFAULT_PAGE_SIZE } from '../../../utils/constants';
import Spinner from '../../atom/Spinner';

import './paginated-table.scss';

const PaginatedTable = ({
  fetchDataPageable,
  size,
  columns,
  FormSearch,
  tableClassname,
  buttonName,
  createUrl
}) => {
  const [pageable, setPageable] = useState({ content: [] });
  const [filter, setFilter] = useState({ page: 0, size });
  const [loading, setLoading] = useState(false);
  const { push } = useHistory();

  const getData = async filter_ => {
    try {
      const pageable_ = await fetchDataPageable(filter_);
      setPageable(pageable_);
    } catch (error) {
      message.error(error.response.data.error);
    }
    setLoading(false);
  };

  useEffect(() => {
    setLoading(true);
    getData(filter);
  }, []);

  useEffect(() => {
    getData(filter);
  }, [filter]);

  const handleReset = () => {
    setFilter({ page: 0, size });
  };

  const handleSearch = values => {
    setFilter({ ...filter, ...values, page: 0 });
  };

  const changePage = page => {
    const correctPage = page - 1;
    setFilter(filter_ => ({ ...filter_, page: correctPage }));
  };

  return (
    <div className="PaginatedTableContainer">
      <div className="FormOptions">
        <FormSearch onReset={handleReset} onSearch={handleSearch} />
        {buttonName && (
          <div className="TableButton">
            <Button type="primary" onClick={() => push(createUrl)}>
              {buttonName}
            </Button>
          </div>
        )}
      </div>
      <div className="TableContainer">
        {(loading && <Spinner />) || (
          <Table
            className={tableClassname}
            columns={columns()}
            dataSource={pageable.content}
            size="middle"
            pagination={{
              pageSize: size,
              onChange: changePage,
              current: filter.page + 1,
              total: pageable.totalElements
            }}
          />
        )}
      </div>
    </div>
  );
};

PaginatedTable.propTypes = {
  FormSearch: PropTypes.node.isRequired,
  buttonName: PropTypes.string,
  columns: PropTypes.func.isRequired,
  createUrl: PropTypes.string,
  fetchDataPageable: PropTypes.func.isRequired,
  size: PropTypes.number,
  tableClassname: PropTypes.string
};

PaginatedTable.defaultProps = {
  size: DEFAULT_PAGE_SIZE
};
export default PaginatedTable;
