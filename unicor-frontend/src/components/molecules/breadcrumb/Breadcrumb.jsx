import React from 'react';
import { Breadcrumb } from 'antd';
import { HomeOutlined } from '@ant-design/icons';
import PropTypes from 'prop-types';

import './breadcrumb.scss';
import { Link } from 'react-router-dom';

const { Item } = Breadcrumb;

const BreadcrumbMenu = ({ homeLink, backLink }) => (
  <div className="BreadcrumbMenu">
    <Breadcrumb>
      <Link to={homeLink}>
        <Item>
          <HomeOutlined />
        </Item>
      </Link>
      {backLink && (
        <Link to={backLink}>
          <Item>Volver</Item>
        </Link>
      )}
    </Breadcrumb>
  </div>
);

export default BreadcrumbMenu;

BreadcrumbMenu.propTypes = {
  backLink: PropTypes.string.isRequired,
  homeLink: PropTypes.string.isRequired
};
