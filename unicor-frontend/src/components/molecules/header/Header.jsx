import React, { useContext } from 'react';
import { Avatar, message, Menu, Dropdown, Button } from 'antd';
import { DownOutlined } from '@ant-design/icons';

import './header.scss';
import { UserContext } from '../../../services/providers/user-context';
import { Link, useHistory } from 'react-router-dom';
import { HOME_URL, LOGIN_URL, USER_LIST_URL } from '../../../utils/constants';
import { clearAll } from '../../../utils/cookie';
import { APIContext } from '../../../services/providers/providers';

import LoginButton from '../../../components/atom/loginButton/LoginButton.jsx';

const Header = () => {
  const { push } = useHistory();

  const {
    user: { email, role }
  } = useContext(UserContext);

  const { logout } = useContext(APIContext);

  const handleLogOut = async () => {
    try {
      await logout();
    } catch (error) {
      message.error(error.response.data.error);
    }
    clearAll();
    push(LOGIN_URL);
  };

  const menu = (
    <Menu>
      {role === 'Administrator' && (
        <Menu.Item key="0" onClick={() => push(USER_LIST_URL)}>
          <span>Administrar Usuarios</span>
        </Menu.Item>
      )}
      <Menu.Item key="1" onClick={handleLogOut}>
        <span>Cerrar Sesión</span>
      </Menu.Item>
    </Menu>
  );

  const actions = (role && (
    <Dropdown overlay={menu} trigger={['click']} placement="bottomRight" arrow>
      <div className="UserData">
        <div className="UserName">
          <h2>{email}</h2>
          <p>{role}</p>
        </div>
        <div className="AvatarContainer">
          <Avatar style={{ color: '#FFFFFF', backgroundColor: '#3995F8' }}>US</Avatar>
          <DownOutlined />
        </div>
      </div>
    </Dropdown>
  )) || (
    <LoginButton onClickHandler={() => push(LOGIN_URL)} />
  );

  return (
    <div className="HeaderContainer">
      <div className="LogoContainer">
        <Link to={HOME_URL}>
          <img src="/logo_white.svg" className="Logo" alt="Logo Municipalidad de Cordoba" />
        </Link>
      </div>
      {actions}
    </div>
  );
};

export default Header;
