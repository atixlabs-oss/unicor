import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { VALUATION_STYLE } from '../../../utils/constants';
import ReviewsCard from '../../atom/reviewCard/ReviewsCard';

const ORDER_OF_REVIEWS = ['GOOD', 'NEUTRAL', 'BAD'];

const ReviewContainer = ({ reviews }) => {
  const [numberOfReviews, setNumberOfReviews] = useState({});

  useEffect(() => {
    reviews.forEach(({ state, numberMessages }) =>
      setNumberOfReviews(previous => ({ ...previous, [state]: numberMessages }))
    );
  }, [reviews]);

  return (
    <div className="ReviewsContainer">
      {ORDER_OF_REVIEWS.map(name => (
        <ReviewsCard
          key={name}
          reviewIcon={VALUATION_STYLE[name].icon}
          reviewAmount={numberOfReviews[name]}
        />
      ))}
    </div>
  );
};

export default ReviewContainer;

ReviewContainer.propTypes = {
  reviews: PropTypes.array.isRequired
};
