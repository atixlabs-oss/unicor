import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

import './summary-table.scss';
import { columnsSummary } from '../../../utils/tables/table-summary';
import { useEffectFilterApi } from '../../../hooks/useApi';
import { withApi } from '../../../services/providers/providers';

const getDefaultCategories = categories =>
  categories.reduce((accumulator, { name, state }) => ({ ...accumulator, [name]: state }), {});

const convert = ({ categories, id, ...rest }) => ({
  ...rest,
  key: id,
  ...getDefaultCategories(categories)
});

const SummaryTable = ({ filter, api: { getSuperSquareStatus } }) => {
  const { data: summaries } = useEffectFilterApi(getSuperSquareStatus, filter, []);

  return (
    <div className="SummaryTable">
      <Table
        columns={columnsSummary(summaries[0])}
        dataSource={summaries.map(convert)}
        size="small"
        align="center"
        pagination={false}
      />
    </div>
  );
};

export default withApi(SummaryTable);

SummaryTable.propTypes = {
  api: PropTypes.shape({
    getSuperSquareStatus: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired
};
