import React from 'react';
import PropTypes from 'prop-types';
import { VALUATION_STYLE } from '../../../utils/constants';
import BubbleChart from '../../atom/bubbleChart/bubble-chart';
import { useEffectFilterApi } from '../../../hooks/useApi';
import { withApi } from '../../../services/providers/providers';

const convert = ({ word, numberMessages, state }) => ({
  label: word,
  value: numberMessages,
  color: VALUATION_STYLE[state].strokeColor
});

const BubbleChartContainer = ({ filter, api: { getTopWords } }) => {
  const { data: words } = useEffectFilterApi(getTopWords, filter, []);

  return (
    <div className="ContainerCenter">
      <BubbleChart data={words.map(convert)} width={465} height={450} />
    </div>
  );
};

export default withApi(BubbleChartContainer);

BubbleChartContainer.propTypes = {
  api: PropTypes.shape({
    getTopWords: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired
};
