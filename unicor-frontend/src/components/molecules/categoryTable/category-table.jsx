import React from 'react';
import PropTypes from 'prop-types';
import CategoryProgressBar from '../../atom/categoryProgressBar/CategoryProgressBar';
import HeaderCategoryChart from '../../atom/headerCategoryChart/HeaderCategoryChart';

const CategoryTable = ({ className, categories, range }) =>
  categories.length !== 0 && (
    <div className={className}>
      <HeaderCategoryChart range={range} />
      {categories.map(({ name, percentage, ...rest }, index) => (
        <CategoryProgressBar key={index} categoryName={name} percentBar={percentage} {...rest} />
      ))}
    </div>
  );

export default CategoryTable;

CategoryTable.propTypes = {
  categories: PropTypes.array.isRequired,
  className: PropTypes.string.isRequired,
  range: PropTypes.object.isRequired
};
