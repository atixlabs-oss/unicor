import React from 'react';
import PropTypes from 'prop-types';
import { BarChart, Bar, XAxis, YAxis } from 'recharts';
import { VALUATION_STYLE } from '../../../utils/constants';

import './_style.scss';

const ORDER_OF_BAR = ['GOOD', 'NEUTRAL', 'BAD'];

const refactorData = states => [
  states.reduce(
    (accumulator, { state, numberMessages }) => ({ ...accumulator, [state]: numberMessages }),
    {}
  )
];

const BarChartReviewsContainer = ({ reviews }) => {
  const totalNumberMessages = reviews.reduce(
    (accumulator, { numberMessages }) => accumulator + numberMessages,
    0
  );

  return (
    <div className="ContainerCenter">
      <div className="ReviewsBarChart">
        <p className="ReviewsBarChartTitle">
          {totalNumberMessages} <span>Reviews</span>
        </p>
        <BarChart data={refactorData(reviews)} width={550} height={50} layout="vertical">
          <XAxis type="number" hide />
          <YAxis type="category" hide />

          {ORDER_OF_BAR.map(name => (
            <Bar
              dataKey={name}
              stackId="state"
              fill={VALUATION_STYLE[name].strokeColor}
              key={name}
            />
          ))}
        </BarChart>
      </div>
    </div>
  );
};

export default BarChartReviewsContainer;

BarChartReviewsContainer.propTypes = {
  reviews: PropTypes.array.isRequired
};
