import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { Avatar } from 'antd';
import { UserOutlined } from '@ant-design/icons';

import './source-item.scss';
import { UserContext } from '../../../services/providers/user-context';
import SeeMoreLink from '../../atom/seeMoreLink/SeeMoreLink.jsx';

import ArrowRightIcon from '../../atom/icons/ArrowRightIcon.jsx';

const getWidth = bool => (bool ? 'w-100' : 'w-46');

const SourceItem = ({
  sourceIcon,
  sourceName,
  sourceReview,
  reviewIcon,
  sourceAvatar,
  postDate,
  url
}) => {
  const {
    user: { role }
  } = useContext(UserContext);

  const [showFullReview, setShowFullReview] = useState(false);

  const handleShowAll = () => {
    setShowFullReview(!showFullReview);
  };

  return (
    <div className={`SourceItem ${getWidth(role)}`}>
      <div className={`SourceUser  ${getWidth(!role)}`}>
        <Avatar size={50} icon={<UserOutlined />} src={sourceAvatar} />
        <div className="UserData">
          <div className="UserName">
            <h2>{sourceName}</h2>
            <img src={sourceIcon} className="MediaIcon" alt="Social Media Icon" />
          </div>
          {sourceReview.length > 140 ? (
            <p className="sourceIntem-review">
              {showFullReview ? sourceReview : sourceReview.slice(0,140)}
              <SeeMoreLink
                onClickHandler={handleShowAll}
                text={showFullReview ? ' ver menos' : ' ... ver más'}
              />
            </p>
          ) : (
            <p className="sourceIntem-review">{sourceReview}</p>
          )}
          <div className="sourceIntem-line" />
          <p className="sourceIntem-postDate">{postDate}</p>
        </div>
        <img src={reviewIcon} className="ReviewIcon" alt="Review Smile" />
      </div>
      {role && (
        <button
          className="reply-button"
          onClick={() => window.open(url, '_blank', 'noopener,noreferrer')}
        >
          <span>Ir a la fuente</span>
          <ArrowRightIcon style="arrow-icon" />
        </button>
      )}
    </div>
  );
};

export default SourceItem;

SourceItem.propTypes = {
  postDate: PropTypes.string.isRequired,
  reviewIcon: PropTypes.string.isRequired,
  sourceAvatar: PropTypes.string.isRequired,
  sourceIcon: PropTypes.string.isRequired,
  sourceName: PropTypes.string.isRequired,
  sourceReview: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
};
