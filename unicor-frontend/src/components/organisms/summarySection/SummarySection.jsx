import React from 'react';
import PropTypes from 'prop-types';
import ReviewContainer from '../../molecules/reviewsContainer/review-container';
import SummaryTable from '../../molecules/summaryTable/SummaryTable';
import BubbleChartContainer from '../../molecules/bubbleChartContainer/bubble-chart-container';
import BarChartReviewsContainer from '../../molecules/barChartReviewsContainer/bar-chart-reviews-container';
import NotResultLayout from '../../layout/not-result-layout';
import { useEffectFilterApi } from '../../../hooks/useApi';
import { withApi } from '../../../services/providers/providers';

import './summary-section.scss';

const SummarySection = ({
  filter,
  api: { getSuperSquareNumberReviews },
  showFilter: { categoryId, superSquareId }
}) => {
  const { data: reviews, firstLoading } = useEffectFilterApi(
    getSuperSquareNumberReviews,
    filter,
    []
  );

  return (
    !firstLoading && (
      <div className="SummaryContainer">
        <NotResultLayout isResult={reviews.some(({ numberMessages }) => numberMessages !== 0)}>
          <div className="LeftSide">
            <div className="SummaryTitle">
              <h1>
                <strong>{categoryId ?? 'General'} /</strong> {superSquareId ?? 'Ciudad'}
              </h1>
            </div>
            <ReviewContainer reviews={reviews} />
            <SummaryTable filter={filter} />
          </div>
          <div className="RightSide">
            <BarChartReviewsContainer reviews={reviews} />
            <BubbleChartContainer filter={filter} />
          </div>
        </NotResultLayout>
      </div>
    )
  );
};

export default withApi(SummarySection);

SummarySection.propTypes = {
  api: PropTypes.shape({
    getSuperSquareNumberReviews: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired,
  showFilter: PropTypes.object.isRequired
};
