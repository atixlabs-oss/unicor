import React, { useContext, useEffect, useState } from 'react';
import { Input, Form, Button, Tooltip, message, Select } from 'antd';
import { useHistory } from 'react-router-dom';
import {
  CONFIRM_PASSWORD_RULE,
  PASSWORD_RULE,
  REQUIRED_FIELD_RULE
} from '../../../utils/validations/validation-rules';
import { APIContext } from '../../../services/providers/providers';
import { useGetApi } from '../../../hooks/useApi';
import { USER_LIST_URL } from '../../../utils/constants';
import {
  CONFIRM_PASSWORD_REQUIRED,
  PASSWORD_CHARACTERS
} from '../../../utils/validations/validation-messages';
import PropTypes from 'prop-types';

import './user-form.scss';
import InputLabel from '../../atom/input-label';

const { Option } = Select;

const UserForm = ({ defaultUser, buttonName, successMessage, sendData }) => {
  const [form] = Form.useForm();
  const { getAllRoles } = useContext(APIContext);
  const { loading: rolesLoading, getData: getRoles, data: roles } = useGetApi(getAllRoles, []);
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);
  const { push } = useHistory();

  const getRolesData = async () => await getRoles();

  useEffect(() => {
    if (defaultUser) {
      const { email, role } = defaultUser;
      form.setFieldsValue({ email, role: role?.code });
    }
    getRolesData();
  }, []);

  const handleSubmit = async values => {
    const { password, confirmPassword } = values;
    setLoading(true);

    try {
      if (password && !confirmPassword) {
        setError(CONFIRM_PASSWORD_REQUIRED);
        return;
      }
      await sendData({ ...defaultUser, ...values });
      message.success(successMessage);
      push(USER_LIST_URL);
    } catch (error_) {
      message.error(error_.response.data);
    }
    setLoading(false);
  };

  return (
    <Form form={form} onFinish={handleSubmit} className="CreateUserForm">
      {/* Email Input */}
      <div className="_input">
        <InputLabel text="Email" />
        <Form.Item rules={[REQUIRED_FIELD_RULE]} name="email">
          <Input className="input" placeholder="Email" type="email" />
        </Form.Item>
      </div>

      {/* Role Input */}
      <div className="_input">
        <InputLabel text="Tipo de Usuario" />
        <Form.Item rules={[REQUIRED_FIELD_RULE]} name="role">
          <Select
            loading={rolesLoading}
            disabled={rolesLoading}
            placeholder="Seleccione tipo de usuario"
            style={{ width: '100%' }}
          >
            {roles.map(({ code, description }, index) => (
              <Option key={index} value={code}>
                {description}
              </Option>
            ))}
          </Select>
        </Form.Item>
      </div>

      {/* Password Input */}
      <div className="_input">
        <InputLabel text="Contraseña" />
        <Tooltip title={PASSWORD_CHARACTERS}>
          <Form.Item
            rules={[REQUIRED_FIELD_RULE, PASSWORD_RULE, CONFIRM_PASSWORD_RULE]}
            name="password"
          >
            <Input.Password className="input" placeholder="******" />
          </Form.Item>
        </Tooltip>
      </div>

      {/* ConfirmPassword Input */}
      <div className="_input">
        <InputLabel text=" Confimar Contraseña" />
        <Form.Item
          rules={[REQUIRED_FIELD_RULE, PASSWORD_RULE, CONFIRM_PASSWORD_RULE]}
          name="confirmPassword"
        >
          <Input.Password className="input" placeholder="******" />
        </Form.Item>
      </div>

      <div className="ButtonContainer">
        <Button htmlType="submit" type="primary" loading={loading}>
          {buttonName}
        </Button>
      </div>
      {error && <p> {error} </p>}
    </Form>
  );
};

export default UserForm;

UserForm.propTypes = {
  buttonName: PropTypes.string.isRequired,
  defaultUser: PropTypes.object,
  sendData: PropTypes.func.isRequired,
  successMessage: PropTypes.string.isRequired
};

UserForm.defaultProps = {
  defaultUser: {}
};
