import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import CategoryTable from '../../molecules/categoryTable/category-table';

import './category-section.scss';
import { withApi } from '../../../services/providers/providers';
import NotResultLayout from '../../layout/not-result-layout';
import { useGetApi } from '../../../hooks/useApi';

const TABLE_SIZE = 5;
const LIMIT = 10;

const BUTTON_TOP_TEN = 'Ver el top 10 de las categorias';
const BUTTON_ALL_CATEGORIES = 'Ver todas las categorias';

const TOP_TITLE = 'Top 10';
const ALL_CATEGORIES_TITLE = 'Categorias';

const CategorySection = ({ filter, api: { getTopCategories, getRange } }) => {
  const [categories, setCategories] = useState([]);
  const [isShowTopTen, setIsShowTopTen] = useState(true);
  const [tableSize, setTableSize] = useState(TABLE_SIZE);
  const [firstLoading, setFirstLoading] = useState(true);
  const { data: range, getData } = useGetApi(getRange);

  const getTopCategoriesData = async filter_ => {
    const categories_ = await getTopCategories(filter_);
    setCategories(categories_.map((c, i) => ({ ...c, index: i + 1 })));

    // Se calcula la distribución de las tablas
    const newTableSize = Math.round(categories_.length / 2);
    setTableSize(newTableSize > TABLE_SIZE ? newTableSize : TABLE_SIZE);

    setFirstLoading(false);
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    let filter_ = filter;
    if (isShowTopTen) filter_ = { ...filter, limit: LIMIT };
    getTopCategoriesData(filter_);
  }, [isShowTopTen, filter]);

  const leftSide = () => categories.slice(0, tableSize);
  const rightSide = () => categories.slice(tableSize);

  return (
    !firstLoading && (
      <div className="CategorySection">
        <NotResultLayout isResult={categories.length !== 0}>
          <div className="CategorySectionTitle">
            <h1>{isShowTopTen ? TOP_TITLE : ALL_CATEGORIES_TITLE}</h1>
            <Button type="link" onClick={() => setIsShowTopTen(previous => !previous)}>
              {isShowTopTen ? BUTTON_ALL_CATEGORIES : BUTTON_TOP_TEN}
            </Button>
          </div>
          <CategoryTable className="LeftSide" categories={leftSide()} range={range} />
          <CategoryTable className="RightSide" categories={rightSide()} range={range} />
        </NotResultLayout>
      </div>
    )
  );
};

export default withApi(CategorySection);

CategorySection.propTypes = {
  api: PropTypes.shape({
    getTopCategories: PropTypes.func.isRequired,
    getRange: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired
};
