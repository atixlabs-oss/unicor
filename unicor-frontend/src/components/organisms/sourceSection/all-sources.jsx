import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { withApi } from '../../../services/providers/providers';
import InfiniteScroll from '../../atom/infinite-scroll';
import SourceList from './source-list';
import Spinner from '../../atom/Spinner';
import { DEFAULT_PAGE_SIZE_SOURCES } from '../../../utils/constants';

const DEFAULT_PAGE_SIZE = 30;

const AllSources = ({
  filter,
  setIsMoreElement,
  socialMediaIds,
  sentimentalIds,
  api: { getMessages }
}) => {
  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [refresh, setRefresh] = useState(true);

  const [hasMore, setHasMore] = useState(false);

  const getMessagesData = async page => {
    setLoading(true);
    const { content, totalPages, totalElements } = await getMessages(
      { ...filter, page, size: DEFAULT_PAGE_SIZE },
      socialMediaIds,
      sentimentalIds
    );
    setMessages(messages_ => [...messages_, ...content]);
    setLoading(false);
    setHasMore(page < totalPages);
    setIsMoreElement(DEFAULT_PAGE_SIZE_SOURCES < totalElements);
    setRefresh(false);
  };

  useEffect(() => {
    setRefresh(true);
    setMessages([]);
  }, [filter, socialMediaIds, sentimentalIds]);

  const handleNextPage = async page => {
    await getMessagesData(page);
  };

  return (
    <>
      <InfiniteScroll hasMore={hasMore} next={handleNextPage} loading={loading} refresh={refresh}>
        {!refresh && <SourceList messages={messages} />}
      </InfiniteScroll>
      {loading && <Spinner />}
    </>
  );
};

export default withApi(AllSources);

AllSources.propTypes = {
  api: PropTypes.shape({
    getMessages: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired,
  sentimentalIds: PropTypes.array.isRequired,
  setIsMoreElement: PropTypes.func.isRequired,
  socialMediaIds: PropTypes.array.isRequired
};
