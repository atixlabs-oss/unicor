import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useGetApi } from '../../../hooks/useApi';
import { withApi } from '../../../services/providers/providers';
import SourceList from './source-list';
import Spinner from '../../atom/Spinner';
import { DEFAULT_PAGE_SIZE_SOURCES } from '../../../utils/constants';

const SourceTop = ({
  filter,
  setIsMoreElement,
  socialMediaIds,
  sentimentalIds,
  api: { getMessages }
}) => {
  const {
    data: { content: messages, totalElements },
    getData: getMessagesData,
    loading
  } = useGetApi(getMessages, {
    content: []
  });

  useEffect(() => {
    getMessagesData(
      { ...filter, page: 0, size: DEFAULT_PAGE_SIZE_SOURCES },
      socialMediaIds,
      sentimentalIds
    );
  }, [filter, socialMediaIds, sentimentalIds]);

  useEffect(() => {
    setIsMoreElement(DEFAULT_PAGE_SIZE_SOURCES < totalElements);
  }, [totalElements]);

  return (loading && <Spinner />) || <SourceList messages={messages} />;
};

export default withApi(SourceTop);

SourceTop.propTypes = {
  api: PropTypes.shape({
    getMessages: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired,
  sentimentalIds: PropTypes.array.isRequired,
  setIsMoreElement: PropTypes.func.isRequired,
  socialMediaIds: PropTypes.array.isRequired
};
