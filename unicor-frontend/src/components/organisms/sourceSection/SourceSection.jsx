import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import FilterPillButton from '../../atom/filterPillButton/FilterPillButton';
import {
  GoogleOutlined,
  TwitterOutlined,
  FacebookFilled,
  InstagramOutlined
} from '@ant-design/icons';

import './source-section.scss';
import { useGetApi } from '../../../hooks/useApi';
import { withApi } from '../../../services/providers/providers';
import TopSources from './top-sources';
import AllSources from './all-sources';

import SadIcon from '../../atom/icons/SadIcon.jsx';
import SmileIcon from '../../atom/icons/SmileIcon.jsx';
import NeutralIcon from '../../atom/icons/NeutralIcon.jsx';
import FilterReviewPillButton from '../../atom/filterReviewPillButton/filterReviewPillButton.jsx';

const ICONS = {
  TWITTER: <TwitterOutlined />,
  GOOGLE_REVIEW: <GoogleOutlined />,
  FACEBOOK: <FacebookFilled />,
  INSTAGRAM: <InstagramOutlined />
};

const TOP_TEN_BUTTON = 'Vel el top 10';
const ALL_COMMENT_BUTTON = 'Ver todas las fuentes';

const SENTIMENTAL_ICONS = {
  GOOD: {
    theme: 'smile-theme',
    icon: <SmileIcon style="filter-icon" />
  },
  NEUTRAL: {
    theme: 'neutral-theme',
    icon: <NeutralIcon style="filter-icon" />
  },
  BAD: {
    theme: 'sad-theme',
    icon: <SadIcon style="filter-icon" />
  }
};

const SourceSection = ({ filter, api: { getSocialMedia, getSentimentals } }) => {
  const [socialMediaIds, setSocialMediaIds] = useState([]);
  const [sentimentalIds, setSentimentalIds] = useState([]);

  const { data: socialMedia, getData: getSocialMediaData } = useGetApi(getSocialMedia, []);
  const { data: sentimentals, getData: getSentimentalsData } = useGetApi(getSentimentals, []);

  const [showTopTen, setShowTopTen] = useState(true);
  const [isMoreElements, setIsMoreElement] = useState(false);

  useEffect(() => {
    getSocialMediaData();
    getSentimentalsData();
  }, []);

  // Logica para el manejo de multi select filters de las pills de sentimentals y sourceMedia
  const handleFilter = (list, setList) => id => {
    if (!id) {
      setList([]);
      return;
    }

    if (list.includes(id)) {
      setList(previous => previous.filter(id_ => id !== id_));
      return;
    }

    setList(previous => [...previous, id]);
  };

  const showSources = Component => (
    <Component {...{ filter, socialMediaIds, setIsMoreElement, sentimentalIds }} />
  );

  const isDisable = (id, list, defaultClass = '') =>
    (!id && list.length === 0) || list.includes(id) ? defaultClass : 'disable';

  return (
    <div className="SourceSection">
      <div className="SourceSectionHeader">
        <div className="FiltersContainer">
          {/* Se crea los botones para filtrar por red social */}
          {[{ name: 'Todas' }, ...socialMedia].map(({ id, name, code }) => (
            <FilterPillButton
              key={name}
              text={name}
              theme={isDisable(id, socialMediaIds)}
              sourceIcon={ICONS[code]}
              handle={handleFilter(socialMediaIds, setSocialMediaIds)}
              id={id}
            />
          ))}
          {/* Se crea los botones para filtrar por estado los mensajes */}
          <div className="review-filter-container">
            {sentimentals.map(({ id, state }) => (
              <FilterReviewPillButton
                key={id}
                theme={isDisable(id, sentimentalIds, SENTIMENTAL_ICONS[state].theme)}
                sourceIcon={SENTIMENTAL_ICONS[state].icon}
                id={id}
                handle={handleFilter(sentimentalIds, setSentimentalIds)}
              />
            ))}
          </div>
        </div>
        {isMoreElements && (
          <Button type="link" onClick={() => setShowTopTen(previous => !previous)}>
            {showTopTen ? ALL_COMMENT_BUTTON : TOP_TEN_BUTTON}
          </Button>
        )}
      </div>
      {showSources(showTopTen ? TopSources : AllSources)}
    </div>
  );
};

export default withApi(SourceSection);

SourceSection.propTypes = {
  api: PropTypes.shape({
    getSocialMedia: PropTypes.func.isRequired,
    getMessages: PropTypes.func.isRequired,
    getSentimentals: PropTypes.func.isRequired
  }).isRequired,
  filter: PropTypes.object.isRequired
};
