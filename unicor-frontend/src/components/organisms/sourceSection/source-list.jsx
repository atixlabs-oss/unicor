import React from 'react';
import PropTypes from 'prop-types';
import { SOURCE_ICONS, VALUATION_STYLE } from '../../../utils/constants';
import NotResultLayout from '../../layout/not-result-layout';
import SourceItem from '../../molecules/sourceItem/SourceItem';

const SourceList = ({ messages }) => (
  <NotResultLayout isResult={messages.length !== 0}>
    <div className="SourcesContainer">
      {messages.map(({ source, username, content, state, id, avatar, postDate, url }) => (
        <SourceItem
          key={id}
          sourceIcon={SOURCE_ICONS[source.code]}
          sourceName={username}
          sourceReview={content}
          sourceAvatar={avatar}
          reviewIcon={VALUATION_STYLE[state].icon}
          postDate={postDate.slice(0, -9)}
          url={url}
        />
      ))}
    </div>
  </NotResultLayout>
);

export default SourceList;

SourceList.propTypes = {
  messages: PropTypes.array.isRequired
};
