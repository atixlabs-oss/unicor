import React from 'react';
import Header from '../molecules/header/Header';
import PropTypes from 'prop-types';

import './main-layout.scss';

const MainLayout = ({ children }) => (
  <div className="HomeContainer">
    <div className="HeaderTop">
      <Header />
    </div>
    <div className="MainContent">{children}</div>
  </div>
);

export default MainLayout;

MainLayout.propTypes = {
  children: PropTypes.node.isRequired
};
