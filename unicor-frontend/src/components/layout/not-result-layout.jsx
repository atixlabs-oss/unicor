import React from 'react';
import NoResults from '../atom/NoResults/NotResult.jsx';

const NotResultLayout = ({ children, isResult }) => {
  if (!isResult) return <NoResults />;

  return children;
};

export default NotResultLayout;
