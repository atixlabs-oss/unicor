import React from 'react';
import { Form, Input, Checkbox, Button } from 'antd';
import PropTypes from 'prop-types';
import { EMAIL_REQUIRED, PASSWORD_REQUIRED } from '../../../utils/validations/validation-messages';

const Login = ({ onFinish, loading }) => (
  <Form onFinish={onFinish} className="LoginForm" layout="vertical" size="large">
    <Form.Item name="email" rules={[{ required: true, message: EMAIL_REQUIRED }]}>
      <Input placeholder="Email" type="email" />
    </Form.Item>
    <Form.Item name="password" rules={[{ required: true, message: PASSWORD_REQUIRED }]}>
      <Input.Password placeholder="Contraseña" />
    </Form.Item>
    <Form.Item name="remember" valuePropName="checked">
      <Checkbox>Recordarme</Checkbox>
    </Form.Item>
    <Form.Item>
      <Button type="primary" htmlType="submit" loading={loading}>
        Ingresar
      </Button>
    </Form.Item>
  </Form>
);

export default Login;

Login.propTypes = {
  loading: PropTypes.bool.isRequired,
  onFinish: PropTypes.func.isRequired
};
