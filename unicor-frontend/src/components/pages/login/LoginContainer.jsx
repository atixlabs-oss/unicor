import PropTypes from 'prop-types';
import React, { useContext, useEffect, useState } from 'react';
import Login from './Login';
import { withApi } from '../../../services/providers/providers';

import './login.scss';
import { getToken } from '../../../services/jwtManager';
import { useHistory } from 'react-router-dom';
import { UserContext } from '../../../services/providers/user-context';
import { HOME_URL } from '../../../utils/constants';
import GoBackLink from '../../atom/goBackLink/GoBackLink';

const LoginContainer = ({ api }) => {
  const { setUser } = useContext(UserContext);
  const [response, setResponse] = useState({});
  const { push } = useHistory();

  const { error, loading } = response;

  useEffect(() => {
    if (getToken()) {
      push(HOME_URL);
    }
  }, []);

  const onFinish = async values => {
    try {
      setResponse({ loading: true });
      const logInResponse = await api.login(values);
      setUser(logInResponse);
      push(HOME_URL);
    } catch (error_) {
      setResponse({ error: error_ });
    }
    setResponse(response_ => ({ ...response_, loading: false }));
  };

  const goBackHandler = () => {
    push('/');
  }

  return (
    <div className="LoginContainer">
      <div className="LoginAside">
        <div className="goBackContainer">
            <GoBackLink onClickHandler={goBackHandler} text="Volver" />
          </div>
        <div className="LogoContainer">
          <img src="/logo_white.svg" className="Logo" alt="Logo Municipalidad de Cordoba" />
        </div>
      </div>
      <div className="FormContainer">
        <div className="WelcomeMessage">
          <img src="/iso_colors.svg" className="Isologo" alt="Isologo Municipalidad de Cordoba" />
          <h1>Bienvenido</h1>
          <p>Ingresá a tu cuenta de Unicor BackOffice</p>
        </div>
        <Login onFinish={onFinish} loading={loading} />
        {error && <p>{error}</p>}
        <img src="/color_line.svg" className="ColorLine" alt="Color Line" />
      </div>
    </div>
  );
};

LoginContainer.propTypes = {
  api: PropTypes.shape({
    login: PropTypes.func.isRequired
  }).isRequired
};

export default withApi(LoginContainer);
