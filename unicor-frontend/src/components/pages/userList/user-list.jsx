import React, { useContext } from 'react';
import { Input } from 'antd';
import { APIContext } from '../../../services/providers/providers';
import { CREATE_USER_URL, EDIT_USER_URL, HOME_URL } from '../../../utils/constants';
import { columnsUser } from '../../../utils/tables/table-users';
import TitlePage from '../../atom/titlePage/TitlePage';
import PaginatedTable from '../../molecules/paginatedTable/paginated-table';

import './user-list.scss';
import BreadcrumbMenu from '../../molecules/breadcrumb/Breadcrumb';

const { Search } = Input;

const BUTTON_NAME = 'Crear Nuevo Usuario';
const TITLE = 'Administrador de Usuarios';

const UserList = () => {
  const { getUsersFiltered, patchUser } = useContext(APIContext);

  return (
    <div className="HomeContainer">
      <div className="MainContent">
        <div className="UserContent">
          <BreadcrumbMenu homeLink={HOME_URL} />
          <TitlePage text={TITLE} />
          <PaginatedTable
            tableClassname="table-users"
            fetchDataPageable={getUsersFiltered}
            columns={() => columnsUser(patchUser, EDIT_USER_URL)}
            buttonName={BUTTON_NAME}
            createUrl={CREATE_USER_URL}
            // Formulario para filtrar en la tabla, recibe onSearch y onReset
            FormSearch={({ onSearch }) => (
              <Search
                name="search"
                placeholder="Buscar"
                onSearch={value => onSearch({ search: value })}
                enterButton
              />
            )}
          />
        </div>
      </div>
    </div>
  );
};

export default UserList;
