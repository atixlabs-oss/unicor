import React, { useContext, useEffect } from 'react';
import { useGetApi } from '../../../../hooks/useApi';
import { APIContext } from '../../../../services/providers/providers';
import { getUrlParameter } from '../../../../utils/func-helper';
import NoResults from '../../../atom/NoResults/NotResult.jsx';
import Spinner from '../../../atom/Spinner';
import TitlePage from '../../../atom/titlePage/TitlePage';
import UserForm from '../../../organisms/UserForm/user-form';
import BreadcrumbMenu from '../../../molecules/breadcrumb/Breadcrumb';

import '../user-list.scss';
import { HOME_URL, USER_LIST_URL } from '../../../../utils/constants';

const TITLE = 'Editar Usuario';
const SUCCESS_MESSAGE = 'El usuario fue editado exitosamente.';
const BUTTON_NAME = 'Editar';

const EditUser = () => {
  const { getUserById, editUser } = useContext(APIContext);
  const id = getUrlParameter('id');
  const { loading, data, getData } = useGetApi(getUserById, {}, true);

  const getUserByIdData = async () => await getData(id);

  useEffect(() => {
    getUserByIdData();
  }, []);

  return (
    <div className="HomeContainer">
      <div className="MainContent">
        <div className="UserContent">
          <BreadcrumbMenu homeLink={HOME_URL} backLink={USER_LIST_URL} />
          <TitlePage text={TITLE} />
          {(loading && <Spinner />) ||
            (data && (
              <UserForm
                buttonName={BUTTON_NAME}
                successMessage={SUCCESS_MESSAGE}
                defaultUser={data}
                sendData={editUser}
              />
            )) || <NoResults />}
        </div>
      </div>
    </div>
  );
};

export default EditUser;
