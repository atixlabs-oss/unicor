import React, { useContext } from 'react';
import { APIContext } from '../../../../services/providers/providers';
import TitlePage from '../../../atom/titlePage/TitlePage';
import UserForm from '../../../organisms/UserForm/user-form';
import BreadcrumbMenu from '../../../molecules/breadcrumb/Breadcrumb';

import '../user-list.scss';
import { HOME_URL, USER_LIST_URL } from '../../../../utils/constants';

const TITLE = 'Crear Usuario';
const SUCCESS_MESSAGE = 'El usuario fue creado exitosamente.';
const BUTTON_NAME = 'Crear';

const CreateUser = () => {
  const { postUser } = useContext(APIContext);
  return (
    <div className="HomeContainer">
      <div className="MainContent">
        <div className="UserContent">
          <BreadcrumbMenu homeLink={HOME_URL} backLink={USER_LIST_URL} />
          <TitlePage text={TITLE} />
          <UserForm buttonName={BUTTON_NAME} successMessage={SUCCESS_MESSAGE} sendData={postUser} />
        </div>
      </div>
    </div>
  );
};

export default CreateUser;
