import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { withApi } from '../../../services/providers/providers';
import './home.scss';
import SummarySection from '../../organisms/summarySection/SummarySection';
import GoogleMapContainer from '../../map/GoogleMapContainer';
import { useGetApi } from '../../../hooks/useApi';
import SelectSearchAtom from '../../atom/select-search-atom';
import SourceSection from '../../organisms/sourceSection/SourceSection';
import CategorySection from '../../organisms/categorySection/CategorySection';

const { TabPane } = Tabs;

const Home = ({ api: { getSuperSquareAllNames, getCategoriesAll, getSuperSquareAll } }) => {
  const [filter, setFilter] = useState({});
  const [showFilter, setShowFilter] = useState({});
  const { data: places, getData: getPlacesData, loading } = useGetApi(getSuperSquareAll, []);

  const getAllData = async filter_ => {
    await getPlacesData(filter_);
  };

  useEffect(() => {
    getAllData(filter);
  }, []);

  const handleSearch = key => valueObj => {
    if (filter[key] !== valueObj?.id) {
      const newFilter = { ...filter, [key]: valueObj?.id };
      setShowFilter(previous => ({ ...previous, [key]: valueObj?.name }));
      setFilter(newFilter);
      getAllData(newFilter);
    }
  };

  return (
    <div className="HomeContainer">
      <div className="MainContent">
        <div className="MapContent">
          {/* Filtros del mapa */}
          <div className="FiltersContainer">
            <SelectSearchAtom
              fetchData={getCategoriesAll}
              placeholder="General"
              onChange={handleSearch('categoryId')}
            />
            <SelectSearchAtom
              fetchData={getSuperSquareAllNames}
              placeholder="Ciudad"
              defaultValue={filter.superSquareId}
              onChange={handleSearch('superSquareId')}
            />
          </div>
          <div className="google-map-container-skeleton">
            {!loading && (
              <GoogleMapContainer places={places} onSearch={handleSearch('superSquareId')} />
            )}
          </div>
        </div>
        <div className="TabContent">
          <Tabs defaultActiveKey="1" onChange={() => {}} className="ValueTabs">
            <TabPane tab="Resumen" key="1">
              <SummarySection filter={filter} showFilter={showFilter} />
            </TabPane>
            <TabPane tab="Categorias" key="2">
              <CategorySection filter={filter} />
            </TabPane>
            <TabPane tab="Fuentes" key="3">
              <SourceSection filter={filter} />
            </TabPane>
          </Tabs>
        </div>
      </div>
    </div>
  );
};

export default withApi(Home);

Home.propTypes = {
  api: PropTypes.shape({
    getSuperSquareAllNames: PropTypes.func.isRequired,
    getSuperSquareAll: PropTypes.func.isRequired,
    getCategoriesAll: PropTypes.func.isRequired
  }).isRequired
};
