import React from 'react';
import { compose, withProps } from 'recompose';
import { withScriptjs, withGoogleMap } from 'react-google-maps';
import Map from './Map';

import './googleMaps.scss';
import Spinner from '../atom/Spinner';
import configs from '../../services/configs';

const GOOGLE_MAP_URL = 'https://maps.googleapis.com/maps/api/js';
const parameters = { libraries: 'geometry,drawing,places', v: '3.44', key: configs.GOOGLE_API_KEY };

const query = Object.keys(parameters)
  .map(v => `${v}=${parameters[v]}`)
  .join('&');

const GoogleMapContainer = compose(
  withProps({
    googleMapURL: `${GOOGLE_MAP_URL}?${query}`,
    loadingElement: (
      <div className="google-map-spiner-container">
        <Spinner />
      </div>
    ),
    containerElement: <div className="google-map-container" />,
    mapElement: <div className="google-map-element" />
  }),
  withScriptjs,
  withGoogleMap
)(properties => <Map {...properties} />);

export default GoogleMapContainer;
