import React, { useState } from 'react';
import { GoogleMap, Polygon } from 'react-google-maps';
import PolygonInfo from './PolygonInfo';
import PropTypes from 'prop-types';
import { VALUATION_STYLE } from '../../utils/constants';
import { calculateCenter } from '../../utils/func-helper';

const DEFAULT_ZOOM = 14;

const calculateZoom = size => (size === 1 ? 16 : DEFAULT_ZOOM);

// Estilo para deshabilitar marcadores en el mapa.
const DESABLED_MARKERS = [
  {
    featureType: 'poi.business',
    stylers: [{ visibility: 'off' }]
  },
  {
    featureType: 'transit',
    elementType: 'labels.icon',
    stylers: [{ visibility: 'off' }]
  }
];

const GOOGLE_MAP_OPTIONS = {
  zoomControl: false,
  fullscreenControl: false,
  disableDoubleClickZoom: true,
  streetView: false,
  scrollwheel: false,
  streetViewControl: false,
  draggable: false,
  gestureHandling: 'none',
  disableDefaultUI: true,
  draggableCursor: 'default',
  clickableIcons: false,
  styles: DESABLED_MARKERS
};

const Map = ({ places, onSearch }) => {
  const [placesOpen, setPlacesOpen] = useState({});

  const handleOpenPolygonInfo = (id, value) => {
    setPlacesOpen({ [id]: value });
  };

  return (
    <GoogleMap
      options={GOOGLE_MAP_OPTIONS}
      mapTypeId="roadmap"
      defaultZoom={calculateZoom(places.length)}
      defaultCenter={calculateCenter(places.flatMap(({ polygon }) => polygon))}
    >
      {places.map(({ id, polygon, status, name }) => (
        <div key={id}>
          <PolygonInfo
            lat={polygon[0].latitude}
            lng={polygon[0].longitude}
            title={name}
            isOpen={placesOpen[id]}
            status={status}
          />
          <Polygon
            onMouseOver={() => handleOpenPolygonInfo(id, true)}
            onMouseOut={() => handleOpenPolygonInfo(id, false)}
            onClick={() => onSearch({ id, name })}
            path={polygon.map(({ latitude: lat, longitude: lng }) => ({ lat, lng }))}
            options={{
              ...VALUATION_STYLE[status.state],
              strokeOpacity: 0.8,
              strokeWeight: 3,
              fillOpacity: 0.35
            }}
          />
        </div>
      ))}
    </GoogleMap>
  );
};

export default Map;

Map.propTypes = {
  onSearch: PropTypes.func.isRequired,
  places: PropTypes.array.isRequired
};
