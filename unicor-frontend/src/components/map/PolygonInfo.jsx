import React from 'react';
import PropTypes from 'prop-types';
import InfoBox from 'react-google-maps/lib/components/addons/InfoBox';
import ReviewScoreBox from '../atom/ReviewScoreBox/ReviewScoreBox.jsx';

import {classifyScore} from '../../utils/func-helper';

const PolygonInfo = ({ lat, lng, title, isOpen, status: { score } }) => {

  const scoreColorClassnames = ["cry-color", "sad-color", "neutral-color", "smile-color", "happy-color"];
  
  return (
    <InfoBox visible={isOpen || false} position={new window.google.maps.LatLng(lat, lng)}>
      <div className="polygon-info">
        <h2 className="polygon-score">
          <p className="primary-title">{title}</p>
        </h2>
        <ReviewScoreBox score={score} />
        <hr />
        <p className="secondary-title">Valoración: <strong className={scoreColorClassnames[classifyScore(score) - 1]}>{score}</strong></p>
      </div>
    </InfoBox>
  )
};

export default PolygonInfo;

PolygonInfo.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  lat: PropTypes.number.isRequired,
  lng: PropTypes.number.isRequired,
  status: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired
};
