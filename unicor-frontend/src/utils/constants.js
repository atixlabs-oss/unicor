// ROUTES
export const HOME_URL = '/';
export const LOGIN_URL = '/login';
export const USER_LIST_URL = '/user-list';
export const CREATE_USER_URL = '/user-list/create';
export const EDIT_USER_URL = '/user-list/edit';

// TABLE
export const DEFAULT_PAGE_SIZE = 11;

// Estilo de colores dependiendo de la valuación
export const VALUATION_STYLE = {
  GOOD: { strokeColor: '#47BB76', fillColor: '#D1EDD6', icon: '/smile.svg' },
  BAD: { strokeColor: '#EB5B5B', fillColor: '#EDC9C6', icon: '/sad.svg' },
  NEUTRAL: { strokeColor: '#F2CB56', fillColor: '#F6E9C3', icon: '/neutral.svg' },
  NO_STATE: { strokeColor: '#767676', fillColor: 'rgba(118, 118,118,0.2)', icon: '' }
};

export const SOURCE_ICONS = {
  TWITTER: '/icon-twitter.svg',
  GOOGLE_REVIEW: '/icon-google.svg',
  FACEBOOK: '/icon-facebook.svg',
  INSTAGRAM: '/icon-instagram.svg'
};

export const DEFAULT_PAGE_SIZE_SOURCES = 10;
