import React from 'react';
import { VALUATION_STYLE } from '../constants';

const iconTable = state =>
  (state !== 'NO_STATE' && (
    <img src={VALUATION_STYLE[state].icon} className="TableSmile" alt="Review Smile" />
  )) || <div>-</div>;

export const columnsSummary = item => {
  const categories = item?.categories ?? [];
  return [
    {
      title: 'Lugares',
      dataIndex: 'name',
      key: 'name',
      render: (name, { numberMessages }) => `${name} (${numberMessages})`
    },
    {
      title: 'General',
      dataIndex: 'state',
      key: 'general',
      render: iconTable,
      align: 'center'
    },
    ...categories.map(({ name }) => ({
      title: name,
      dataIndex: name,
      key: name,
      render: iconTable,
      align: 'center'
    }))
  ];
};
