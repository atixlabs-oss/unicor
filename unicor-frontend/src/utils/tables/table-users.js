import React from 'react';
import { Link } from 'react-router-dom';
import { Switch } from 'antd';
import { EditOutlined } from '@ant-design/icons';

export const columnsUser = (patchUser, editLink) => [
  {
    title: 'Email',
    width: 270,
    dataIndex: 'email',
    key: '1'
  },
  {
    title: 'Rol',
    dataIndex: 'role',
    width: 270,
    key: '2',
    render: ({ description }) => description
  },
  {
    title: 'Acciones',
    dataIndex: 'active',
    width: 100,
    key: '3',
    render: (active, { id }) => (
      <div className="TableUserActions">
        <div className="TableUserActionsLink">
          <Link to={`${editLink}?id=${id}`}>
            {/* Habia una imagen antes */}
            <EditOutlined />
          </Link>
        </div>
        <Switch defaultChecked={active} key={id} onChange={value => patchUser(value, id)} />
      </div>
    )
  }
];
