const getUrlVars = () => {
  const vars = {};
  window.location.href.replace(/[&?]+([^&=]+)=([^&]*)/gi, (m, key, value) => {
    vars[key] = value;
  });
  return vars;
};

export const getUrlParameter = (parameter, defaultvalue) => {
  let urlparameter = defaultvalue;
  if (window.location.href.includes(parameter)) {
    urlparameter = getUrlVars()[parameter];
  }
  return urlparameter;
};

const calculateMinOrMax = (x1, x2, cal) => (x1 ? Math[cal](x1, x2) : x2);

const center = (x1, x2) => x1 + (x2 - x1) / 2;

const calculateMinMax = (items, key) => {
  const x1 = `${key}1`;
  const x2 = `${key}2`;

  return items.reduce(
    (accumulator, value) => ({
      [x1]: calculateMinOrMax(accumulator[x1], value[key], 'min'),
      [x2]: calculateMinOrMax(accumulator[x2], value[key], 'max')
    }),
    {}
  );
};

export const calculateCenter = coords => {
  const { latitude1, latitude2 } = calculateMinMax(coords, 'latitude');
  const { longitude1, longitude2 } = calculateMinMax(coords, 'longitude');

  const lat = center(latitude1, latitude2);
  const lng = center(longitude1, longitude2);

  return { lat, lng };
};

export const createArrayParameter = (url, arraysToQuery) => {
  const keys = Object.keys(arraysToQuery);
  const query = keys
    .reduce(
      (accumulator, key) => accumulator.concat(arraysToQuery[key].map(id => `${key}=${id}`)),
      []
    )
    .join('&');

  return query === '&' ? url : `${url}?${query}`;
};

const ratings = [
  { value: -0.6, order: 1 },
  { value: -0.2, order: 2 },
  { value: 0.2, order: 3 },
  { value: 0.6, order: 4 },
  { value: 1, order: 5 },
];

/**
 * Returns a natural number between 1 and 5 that rates the provided score.
 * If the score is not valid it returns undefined.
 * @param {Number} score Number between -1 and 1
 */
 export const classifyScore = (score) => ratings.find(({ value }) => score <= value)?.order;