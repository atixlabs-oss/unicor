import { INPUT_REQUIRED, PASSWORD, PASSWORD_NOT_MATCH } from './validation-messages';
import { REGEX_PASSWORD } from './validation-regex';

export const PASSWORD_RULE = { pattern: REGEX_PASSWORD, message: PASSWORD, type: 'string' };
export const REQUIRED_FIELD_RULE = { required: true, message: INPUT_REQUIRED };

export const CONFIRM_PASSWORD_RULE = ({ getFieldValue }) => ({
  validator(rule, value) {
    if (!value || getFieldValue('password') === value) {
      return Promise.resolve();
    }
    return Promise.reject(PASSWORD_NOT_MATCH);
  }
});
