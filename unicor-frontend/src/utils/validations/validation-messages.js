export const LOGIN_INVALID = 'El email o la contraseña son incorrectos.';
export const EMAIL_REQUIRED = 'Por favor, escriba su email.';
export const INPUT_REQUIRED = 'Campo obligatorio.';

export const PASSWORD_REQUIRED = 'Por favor, escriba su contraseña.';
export const PASSWORD = 'Contraseña invalida';
export const PASSWORD_NOT_MATCH = '¡Las dos contraseñas que ingresó no coinciden!';
export const CONFIRM_PASSWORD_REQUIRED = 'Se requiere confirmar la contraseña.';
export const PASSWORD_CHARACTERS =
  'La password debe contener un minimo de 6 caracteres, al menos un numero y al menos una letra.';
