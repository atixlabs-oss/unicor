export const getItem = name => {
  const value = localStorage.getItem(name) || sessionStorage.getItem(name);
  if (value) return JSON.parse(value);
  return value;
};

export const setItem = (name, value) => {
  const isRemember = getItem('isRemember');
  if (isRemember) {
    localStorage.setItem(name, JSON.stringify(value));
  } else {
    sessionStorage.setItem(name, JSON.stringify(value));
  }
};

export const clearAll = () => {
  sessionStorage.clear();
  localStorage.clear();
};

export const removeItem = name => {
  sessionStorage.removeItem(name);
  localStorage.removeItem(name);
};
