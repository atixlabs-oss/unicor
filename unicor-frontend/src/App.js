import React from 'react';
import Router from './components/router/Router';
import { APIContext } from './services/providers/providers';
import { api } from './services/api/all';
import configs from './services/configs';
import jwtManager from './services/jwtManager';
import 'antd/dist/antd.css';
import './App.scss';
import { UserProvider } from './services/providers/user-context';

const App = () => (
  <div className="App">
    <APIContext.Provider value={api(configs, jwtManager)}>
      <UserProvider>
        <Router />
      </UserProvider>
    </APIContext.Provider>
  </div>
);

export default App;
