import axios from 'axios';
import { LOGIN_URL } from '../../utils/constants';
import { clearAll } from '../../utils/cookie';
const AUTH_SUFFIX = 'Bearer ';

/**
 * getAxios instance
 * @param {Object} configs - api configurations.
 * @param {Object} jwtManager - jwt manager has a function that returns the JWT token.
 */
const getInstance = (
  {
    config: {
      endpoints: { backend: baseURL }
    }
  },
  { getToken }
) => {
  const instance = axios.create({
    baseURL,
    headers: {
      Accept: 'application/json, text/plain, */*', // TODO check */*
      'Content-Type': 'application/json'
    }
  });

  const setAuthorizationHeader = config => {
    // Check baseUrl to prevent sending credentials to third parties
    // Check Authorization header to allow header overriding from the call site
    if (config.baseURL === baseURL && !config.headers.Authorization) {
      const token = getToken();

      if (token) {
        config.headers.Authorization = AUTH_SUFFIX + token;
      }
    }

    return config;
  };

  instance.interceptors.request.use(
    config => setAuthorizationHeader(config),
    error => {
      throw error;
    }
  );

  instance.interceptors.response.use(
    response => response,
    error => {
      if (error.response && error.response.status === 401) {
        clearAll();
        window.location = LOGIN_URL;
      }
      throw error;
    }
  );

  return instance;
};

export default getInstance;
