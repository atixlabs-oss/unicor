import categoryApiCalls from './alls/category.api-calls';
import superSquareApiCalls from './alls/superSquare.api-calls';
import authApiCalls from './alls/auth.api-calls';
import roleApiCalls from './alls/role.api-calls';
import userApiCalls from './alls/user.api-calls';
import getInstance from './configuration';
import authApiCallsMock from './mocks/auth.api-calls.mock';
import roleApiCallsMock from './mocks/role.api-calls.mock';
import userApiCallsMock from './mocks/user.api-calls.mock';
import superSquareApiCallsMock from './mocks/superSquare.api-calls.mock';
import categoryApiCallsMock from './mocks/category.api-calls.mock';
import socialMediaApiCallsMock from './mocks/socialMedia.api-calls.mock';
import socialMediaApiCalls from './alls/socialMedia.api-calls';
import messagesApiCallsMock from './mocks/messages.api-calls.mock';
import messagesApiCalls from './alls/messages.api-calls';
import sentimentApiCallsMock from './mocks/sentiment.api-calls.mock';
import sentimentApiCalls from './alls/sentiment.api-calls';

export const api = (configs, jwtManager) => {
  const instance = getInstance(configs, jwtManager);

  if (process.env.REACT_APP_CUSTOM_ENV === 'mocked') {
    return {
      ...authApiCallsMock(),
      ...superSquareApiCallsMock(),
      ...userApiCallsMock(),
      ...roleApiCallsMock(),
      ...categoryApiCallsMock(),
      ...socialMediaApiCallsMock(),
      ...messagesApiCallsMock(),
      ...sentimentApiCallsMock()
    };
  }
  return {
    ...superSquareApiCalls(instance),
    ...categoryApiCalls(instance),
    ...authApiCalls(instance),
    ...userApiCalls(instance),
    ...roleApiCalls(instance),
    ...socialMediaApiCalls(instance),
    ...messagesApiCalls(instance),
    ...sentimentApiCalls(instance)
  };
};
