import helpers from '../helpers';

const BASE_URL = '/roles';

const getAllRoles = makeGetRequest => () => makeGetRequest(BASE_URL);

export default axiosInstance => {
  const { makeGetRequest } = helpers(axiosInstance);
  return {
    getAllRoles: getAllRoles(makeGetRequest)
  };
};
