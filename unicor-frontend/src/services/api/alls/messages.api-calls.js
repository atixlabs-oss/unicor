import { createArrayParameter } from '../../../utils/func-helper';
import helpers from '../helpers';

const BASE_URL = '/messages';

const getMessages = makeGetRequest => (filter, socialMediaSourceIds, sentimentalIds) =>
  makeGetRequest(createArrayParameter(BASE_URL, { socialMediaSourceIds, sentimentalIds }), filter);

export default axiosInstance => {
  const { makeGetRequest } = helpers(axiosInstance);
  return {
    getMessages: getMessages(makeGetRequest)
  };
};
