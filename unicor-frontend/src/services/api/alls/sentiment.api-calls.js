import helpers from '../helpers';

const BASE_URL = '/sentimental-state-configuration';

const getRange = makeGetRequest => () => makeGetRequest(`${BASE_URL}/scale`);
const getSentimentals = makeGetRequest => () => makeGetRequest(BASE_URL);

export default axiosInstance => {
  const { makeGetRequest } = helpers(axiosInstance);
  return {
    getRange: getRange(makeGetRequest),
    getSentimentals: getSentimentals(makeGetRequest)
  };
};
