import helpers from '../helpers';

const BASE_URL = '/social-media';

const getSocialMedia = makeGetRequest => () => makeGetRequest(BASE_URL);

export default axiosInstance => {
  const { makeGetRequest } = helpers(axiosInstance);
  return {
    getSocialMedia: getSocialMedia(makeGetRequest)
  };
};
