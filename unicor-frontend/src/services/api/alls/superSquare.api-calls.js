import helpers from '../helpers';

const BASE_URL = '/super-squares';

const getSuperSquareAllNames = makeGetRequest => () => makeGetRequest(`${BASE_URL}/list`);
const getSuperSquareAll = makeGetRequest => filter => makeGetRequest(BASE_URL, filter);

const getSuperSquareStatus = makeGetRequest => filter =>
  makeGetRequest(`${BASE_URL}/status`, filter);

const getSuperSquareNumberReviews = makeGetRequest => filter =>
  makeGetRequest(`${BASE_URL}/number-messages`, filter);

const getTopWords = makeGetRequest => filter => makeGetRequest(`${BASE_URL}/top-words`, filter);

export default axiosInstance => {
  const { makeGetRequest } = helpers(axiosInstance);
  return {
    getSuperSquareAllNames: getSuperSquareAllNames(makeGetRequest),
    getSuperSquareAll: getSuperSquareAll(makeGetRequest),
    getSuperSquareStatus: getSuperSquareStatus(makeGetRequest),
    getSuperSquareNumberReviews: getSuperSquareNumberReviews(makeGetRequest),
    getTopWords: getTopWords(makeGetRequest)
  };
};
