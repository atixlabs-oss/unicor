import helpers from '../helpers';

const BASE_URL = '/users';

const getUsersFiltered = makeGetRequest => filter => makeGetRequest(`${BASE_URL}/filters`, filter);

const patchUser = makeGetRequest => (value, id) =>
  makeGetRequest(`users/${value ? 'enable' : 'disable'}/${id}`);

const postUser = makePostRequest => body => makePostRequest(BASE_URL, body);

const getUserById = makeGetRequest => id => makeGetRequest(`${BASE_URL}/${id}`);

const editUser = makePutRequest => ({ id, ...data }) => makePutRequest(`${BASE_URL}/${id}`, data);

export default axiosInstance => {
  const { makeGetRequest, makePostRequest, makePutRequest } = helpers(axiosInstance);
  return {
    getUsersFiltered: getUsersFiltered(makeGetRequest),
    patchUser: patchUser(makeGetRequest),
    postUser: postUser(makePostRequest),
    getUserById: getUserById(makeGetRequest),
    editUser: editUser(makePutRequest)
  };
};
