import { setItem } from '../../../utils/cookie';
import { LOGIN_INVALID } from '../../../utils/validations/validation-messages';
import { setToken } from '../../jwtManager';
import helpers from '../helpers';

const BASE_URL = '/auth';

const login = makePostRequest => async ({ remember, ...rest }) => {
  try {
    const { data } = await makePostRequest(`${BASE_URL}/login`, rest);

    setToken(data.accessToken, remember);
    setItem('user', data);

    return data;
  } catch (_) {
    throw LOGIN_INVALID;
  }
};

const logout = makeGetRequest => () => makeGetRequest(`${BASE_URL}/logout`);

export default axiosInstance => {
  const { makeGetRequest, makePostRequest } = helpers(axiosInstance);
  return {
    login: login(makePostRequest),
    logout: logout(makeGetRequest)
  };
};
