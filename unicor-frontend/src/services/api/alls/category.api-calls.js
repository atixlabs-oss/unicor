import helpers from '../helpers';

const BASE_URL = '/categories';

const getCategoriesAll = makeGetRequest => () => makeGetRequest(BASE_URL);
const getTopCategories = makeGetRequest => filter => makeGetRequest(`${BASE_URL}/top`, filter);

export default axiosInstance => {
  const { makeGetRequest } = helpers(axiosInstance);
  return {
    getCategoriesAll: getCategoriesAll(makeGetRequest),
    getTopCategories: getTopCategories(makeGetRequest)
  };
};
