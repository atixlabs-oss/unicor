const makeGetRequest = httpClient => (url, parameters) =>
  httpClient
    .get(url, {
      params: parameters
    })
    .then(response => response.data || response);

const makePostRequest = httpClient => (url, bodyParameters = {}) =>
  httpClient.post(url, { ...bodyParameters });

const makePatchRequest = httpClient => (url, bodyParameters = {}) =>
  httpClient.patch(url, { ...bodyParameters });

const makePutRequest = httpClient => (url, bodyParameters = {}) =>
  httpClient.put(url, { ...bodyParameters });

export default client => ({
  makeGetRequest: makeGetRequest(client),
  makePostRequest: makePostRequest(client),
  makePatchRequest: makePatchRequest(client),
  makePutRequest: makePutRequest(client)
});
