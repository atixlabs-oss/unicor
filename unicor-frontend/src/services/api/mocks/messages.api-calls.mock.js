const getMessages = () => ({
  content: [
    {
      id: 1,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 1, code: 'TWITTER', name: 'Twitter' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'GOOD',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 2,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 2, code: 'GOOGLE_REVIEW', name: 'Google' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'NEUTRAL',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 3,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 3, code: 'FACEBOOK', name: 'Facebook' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'BAD',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 4,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 4, code: 'INSTAGRAM', name: 'Instagram' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'GOOD',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 5,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 4, code: 'INSTAGRAM', name: 'Instagram' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'NEUTRAL',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 6,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 1, code: 'TWITTER', name: 'Twitter' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'GOOD',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 7,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 2, code: 'GOOGLE_REVIEW', name: 'Google' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'NEUTRAL',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 8,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 3, code: 'FACEBOOK', name: 'Facebook' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'BAD',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 9,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 4, code: 'INSTAGRAM', name: 'Instagram' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'GOOD',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    },
    {
      id: 10,
      postDate: '16-03-2021 23:06:54',
      avatar:
        'https://lh4.googleusercontent.com/-m9AfADoarXs/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckzSviA3qOmXwNRGEkuNenjU-cfRw/w60-h60-p-rp-mo-br100/photo.jpg',
      username: 'Pablo Perez',
      source: { id: 4, code: 'INSTAGRAM', name: 'Instagram' },
      content:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet neque sapien. Etiam in mi at nulla ultrices mollis sit amet non purus. Integer eu sem non nunc sodales tristique. Maecenas tempor dapibus leo in viverra. Vivamus non magna ac sem fringilla hendrerit et et nisl. Integer efficitur quis magna et dapibus.',
      state: 'NEUTRAL',
      url: 'https://goo.gl/maps/rt7sn95BvNmvmbMB6'
    }
  ]
});

export default () => ({
  getMessages
});
