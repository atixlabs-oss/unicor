const getSocialMedia = () => [
  { id: 1, code: 'TWITTER', name: 'Twitter' },
  { id: 2, code: 'GOOGLE_REVIEW', name: 'Google' },
  { id: 3, code: 'FACEBOOK', name: 'Facebook' },
  { id: 4, code: 'INSTAGRAM', name: 'Instagram' }
];

export default () => ({
  getSocialMedia
});
