const getSuperSquareAll = () => [
  {
    id: 1,
    name: 'Super manzana 1',
    polygon: [
      {
        latitude: -31.416192891095786166033576591871678829193115234375,
        longitude: -64.1872268159656442776395124383270740509033203125
      },
      {
        latitude: -31.4173633751327230356764630414545536041259765625,
        longitude: -64.1831718028292783628785400651395320892333984375
      }
    ],
    status: { state: 'GOOD', score: 2 }
  },
  {
    id: 2,
    name: 'Super manzana 2',
    polygon: [
      {
        latitude: -31.415719391893414780270177288912236690521240234375,
        longitude: -64.1930476680082477969335741363465785980224609375
      },
      {
        latitude: -31.416578345563181073885061778128147125244140625,
        longitude: -64.1903919007685175301958224736154079437255859375
      }
    ],
    status: { state: 'NEUTRAL', score: 0.5 }
  },
  {
    id: 3,
    name: 'Super manzana 3',
    polygon: [
      {
        latitude: -31.410097453203082551453917403705418109893798828125,
        longitude: -64.1815630759275421723941690288484096527099609375
      },
      {
        latitude: -31.410465227283669520375042338855564594268798828125,
        longitude: -64.18023166918072774933534674346446990966796875
      },
      {
        latitude: -31.409480598604165635379104060120880603790283203125,
        longitude: -64.1797380430976005527554661966860294342041015625
      },
      {
        latitude: -31.409029263218197769447215250693261623382568359375,
        longitude: -64.181091407515310720555135048925876617431640625
      }
    ],
    status: { state: 'BAD', score: -1.5 }
  }
];

const getSuperSquareAllNames = () => [
  { id: 1, name: 'Super manzana 1' },
  { id: 2, name: 'Super manzana 2' },
  { id: 3, name: 'Super manzana 3' },
  { id: 4, name: 'Super manzana 4' }
];

const getSuperSquareStatus = () => [
  {
    id: 1,
    name: 'Super Manzana 1',
    state: 'GOOD',
    categories: [
      { id: 1, name: 'Medioambiente', state: 'GOOD' },
      { id: 2, name: 'Servicio', state: 'GOOD' },
      { id: 3, name: 'Infraestructura', state: 'NEUTRAL' }
    ]
  },
  {
    id: 2,
    name: 'Super Manzana 2',
    state: 'NEUTRAL',
    categories: [
      { id: 1, name: 'Medioambiente', state: 'GOOD' },
      { id: 2, name: 'Servicio', state: 'BAD' },
      { id: 3, name: 'Infraestructura', state: 'NEUTRAL' }
    ]
  },
  {
    id: 3,
    name: 'Super Manzana 3',
    state: 'BAD',
    categories: [
      { id: 1, name: 'Medioambiente', state: 'BAD' },
      { id: 2, name: 'Servicio', state: 'BAD' },
      { id: 3, name: 'Infraestructura', state: 'NO_STATE' }
    ]
  }
];

const getSuperSquareNumberReviews = () => [
  { state: 'GOOD', average: 3.5, numberMessages: 3500 },
  { state: 'NEUTRAL', average: 2.3, numberMessages: 2300 },
  { state: 'BAD', average: 1.2, numberMessages: 1100 }
];

const getTopWords = () => (
  [
    { word: 'Plaza', state: 'GOOD', numberMessages: 5000 },
    { word: 'Funcionarios', state: 'BAD', numberMessages: 320 },
    { word: 'Verde', state: 'NEUTRAL', numberMessages: 1500 },
    { word: 'Pagos', state: 'NEUTRAL', numberMessages: 500 },
    { word: 'Desastre', state: 'BAD', numberMessages: 200 }
  ]
);

export default () => ({
  getSuperSquareAllNames,
  getSuperSquareAll,
  getSuperSquareStatus,
  getSuperSquareNumberReviews,
  getTopWords
});
