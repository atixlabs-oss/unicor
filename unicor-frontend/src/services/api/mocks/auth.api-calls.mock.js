import { setItem } from '../../../utils/cookie';
import { setToken } from '../../jwtManager';

const login = () => {
  const USER_MOCKED = {
    id: 1,
    username: 'admin@atixlabs.com',
    email: 'rickyFort@atixlabs.com',
    name: null,
    lastname: null,
    accessToken:
      'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBhdGl4bGFicy5jb20iLCJpYXQiOjE2MTQ2MzAzNTQsImV4cCI6MTYxNTIzNTE1NH0.39smJksuOMkAmcyYfcLHFUWu9pL3rqXJdmxmP1lVwstbUx3zFpQSBGz5sEeeq5LKa2ywK8XAszdir6w8LRSTbQ',
    primaryNavbar: {
      menus: [
        {
          key: 'USERS',
          description: 'My account',
          items: [
            {
              key: 'LOGOUT',
              description: 'Logout',
              items: [],
              url: '/login',
              icon: 'logout',
              order: 1,
              type: 'GET'
            },
            {
              key: 'VIEW_PROFILE',
              description: 'View Profile',
              items: [],
              url: '/user-information',
              icon: 'user',
              order: 0,
              type: 'GET'
            }
          ],
          url: null,
          icon: 'user',
          order: 1
        }
      ]
    },
    secondaryNavbar: {
      menus: [
        {
          key: 'MANAGE_USERS',
          description: 'Manage Users',
          items: [],
          url: '/administration',
          icon: 'setting',
          order: 2
        }
      ]
    },
    role: 'Administrator',
    permissions: ['VIEW_PROFILE', 'CREATE_USER', 'MODIFY_USERS', 'LIST_USERS'],
    actions: ['DELETE', 'CREATE', 'UPDATE', 'VIEW']
  };

  setToken(USER_MOCKED.accessToken, false);
  setItem('user', USER_MOCKED);

  return USER_MOCKED;
};

const logout = () => ({});

export default () => ({
  login,
  logout
});
