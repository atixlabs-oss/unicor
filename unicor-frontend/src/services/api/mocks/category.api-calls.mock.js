const getCategoriesAll = () => [
  { id: 1, name: 'Medioambiente' },
  { id: 2, name: 'Servicio' },
  { id: 3, name: 'Infraestructura' }
];

const getTopCategories = () => [
  { name: 'Servicio', count: 102, average: 0.09, percentage: 0, state: 'NEUTRAL' },
  { name: 'Medioambiente', count: 60, average: 0.03, percentage: 51.67, state: 'NEUTRAL' },
  { name: 'Infraestructura', count: 42, average: 0.17, percentage: 58.33, state: 'NEUTRAL' }
];

export default () => ({
  getCategoriesAll,
  getTopCategories
});
