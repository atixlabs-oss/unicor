const getAllRoles = () => [
  { id: 1, code: 'ROLE_ADMIN', description: 'Administrator' },
  { id: 2, code: 'ROLE_OPERATOR', description: 'Operator' }
];

export default () => ({
  getAllRoles
});
