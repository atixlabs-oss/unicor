const getRange = () => ({
  min: -1,
  max: 1
});

const getSentimentals = () => [
  {
    id: 1,
    state: 'GOOD'
  },
  {
    id: 2,
    state: 'NEUTRAL'
  },
  {
    id: 3,
    state: 'BAD'
  }
];

export default () => ({
  getRange,
  getSentimentals
});
