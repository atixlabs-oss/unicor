const getUsersFiltered = () => ({
  content: [
    {
      id: 1,
      email: 'rickyFort@atixlabs.com',
      role: { id: 1, code: 'ROLE_ADMIN', description: 'Administrator' },
      active: true
    },
    {
      id: 4,
      email: 'JonSnow@test.com',
      role: { id: 1, code: 'ROLE_ADMIN', description: 'Administrator' },
      active: true
    },
    {
      id: 6,
      email: 'google@yahoo.com.ar',
      role: { id: 1, code: 'ROLE_ADMIN', description: 'Administrator' },
      active: true
    },
    {
      id: 2,
      email: 'operator@atixlabs.com',
      role: { id: 2, code: 'ROLE_OPERATOR', description: 'Operator' },
      active: true
    }
  ],
  pageable: {
    sort: { sorted: true, unsorted: false, empty: false },
    offset: 0,
    pageNumber: 0,
    pageSize: 11,
    paged: true,
    unpaged: false
  },
  totalPages: 1,
  totalElements: 4,
  last: true,
  size: 11,
  number: 0,
  sort: { sorted: true, unsorted: false, empty: false },
  first: true,
  numberOfElements: 7,
  empty: false
});

const getUserById = () => ({
  id: 1,
  email: 'rickyFort@atixlabs.com',
  role: { id: 1, code: 'ROLE_ADMIN', description: 'Administrator' },
  active: true,
  passwordUpdated: false
});

const patchUser = () => {};
const postUser = () => {};
const editUser = () => {};

export default () => ({
  getUsersFiltered,
  patchUser,
  postUser,
  getUserById,
  editUser
});
