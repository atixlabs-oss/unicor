const Logger = {
  // eslint-disable-next-line no-console
  error: console.error,
  // eslint-disable-next-line no-console
  warn: console.warn,
  // eslint-disable-next-line no-console
  info: console.info,
  // eslint-disable-next-line no-console
  debug: console.debug
};

export default Logger;
