const development = {
  endpoints: {
    backend: 'http://localhost:3010/'
  }
};

const staging = {
  endpoints: {
    backend: 'https://api.staging.unicor.atixlabs.xyz/'
  }
};

const uat = {
  endpoints: {
    backend: 'https://api.uat.unicor.atixlabs.xyz/'
  }
};

const production = {
  endpoints: {
    backend: 'https://api.cordoba.socialatlas.app'
  }
};

const configs = {
  development,
  staging,
  uat,
  production
};

let environment = window._env_ || 'development';

const setEnvironment = customEnvironment => (environment = customEnvironment);

const config = configs[environment];

const GOOGLE_API_KEY = 'AIzaSyCmdQDHoFZgDEaAb48mZdz5dhQI-O94H3I';

export default {
  setEnvironment,
  config,
  GOOGLE_API_KEY
};
