import { getItem, removeItem, setItem } from '../utils/cookie';

const saveToken = (token, isRemember) => {
  if (isRemember) {
    localStorage.setItem('isRemember', isRemember);
  } else {
    sessionStorage.setItem('isRemember', isRemember);
  }
  setItem('token', token);
};

export const setToken = (token, isRemember) => {
  saveToken(token, Boolean(isRemember));
};

export const getToken = () => getItem('token');

export const removeToken = () => removeItem('token');

export default {
  setToken,
  removeToken,
  getToken
};
