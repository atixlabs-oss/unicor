import React, { useContext, createContext } from 'react';

export const APIContext = createContext({});

export const withApi = Component => properties => {
  const api = useContext(APIContext);
  return <Component {...properties} api={api} />;
};
