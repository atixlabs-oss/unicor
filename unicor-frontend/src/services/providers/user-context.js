import React, { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getItem, setItem } from '../../utils/cookie';

const UserContext = createContext();

const UserProvider = ({ children }) => {
  const [user, setUser] = useState(getItem('user') || {});

  useEffect(() => {
    setItem('user', user);
  }, [user]);

  return <UserContext.Provider value={{ user, setUser }}>{children}</UserContext.Provider>;
};

export { UserContext, UserProvider };

UserProvider.propTypes = {
  children: PropTypes.node.isRequired
};
