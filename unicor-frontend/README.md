#Atix React Frontend Template

This is the Atix's template for all frontend projects

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Node 12.16.1

### Installing

A step by step series of examples that tell you how to get a development env running

1. Add the atix-react-frontend-template repository:

```shell script
git remote add atix-react-frontend-template git@gitlab.com:atixlabs/atix-react-frontend-template.git
# Replace ${PROJECT_NAME}
git subtree add --prefix ${PROJECT_NAME}-frontend atix-react-frontend-template master --squash
```

2. Install dependencies

```shell script
cd ${PROJECT_NAME}-frontend
npm install
```

3. Edit the files and push the changes to your repository. See [Change configurations](#Change configurations)

4. If you want to update and get latest `atix-react-frontend-template` changes, execute:

```
git fetch atix-react-frontend-template master # update the reference
git subtree pull --prefix ${PROJECT_NAME}-frontend atix-react-frontend-template master --squash # pull the changes into our repo
```

### Change configurations

Change each property list below accordingly

TODO list

## Running the tests

- npm run test

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Run EsLint tests

```shell script
npx eslint . --ext .jsx --ext .js
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://github.com/IQAndreas/markdown-licenses) file for details
